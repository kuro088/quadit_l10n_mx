# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from openerp.osv import osv
from odoo import tools
import base64
#~ import mx.DateTime
import time
import os
from xml.dom import minidom
import xmltodict
import base64
from suds.client import Client
import os
import sys
import time
import tempfile
import base64
import binascii
import string
import random
from odoo.exceptions import ValidationError

class wizard_read_xml(osv.TransientModel):
    _name = 'wizard.read.xml'

    @api.model
    def _default_company(self):
        company=self.env['res.users'].browse(self._uid).company_id.id
        return company

    xml_file= fields.Binary('Archivo XML')
    xml_datas_fname=fields.Char('XML')
    company_id=fields.Many2one('res.company','company',default=_default_company)
    valid=fields.Boolean('Valid', default=False)
    state=fields.Selection([
        ('draft','Borrador'),
        ('open','Abierta'),
        ],'Estado', default='open')
    sale_product=fields.Boolean('Crear productos si no existen')
    purchase_product=fields.Boolean('Crear productos si no existen')
    lines=fields.One2many('wizard.read.xml.lines', 'wizard_id', 'Facturas')     

    def get_taxes(self,imp,baseline,type_invoice):
        acc_conf=self.env['res.config.settings']
        tax_id=[]
        tax_cuota=[]
        if imp.get('cfdi:Impuestos',[]):

            traslados=imp.get('cfdi:Impuestos',[]).get('cfdi:Traslados',{})
            if traslados:
                if type(imp.get('cfdi:Impuestos',[]).get('cfdi:Traslados',{}).get('cfdi:Traslado',{}))==list:
                    for tras in imp.get('cfdi:Impuestos',[]).get('cfdi:Traslados',{}).get('cfdi:Traslado',{}):
                        traslado=tras
                        Importe=traslado.get('@Importe',0.0) 
                        TasaOCuota=traslado.get('@TasaOCuota',0.000000)
                        TipoFactor=traslado.get('@TipoFactor','Tasa')
                        Impuesto=traslado.get('@Impuesto','002')
                        Base=traslado.get('@Base',0)
                        if TipoFactor == 'Cuota':
                            acc_cuota_id=acc_conf.search([('account_tax_oil_id','!=',None)],limit=1)
                            if not acc_cuota_id:
                                raise ValidationError(_('No tienes configurada cuenta para impuestos de combustible, Entra en configuracion de contabilidad y asigna una'))
                            tax_cuota.append({'name':TipoFactor+' ' + str(TasaOCuota),
                                'account_id':acc_cuota_id.account_tax_oil_id.id or False,'manual':True,'base':float(Base),'amount':Importe})
                        if Impuesto:
                            if type_invoice=='sale':
                                id_tax=self.env['account.tax'].search( [('type_tax_use','=','sale'),('amount','=',(float(TasaOCuota)*100)),('code_sat','=',Impuesto),('company_id','=',self.company_id.id)],limit=1)
                                if id_tax:
                                    
                                    #~ tax_id.append(id_tax.id)
                                    tax_cuota.append({'name':id_tax.name,
                                    'account_id':id_tax.account_id.id or False,'manual':True,'base':float(Base),'amount':Importe})
                            if type_invoice=='purchase':
                                id_tax=self.env['account.tax'].search([('type_tax_use','=','purchase'),('amount','=',(float(TasaOCuota)*100)),('code_sat','=',Impuesto),('company_id','=',self.company_id.id)],limit=1)
                                if id_tax:
                                    tax_id.append(id_tax.id)
                                    if Base != baseline:
                                        acc_cuota_id=acc_conf.search([('account_tax_oil_id','!=',None)],limit=1)
                                        if not acc_cuota_id:
                                            raise ValidationError(_('No tienes configurada cuenta para impuestos de combustible, Entra en configuracion de contabilidad y asigna una'))
                                        tax_cuota.append({'name':'Combustible',
                                            'account_id':acc_cuota_id.account_tax_oil_id.id or False,'manual':True,'base':float(Base),'amount':-1*((float(baseline)*float(TasaOCuota))-float(Importe))})
                                    #~ tax_cuota.append({'name':id_tax.name,
                                    #~ 'account_id':id_tax.account_id.id or False,'manual':True,'base':float(Base),'amount':Importe})
                else:
                    traslado=imp.get('cfdi:Impuestos',[]).get('cfdi:Traslados',{}).get('cfdi:Traslado',{})
                    Importe=traslado.get('@Importe',0.0) 
                    TasaOCuota=traslado.get('@TasaOCuota',0.000000)
                    TipoFactor=traslado.get('@TipoFactor','Tasa')
                    Impuesto=traslado.get('@Impuesto','002')
                    Base=traslado.get('@Base',0)
                    if Impuesto:
                        if type_invoice=='sale':
                            id_tax=self.env['account.tax'].search( [('type_tax_use','=','sale'),('amount','=',(float(TasaOCuota)*100)),('code_sat','=',Impuesto),('company_id','=',self.company_id.id)],limit=1)
                            if id_tax:
                                #~ tax_id.append(id_tax.id)
                                tax_cuota.append({'name':id_tax.name,
                                    'account_id':id_tax.account_id.id or False,'manual':True,'base':float(Base),'amount':Importe})
                        if type_invoice=='purchase':
                            id_tax=self.env['account.tax'].search([('type_tax_use','=','purchase'),('amount','=',(float(TasaOCuota)*100)),('code_sat','=',Impuesto),('company_id','=',self.company_id.id)],limit=1)
                            if id_tax:
                                tax_id.append(id_tax.id)
                                if Base != baseline:
                                    acc_cuota_id=acc_conf.search([('account_tax_oil_id','!=',None)],limit=1)
                                    if not acc_cuota_id:
                                        raise ValidationError(_('No tienes configurada cuenta para impuestos de combustible, Entra en configuracion de contabilidad y asigna una'))
                                    tax_cuota.append({'name':'Combustible',
                                        'account_id':acc_cuota_id.account_tax_oil_id.id or False,'manual':True,'base':float(Base),'amount':-1*((float(baseline)*float(TasaOCuota))-float(Importe))})
                                    #~ tax_cuota.append({'name':id_tax.name,
                                        #~ 'account_id':id_tax.account_id.id or False,'manual':True,'base':float(Base),'amount':Importe})
            retenidos=imp.get('cfdi:Impuestos',[]).get('cfdi:Retenciones',{})
            if retenidos:
                ret=imp.get('cfdi:Impuestos',{}).get('cfdi:Retenciones',{}).get('cfdi:Retencion',{})
                if type(ret)==list:
                    for r in ret:
                        Importe=r.get('@Importe',0.0) 
                        TasaOCuota= float(r.get('@TasaOCuota',0.000000)) * -1
                        TipoFactor=r.get('@TipoFactor','Tasa')
                        Impuesto=r.get('@Impuesto','')
                        Base=r.get('@Base',0)
                        if Impuesto:
                            if type_invoice=='sale':
                                tax_ids=self.env['account.tax'].search([('type_tax_use','=','sale'),('code_sat','=',Impuesto),('company_id','=',self.company_id.id)])
                                if tax_ids:
                                    for tax_obj in tax_ids:
                                        if "%.3f" % tax_obj.amount == "%.3f" % (TasaOCuota*100):
                                            tax_id.append(tax_obj.id)
                                
                            if type_invoice=='purchase':
                                tax_ids=self.env['account.tax'].search([('type_tax_use','=','purchase'),('code_sat','=',Impuesto),('company_id','=',self.company_id.id)])
                                if tax_ids:
                                    for tax_obj in tax_ids:
                                        if "%.3f" % tax_obj.amount == "%.3f" % (TasaOCuota*100):
                                            tax_id.append(tax_obj.id)
                else:
                    if retenidos:
                        ret=imp.get('cfdi:Impuestos',{}).get('cfdi:Retenciones',{}).get('cfdi:Retencion',{})
                        Importe=ret.get('@Importe',0.0) 
                        TasaOCuota= -1 *(ret.get('@TasaOCuota',0.000000))
                        TipoFactor=ret.get('@TipoFactor','Tasa')
                        Impuesto=ret.get('@Impuesto','')
                        Base=ret.get('@Base',0)
                        if Impuesto:
                            if type_invoice=='sale':
                                tax_ids=self.env['account.tax'].search([('type_tax_use','=','sale'),('code_sat','=',Impuesto),('company_id','=',self.company_id.id)])
                                if tax_ids:
                                    for tax_obj in tax_ids:
                                        if "%.3f" % tax_obj.amount == "%.3f" % (TasaOCuota*100):
                                            tax_id.append(tax_obj.id)
                                
                            if type_invoice=='purchase':
                                tax_ids=self.env['account.tax'].search([('type_tax_use','=','purchase'),('code_sat','=',Impuesto),('company_id','=',self.company_id.id)])
                                if tax_ids:
                                    for tax_obj in tax_ids:
                                        if "%.3f" % tax_obj.amount == "%.3f" % (TasaOCuota*100):
                                            tax_id.append(tax_obj.id)
        else:
            pass
        return tax_id,tax_cuota

    def create_invoices(self):
        journal=self.env['account.journal']
        acc_obj=self.env['account.account']
        tax_obj=self.env['account.tax']
        acc_conf=self.env['res.config.settings']
        invoice=self.env['account.invoice']
        invoice_tax=self.env['account.invoice.tax']
        invoice_line_obj=self.env['account.invoice.line']
        account_obj = self.env['account.account']
        categ_obj = self.env['product.category']
        invoice_ids=[]
        for w in self:
            for line in w.lines:
                if line.state=='valid':
                    dic_xml= dict(xmltodict.parse(base64.decodestring(line.xml_dic.encode('utf-8'))).get('cfdi:Comprobante', {}))
                    folio=dic_xml.get('@Folio', '')
                    duplicate_folio=invoice.search( ['|',('number','=',str(folio)),('move_name','=',str(folio))])
                    if duplicate_folio:
                        folio=folio + ''.join(random.choice(string.ascii_uppercase) for _ in range(2))
                    uuid=dic_xml.get('cfdi:Complemento', {}).get('tfd:TimbreFiscalDigital', {}).get('@UUID', '')
                    fecha_xml=dic_xml.get('@Fecha', ''). split('T')[0]+ ' '+ dic_xml.get('@Fecha', ''). split('T')[1]
                    total=dic_xml.get('@Total', 0.0)
                    pay=dic_xml.get('@FormaPago', '')
                    pay_id=self.env['res.forma.pago'].search([('name','=',pay)])
                    pay_method=dic_xml.get('@MetodoPago', '')
                    pay_method_id=self.env['res.met.pago'].search([('name','=',pay_method)])
                    sello_sat=dic_xml.get('cfdi:Complemento', {}).get('tfd:TimbreFiscalDigital', {}).get('SelloSAT', '')
                    cer_sat=dic_xml.get('cfdi:Complemento', {}).get('tfd:TimbreFiscalDigital', {}).get('@NoCertificadoSAT', '')
                    cfdi_fecha_timbrado=dic_xml.get('cfdi:Complemento', {}).get('tfd:TimbreFiscalDigital', {}).get('@FechaTimbrado', '')
                    cfdi_no_certificado=dic_xml.get('cfdi:Complemento', {}).get('tfd:TimbreFiscalDigital', {}).get('@NoCertificadoSAT', '')
                    no_certificado=dic_xml.get('@NoCertificado', '')
                    
                    if line.type=='out_invoice' or line.type=='out_refund' :
                        type_invoice='sale'
                        rfc_attachment=dic_xml.get('cfdi:Emisor', {}).get('@Rfc', '')
                        journal_id=self.env['account.journal'].search([('type','=',type_invoice),('company_id','=',w.company_id.id)],limit=1)
                        journal_id=journal_id and journal_id.id or False
                        valores={'journal_id':journal_id,'sign':True,'number':folio,'number':folio,'partner_id':line.partner_id.id,
                                'date_invoice':fecha_xml,'account_id':line.partner_id.property_account_receivable_id.id,'type':line.type,
                                'met_pago_id':pay_method_id and pay_method_id.id or False,
                                'forma_pago_id':pay_id and pay_id.id or False,
                                'folio_fiscal_cfdi':str(uuid),'cfdi_no_certificado':cfdi_no_certificado,
                                'no_certificado':unicode(no_certificado),
                                'currency_id':line.currency_id.id}
                        invoice_id=invoice.create(valores)

                    else:
                        type_invoice='purchase'
                        rfc_attachment=dic_xml.get('cfdi:Emisor', {}).get('@Rfc', '')
                        journal_id=self.env['account.journal'].search( [('type','=',type_invoice),('company_id','=',w.company_id.id)],limit=1)
                        journal_id=journal_id and journal_id.id or False
                        invoice_id=invoice.create({'journal_id':journal_id,'supplier_invoice_number':folio,'number':folio,'reference':line.file,'move_name':folio,'partner_id':line.partner_id.id,
                                'date_invoice':fecha_xml,'account_id':line.partner_id.property_account_payable_id.id,'type':line.type,
                                'met_pago_id':pay_method_id and pay_method_id.id or False,
                                'forma_pago_id':pay_id and pay_id.id or False,
                                'folio_fiscal_cfdi':str(uuid),'check_total':float(total[0]),
                                'currency_id':line.currency_id.id})
                    tax_id=[]
                    if type(dic_xml.get('cfdi:Conceptos', {}).get('cfdi:Concepto', []))==list:
                        for invoice_line in dic_xml.get('cfdi:Conceptos', {}).get('cfdi:Concepto', []):
                            base=invoice_line.get('@Importe', 0)
                            tax_id,tax_cuota=self.get_taxes(invoice_line,base,type_invoice)
                            if tax_cuota:
                                for t in tax_cuota:
                                    t.update({'invoice_id': invoice_id.id})
                                    invoice_tax.create(t)
                            uom=invoice_line.get('@ClaveUnidad', '')
                            uom_id=self.env['clave.prod.uom'].search([('name','=',uom)],limit=1)
                            cve_serv=invoice_line.get('@ClaveProdServ', '')
                            cve_serv_id=self.env['clave.prod.serv'].search( [('name','=',cve_serv)],limit=1)
                            if not uom_id:
                                uom_id=self.env['clave.prod.uom'].search( [('name','=','EA')],limit=1)
                            id_product =False
                            prod=False
                            acc_default = categ_obj.search([('id', '=', 1)])
                            acc_default_id=acc_default and acc_default[0].property_account_expense_categ_id or False
                            discount=float(invoice_line.get('@Descuento', 0))*100/float(invoice_line.get('@Importe', 0.0))
                            invoice_line_vals = {
                                #~ 'product_id': id_product,
                                'quantity': invoice_line.get('@Cantidad', 0.0),
                                'name': invoice_line.get('@Descripcion', ''),
                                'account_id':acc_default_id and acc_default_id.id or False,
                                'price_unit': invoice_line.get('@ValorUnitario', 0.0),
                                'claveserv_id': cve_serv_id and cve_serv_id.id or False,
                                'uos_id': uom_id and uom_idid or False,
                                'invoice_line_tax_ids':[(6,0,tax_id)],
                                'invoice_id': invoice_id.id,
                                'discount':discount,
                            }
                            invoice_line_obj.create(invoice_line_vals)
                    else:
                        uom=dic_xml.get('cfdi:Conceptos', {}).get('cfdi:Concepto', []).get('@Unidad', '')
                        cve_serv=dic_xml.get('cfdi:Conceptos', {}).get('cfdi:Concepto', []).get('@ClaveProdServ', '')
                        cve_serv_id=self.env['clave.prod.serv'].search([('name','=',cve_serv)],limit=1)
                        new_list_tax=[]
                        imp=dic_xml.get('cfdi:Conceptos', {}).get('cfdi:Concepto', {})
                        base=dic_xml.get('cfdi:Conceptos', {}).get('cfdi:Concepto', []).get('@Importe', 0.0)
                        tax_id,tax_cuota=self.get_taxes(imp,base,type_invoice)

                        if tax_cuota:
                            for t in tax_cuota:
                                t.update({'invoice_id': invoice_id.id})
                                invoice_tax.create(t)
                        acc_default = categ_obj.search([('id', '=', 1)])
                        acc_default_id=acc_default and acc_default[0].property_account_expense_categ_id or False
                        discount=float(dic_xml.get('cfdi:Conceptos', {}).get('cfdi:Concepto', []).get('@Descuento', 0))*100/float(dic_xml.get('cfdi:Conceptos', {}).get('cfdi:Concepto', []).get('@Importe', 0.0))
                        invoice_line_vals = {
                            #~ 'product_id': id_product,
                            'quantity': dic_xml.get('cfdi:Conceptos', {}).get('cfdi:Concepto', []).get('@Cantidad', 0.0),
                            'name': dic_xml.get('cfdi:Conceptos', {}).get('cfdi:Concepto', []).get('@Descripcion', ''),
                            'price_unit': dic_xml.get('cfdi:Conceptos', {}).get('cfdi:Concepto', []).get('@ValorUnitario', 0.0),
                            'claveserv_id': cve_serv_id and cve_serv_id.id or False,
                            'account_id':acc_default_id and acc_default_id.id or False,
                            #~ 'uos_id': uom_id and uom_id[0] or False,
                            'invoice_line_tax_ids':[(6,0,tax_id)],
                            'invoice_id': invoice_id.id,
                            'discount':discount,
                        }
                        invoice_line_obj.create(invoice_line_vals)
                    if dic_xml.get('cfdi:Complemento', {}).get('implocal:ImpuestosLocales', {}):
                        acc_cuota_id=acc_conf.search([('account_tax_local_id','!=',None)],limit=1)
                        if not acc_cuota_id:
                            raise ValidationError(_('No tienes configurada cuenta para impuestos locales, Entra en configuracion de contabilidad y asigna una'))
                        tax_local=dic_xml.get('cfdi:Complemento', {}).get('implocal:ImpuestosLocales', {}).get('implocal:TrasladosLocales',{})
                        tax_cuota_dic={'name':tax_local.get('@ImpLocTrasladado',"-"),'invoice_id':invoice_id.id,
                                'account_id':acc_cuota_id.account_tax_local_id.id or False,'manual':True,'base':invoice_id.amount_untaxed,'amount':float(tax_local.get('@Importe',0.0))}
                        invoice_tax.create(tax_cuota_dic)
                    id_atta=self.env['ir.attachment'].create( {
                                'name':rfc_attachment + '_'+ folio + '.xml',
                                'datas': line.xml_dic,
                                'datas_fname': rfc_attachment + '_'+ folio +'.xml',
                                'res_model': 'account.invoice',
                                'res_id': invoice_id.id,
                                })
                    invoice_id.compute_taxes()
                    invoice_ids.append(invoice_id.id)
                    #~ if w.state=='open':
                        #~ invoice_id.signal_workflow( 'invoice_open')

        return {
                'name': 'Facturas importadas',
                'view_type': 'form',
                'view_mode': 'list,form',

                'context':{'default_type': 'in_invoice', 'type': 'in_invoice', 'journal_type': 'purchase'},
                'res_model': 'account.invoice',
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', invoice_ids)],
            }
        
    def valida_xml(self):
        journal=self.env['account.journal']
        acc_obj=self.env['account.account']
        tax_obj=self.env['account.tax']
        invoice=self.env['account.invoice']
        #~ certificate_lib = self.pool.get('facturae.certificate.library')
        for w in self:
            rfc=w.company_id.vat
            invoice_ids=[]
            if w.xml_file:
                if '.xml' in w.xml_datas_fname:
                    msg=""
                    #~ xml_bin=binascii.b2a_base64(w.xml_file).encode('utf-8')
                    result=invoice.valida_xml(w.xml_file)
                    result.update({'name':w.xml_datas_fname,'xml':w.xml_file})
                    invoice_ids.append(result)
                if '.zip' in w.xml_datas_fname:
                    (fileno, fname) = tempfile.mkstemp(".zip", "invoice_tmp_zip")
                    f = open( fname, 'wb' )
                    f.write( base64.decodestring( w.xml_file or '' ) )
                    f.close()
                    os.close( fileno )
                    path=tempfile.gettempdir()+'/ZIP'
                    convert = "unzip -o %s -d%s"%(fname,path)
                    os.system(convert)
                    for filename in os.listdir(path+'/'+str(w.xml_datas_fname).split('.')[0]):
                        if '.xml' in filename:
                            xml_file=base64.b64encode(open(path+'/'+str(w.xml_datas_fname).split('.')[0]+'/'+filename, 'rb').read())
                            xml_bin=binascii.b2a_base64(xml_file)
                            result=invoice.valida_xml(xml_file)
                            
                            result.update({'name':filename,'xml':xml_file})
                            invoice_ids.append(result)
                invoice_line=[]                

                for line in invoice_ids:
                    msg=''
                    state='valid'
                    partner_id=False
                    if  line['comprobante'].get('@TipoDeComprobante','') != 'I':
                        state='no_valid'
                        msg='Tipo de Comprobante no es Ingreso'

                    rfc_receptor = line['comprobante'].get('cfdi:Receptor', {}).get('@Rfc', '').replace(' ','')
                    rfc_emisor = line['comprobante'].get('cfdi:Emisor', {}).get('@Rfc', '').replace(' ','')
                    moneda = line['comprobante'].get('@Moneda', '')
                    currency_id=self.env['res.currency'].search( [('name','=',moneda)],limit=1)
                    if rfc != rfc_emisor and rfc != rfc_receptor:
                        state='no_valid'
                        msg='XML no corresponde a la empresa'
                    
                    if  line['comprobante'].get('@TipoDeComprobante','') == 'I' and rfc==rfc_receptor:
                        type_invoice='in_invoice'
                        partner_ids = self.env['res.partner'].search([('vat','=',rfc_emisor)],limit=1)
                    else:
                        type_invoice='out_invoice'
                        partner_ids = self.env['res.partner'].search([('vat','=',rfc_receptor)],limit=1)
                    if  line['comprobante'].get('@TipoDeComprobante','') == 'E':
                        state='valid'
                        msg=""
                        type_invoice='in_refund'
                        partner_ids = self.env['res.partner'].search([('vat','=',rfc_emisor)],limit=1)
                        rfc_aux=rfc_receptor
                        rfc_receptor=rfc_emisor
                        rfc_emisor=rfc_aux
                    if not partner_ids and state=='valid':
                        if type_invoice=='out_invoice' or type_invoice=='out_refound':
                            uso_cfdi_id= self.env['res.uso.cfdi'].search([('name','=ilike',line['comprobante'].get('cfdi:Receptor', {}).get('@UsoCFDI', ''))],limit=1)
                            uso_id=uso_cfdi_id and uso_cfdi_id[0].id or False
                            
                            partner={
                                    'name':line['comprobante'].get('cfdi:Receptor', {}).get('@Nombre', ''),
                                    'uso_cfdi_id':uso_id,
                                    'vat':rfc_receptor,
                                    'customer':True,
                                    'supplier':False,
                                    'type':'invoice',
                                }
                        if type_invoice=='in_invoice' or type_invoice=='in_refund':
                            partner={
                                    'name':line['comprobante'].get('cfdi:Emisor', {}).get('@Nombre', ''),
                                    'zip':line['comprobante'].get('cfdi:LugarExpedicion', ''),
                                    'vat':rfc_emisor,
                                    'customer':False,
                                    'supplier':True,
                                    'type':'invoice',
                                }
                        partner_id=self.env['res.partner'].create(partner)
                    tax_id=[]
                    if state=='valid':
                        if type(line['comprobante'].get('cfdi:Conceptos', {}).get('cfdi:Concepto', []))==list:
                            for imp in line['comprobante'].get('cfdi:Conceptos', {}).get('cfdi:Concepto', {}):
                                if imp.get('cfdi:Impuestos',[]):
                                    traslados=imp.get('cfdi:Impuestos',[]).get('cfdi:Traslados',{})
                                    if traslados:
                                        if type(imp.get('cfdi:Impuestos',[]).get('cfdi:Traslados',{}).get('cfdi:Traslado',{})) == list:
                                            for n_imp in imp.get('cfdi:Impuestos',[]).get('cfdi:Traslados',{}).get('cfdi:Traslado',{}):
                                                traslado=n_imp
                                                Importe=traslado.get('@Importe',0.0) 
                                                TasaOCuota=traslado.get('@TasaOCuota',0.000000)
                                                TipoFactor=traslado.get('@TipoFactor','Tasa')
                                                Impuesto=traslado.get('@Impuesto','002')
                                                Base=traslado.get('@Base',0)
                                                if Impuesto:
                                                    if type_invoice=='out_invoice' or type_invoice=='out_refund':
                                                        id_tax=self.env['account.tax'].search( [('type_tax_use','=','sale'),('amount','=',(float(TasaOCuota)*100)),('code_sat','=',Impuesto),('company_id','=',w.company_id.id)],limit=1)
                                                        if id_tax:
                                                            tax_id.append(id_tax.id)
                                                        
                                                    if type_invoice=='in_invoice' or type_invoice=='in_refund':
                                                        id_tax=self.env['account.tax'].search([('type_tax_use','=','purchase'),('amount','=',(float(TasaOCuota)*100)),('code_sat','=',Impuesto),('company_id','=',w.company_id.id)],limit=1)
                                                        if id_tax:
                                                            tax_id.append(id_tax.id)
                                        else:
                                            
                                            traslado=imp.get('cfdi:Impuestos',[]).get('cfdi:Traslados',{}).get('cfdi:Traslado',{})
                                            Importe=traslado.get('@Importe',0.0) 
                                            TasaOCuota=traslado.get('@TasaOCuota',0.000000)
                                            TipoFactor=traslado.get('@TipoFactor','Tasa')
                                            Impuesto=traslado.get('@Impuesto','002')
                                            Base=traslado.get('@Base',0)
                                            if Impuesto:
                                                if type_invoice=='out_invoice' or type_invoice=='out_refund':
                                                    id_tax=self.env['account.tax'].search( [('type_tax_use','=','sale'),('amount','=',(float(TasaOCuota)*100)),('code_sat','=',Impuesto),('company_id','=',w.company_id.id)],limit=1)
                                                    if id_tax:
                                                        tax_id.append(id_tax.id)
                                                    
                                                if type_invoice=='in_invoice' or type_invoice=='in_refund':
                                                    id_tax=self.env['account.tax'].search([('type_tax_use','=','purchase'),('amount','=',(float(TasaOCuota)*100)),('code_sat','=',Impuesto),('company_id','=',w.company_id.id)],limit=1)
                                                    if id_tax:
                                                        tax_id.append(id_tax.id)
                                else:
                                    pass
                        else:
                            if line['comprobante'].get('cfdi:Conceptos', {}).get('cfdi:Concepto', {}).get('cfdi:Impuestos',[]):
                                traslados=line['comprobante'].get('cfdi:Conceptos', {}).get('cfdi:Concepto', {}).get('cfdi:Impuestos',{}).get('cfdi:Traslados',{})
                                if traslados:
                                    
                                    traslado=line['comprobante'].get('cfdi:Conceptos', {}).get('cfdi:Concepto', {}).get('cfdi:Impuestos',{}).get('cfdi:Traslados',{}).get('cfdi:Traslado',{})
                                    if type(traslado)==list:
                                        for tras in traslado:
                                            Importe=tras.get('@Importe',0.0) 
                                            TasaOCuota=float(tras.get('@TasaOCuota',0.000000))
                                            TipoFactor=tras.get('@TipoFactor','Tasa')
                                            Impuesto=tras.get('@Impuesto','002')
                                            Base=tras.get('@Base',0)
                                            if Impuesto:
                                                if type_invoice=='out_invoice' or type_invoice=='out_refund':
                                                    tax_ids=self.env['account.tax'].search([('type_tax_use','=','sale'),('code_sat','=',Impuesto),('company_id','=',w.company_id.id)])
                                                    if tax_ids:
                                                        for tax_obj in tax_ids:
                                                            if "%.4f" % tax_obj.amount == "%.4f" % (TasaOCuota*100):
                                                                tax_id.append(tax_obj.id)
                                                    
                                                if type_invoice=='in_invoice' or type_invoice=='in_refund':
                                                    tax_ids=self.env['account.tax'].search([('type_tax_use','=','purchase'),('code_sat','=',Impuesto),('company_id','=',w.company_id.id)])
                                                    if tax_ids:
                                                        for tax_obj in tax_ids:
                                                            if "%.4f" % tax_obj.amount == "%.4f" % (TasaOCuota*100):
                                                                tax_id.append(tax_obj.id)
                                    else:
                                        Importe=traslado.get('@Importe',0.0) 
                                        TasaOCuota=float(traslado.get('@TasaOCuota',0.000000))
                                        TipoFactor=traslado.get('@TipoFactor','Tasa')
                                        Impuesto=traslado.get('@Impuesto','002')
                                        Base=traslado.get('@Base',0)
                                        if Impuesto:
                                            if type_invoice=='out_invoice' or type_invoice=='out_refund':
                                                tax_ids=self.env['account.tax'].search([('type_tax_use','=','sale'),('code_sat','=',Impuesto),('company_id','=',w.company_id.id)])
                                                if tax_ids:
                                                    for tax_obj in tax_ids:
                                                        if "%.4f" % tax_obj.amount == "%.4f" % (TasaOCuota*100):
                                                            tax_id.append(tax_obj.id)
                                                
                                            if type_invoice=='in_invoice' or type_invoice=='in_refund':
                                                tax_ids=self.env['account.tax'].search([('type_tax_use','=','purchase'),('code_sat','=',Impuesto),('company_id','=',w.company_id.id)])
                                                if tax_ids:
                                                    for tax_obj in tax_ids:
                                                        if "%.4f" % tax_obj.amount == "%.4f" % (TasaOCuota*100):
                                                            tax_id.append(tax_obj.id)
                                    
                                retenidos=line['comprobante'].get('cfdi:Conceptos', {}).get('cfdi:Concepto', {}).get('cfdi:Impuestos',{}).get('cfdi:Retenciones',{})
                                if retenidos:
                                    ret=line['comprobante'].get('cfdi:Conceptos', {}).get('cfdi:Concepto', {}).get('cfdi:Impuestos',{}).get('cfdi:Retenciones',{}).get('cfdi:Retencion',{})
                                    if type(ret)==list:
                                        for r in ret:
                                            Importe=r.get('@Importe',0.0) 
                                            TasaOCuota= float(r.get('@TasaOCuota',0.000000)) * -1
                                            TipoFactor=r.get('@TipoFactor','Tasa')
                                            Impuesto=r.get('@Impuesto','')
                                            Base=r.get('@Base',0)
                                            if Impuesto:
                                                if type_invoice=='out_invoice' or type_invoice=='out_refund':
                                                    tax_ids=self.env['account.tax'].search([('type_tax_use','=','sale'),('code_sat','=',Impuesto),('company_id','=',w.company_id.id)])
                                                    if tax_ids:
                                                        for tax_obj in tax_ids:
                                                            if "%.3f" % tax_obj.amount == "%.3f" % (TasaOCuota*100):
                                                                tax_id.append(tax_obj.id)
                                                    
                                                if type_invoice=='in_invoice' or type_invoice=='in_refund':
                                                    tax_ids=self.env['account.tax'].search([('type_tax_use','=','purchase'),('code_sat','=',Impuesto),('company_id','=',w.company_id.id)])
                                                    if tax_ids:
                                                        for tax_obj in tax_ids:
                                                            if "%.3f" % tax_obj.amount == "%.3f" % (TasaOCuota*100):
                                                                tax_id.append(tax_obj.id)
                                    else:
                                        if retenidos:
                                            ret=line['comprobante'].get('cfdi:Conceptos', {}).get('cfdi:Concepto', {}).get('cfdi:Impuestos',{}).get('cfdi:Retenciones',{}).get('cfdi:Retencion',{})
                                            Importe=ret.get('@Importe',0.0) 
                                            TasaOCuota= -1 *(ret.get('@TasaOCuota',0.000000))
                                            TipoFactor=ret.get('@TipoFactor','Tasa')
                                            Impuesto=ret.get('@Impuesto','')
                                            Base=ret.get('@Base',0)
                                            if Impuesto:
                                                if type_invoice=='out_invoice' or type_invoice=='out_refund':
                                                    tax_ids=self.env['account.tax'].search([('type_tax_use','=','sale'),('code_sat','=',Impuesto),('company_id','=',w.company_id.id)])
                                                    if tax_ids:
                                                        for tax_obj in tax_ids:
                                                            if "%.3f" % tax_obj.amount == "%.3f" % (TasaOCuota*100):
                                                                tax_id.append(tax_obj.id)
                                                    
                                                if type_invoice=='in_invoice' or type_invoice=='in_refund':
                                                    tax_ids=self.env['account.tax'].search([('type_tax_use','=','purchase'),('code_sat','=',Impuesto),('company_id','=',w.company_id.id)])
                                                    if tax_ids:
                                                        for tax_obj in tax_ids:
                                                            if "%.3f" % tax_obj.amount == "%.3f" % (TasaOCuota*100):
                                                                tax_id.append(tax_obj.id)
                            else:
                                pass

                                            
                    uuid=line['comprobante'].get('cfdi:Complemento', {}).get('tfd:TimbreFiscalDigital', {}).get('@UUID', '')
                    duplicate=invoice.search( ['|',('cfdi_folio_fiscal','=',str(uuid)),('folio_fiscal_cfdi','=',str(uuid)), ('state','!=','cancel')])
                    if duplicate:
                        state='no_valid'
                        msg='Factura %s ya existe en el sistema'%uuid
                    
                    if line['acuse'] and line['acuse'].Estado != 'Vigente':
                        state='no_valid'
                        msg='El XML no esta vigente o fue cancelado!'
                    if partner_ids:
                        partner=partner_ids.id
                    else:
                        partner=partner_id and partner_id.id or False

                    self.write( {'lines':[(0,0,{'xml_dic':line['xml'],'invoice_line_tax_id':[(6,0,tax_id)],'partner_id':partner,
                    'file':line['name'],'type':type_invoice,'currency_id':currency_id and currency_id.id or False,'state':state,'msg':msg})]})
                    
        self.write({'valid':True})
        return {
                'name':"Leer XML",
                'view_type': 'form',
                'view_mode': 'form',
                'view_id': False,
                'res_model': 'wizard.read.xml',
                'domain': [],
                'type': 'ir.actions.act_window',
                'target': 'new',
                'res_id': self._ids[0],
                }

class wizard_read_xml_lines(osv.TransientModel):
    _name='wizard.read.xml.lines'
    

    file=fields.Char('Archivo')
    partner_id=fields.Many2one('res.partner','Proveedor')
    type= fields.Selection([
        ('out_invoice','Factura de Cliente'),
        ('in_invoice','Factura de Proveedor'),
        ('out_refund','Factura rectificativa de cliente'),
        ('in_refund','Factura rectificativa de Proveedor'),
        ],'Tipo',required=True, default='in_invoice')
    state=fields.Selection([
        ('valid','Valida'),
        ('no_valid','No valida'),
        ('cancel','Cancelada'),
        ],'Estado',required=True)
    xml_dic=fields.Text('Dic xml')
    msg=fields.Char('Error')
    invoice_line_tax_id=fields.Many2many('account.tax','account_tax_wiz', 'wiz_id', 'tax_id',string='Impuestos')
    xml_diC=fields.Binary('Archivo XML')
    currency_id=fields.Many2one('res.currency','Moneda')
    wizard_id=fields.Many2one('wizard.read.xml', 'Wizard')

    
    
    
    
    
    
    
    
