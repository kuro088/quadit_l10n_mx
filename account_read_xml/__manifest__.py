# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    "name" : "account_read_xml",
    "version": "11.0.1.0.0",
    "author": "Quadit, S.A. de C.V.",
    "category": "account",
    "description" : """This module read and validate xml CFDI 3.3, and create invoice""",
    'website': 'https://www.quadit.mx',
    "license": "OEEL-1",
    "depends" : [
        "l10n_mx_facturae_33",
    ],
    "demo" : [],
    "data" : ['wizard/read_xml_view.xml',
            'res_config_view.xml'],
    "installable" : True,
    "active" : False,
}
