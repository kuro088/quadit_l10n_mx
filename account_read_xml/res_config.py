# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time
import datetime
from dateutil.relativedelta import relativedelta

import odoo
from odoo import SUPERUSER_ID
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AccountConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'


    @api.model
    def default_get(self, fields):
        res = super(AccountConfigSettings, self).default_get(fields)
        tax_id=self.search([('account_tax_oil_id','!=',None)],limit=1)
        if tax_id:
            res.update({
                'account_tax_oil_id': tax_id.account_tax_oil_id and tax_id.account_tax_oil_id.id or False
            })
        tax_local_id=self.search([('account_tax_local_id','!=',None)],limit=1)
        if tax_local_id:
            res.update({
                'account_tax_local_id': tax_local_id.account_tax_local_id and tax_local_id.account_tax_local_id.id or False
            })
        return res
        
    account_tax_oil_id = fields.Many2one('account.account', string="Cuenta impuesto combustible")
    account_tax_local_id = fields.Many2one('account.account', string="Cuenta impuesto local")
