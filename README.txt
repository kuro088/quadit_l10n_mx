﻿# -*- encoding: utf-8 -*- 
#####################################################################################
#    Module Writen to Odoo 11.0 Community Edition
#    All Rights Reserved to <Quadit, S.A. de C.V.>
#####################################################################################

[ACERCA DE ESTA LOCALIZACION MEXICANA PARA ODOO]

Estos módulos contemplan los cambios necesarios para generar la versión XML 3.3 de
timbrado requerida en México. 
Incluye también los complementos de pago CFDI 3.3 


[ACERCA DEL IDIOMA]

Todos los mensajes al usuario y cadenas de los campos están intencionalmente escritos
en español mexicano ya que no consideramos necesario realizarlos en inglés e incluir
un archivo de traducción puesto que esta localización esta diseñada especialmente
para México y los requerimientos fiscales que este país requiere para facturación y 
complementos de pagos.

[LICENCIA DEL CÓDIGO]

Este código está desarrollado bajo la Liecencia "OEEL-1"

[SOPORTE]

Si usted requiere soporte, reportar algún problema o realizar algún comentario puede
enviarnos un correo a: info@quadit.mx o visiarnos en nuestro sitio web.

www.quadit.mx


#####################################################################################
