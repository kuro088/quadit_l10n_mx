# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class ParamsPac(models.Model):
    """Object use to hold the parameters for the PAC"""
    _name = 'params.pac'
    _description = __doc__

    def _get_method_type_selection(self):
        # From module of PAC inherit this function and add new methods
        types = []
        return types

    name = fields.Char(size=128)
    url_webservice = fields.Char(size=256)
    namespace = fields.Char(size=256)
    user = fields.Char(size=128)
    password = fields.Char(size=128)
    method_type = fields.Selection([
        ('firmar', 'Timbrar'), ('cancelar', 'Cancelar')], string='Proceso')
    company_id = fields.Many2one(
        'res.company', default=lambda self: self.env.user.company_id)
    active = fields.Boolean(
        help='Indicate if this param is active', default=True)
    sequence = fields.Integer(default=10)
    certificate_link = fields.Char(
        size=256, help='PAC have a public certificate that is necessary by '
        'customers to check the validity of the XML and PDF')
    user_id = fields.Many2one(
        'res.users', 'User Params Pac',
        help="The user responsible for this params pac")
