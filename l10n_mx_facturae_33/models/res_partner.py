# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class ResUsoCFDI(models.Model):
    """Object use to save the catalog to Use of CFDIs"""
    _name = 'res.uso.cfdi'
    _description = __doc__

    @api.depends('name', 'description')
    def name_get(self):
        result = []
        for pay in self:
            name = pay.name + '-' + pay.description
            result.append((pay.id, name))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        recs = self.browse()
        if name:
            recs = self.search([
                ('description', operator, name)] + args, limit=limit)
        if not recs:
            recs = self.search([('name', operator, name)] + args, limit=limit)
        return recs.name_get()

    name = fields.Char('Codigo', size=3, required=True)
    description = fields.Char(size=256)
    fisica = fields.Boolean()
    moral = fields.Boolean()
    date = fields.Date('Fecha inicio de vigencia')
    date_due = fields.Date('Fecha fin de vigencia')

    _sql_constraints = [
        ('name_unique', 'UNIQUE (name)', 'La clave debe ser unica')]


class ResFormaPago(models.Model):
    """Object use to save the catalog to payment ways"""
    _name = 'res.forma.pago'
    _description = __doc__

    @api.depends('name', 'description')
    def name_get(self):
        result = []
        for pay in self:
            name = pay.name + ' ' + pay.description
            result.append((pay.id, name))
        return result

    name = fields.Char(size=3, required=True)
    description = fields.Char(size=256)
    bancarizado = fields.Boolean()

    _sql_constraints = [
        ('name_unique', 'UNIQUE (name)', 'El codigo debe ser unico')]


class ResMetPago(models.Model):
    """Object use to save the catalog to payment methods"""
    _name='res.met.pago'
    _description = __doc__

    @api.depends('name', 'description')
    def name_get(self):
        result = []
        for pay in self:
            name = pay.name + ' ' + pay.description
            result.append((pay.id, name))
        return result

    name = fields.Char('Codigo', size=3, required=True)
    description = fields.Char(size=256)

    _sql_constraints= [(
        'name_unique', 'UNIQUE (name)', 'El codigo debe ser unica')]


class ResPartner(models.Model):
    _inherit = 'res.partner'

    uso_cfdi_id = fields.Many2one('res.uso.cfdi')

    @api.multi
    def _get_vat_partner(self):
        self.ensure_one()
        if self.country_id and self.country_id != self.env.ref('base.mx'):
            return 'XEXX010101000'
        return self.vat or ''


class AccountFiscalPosition(models.Model):
    _inherit = 'account.fiscal.position'

    code = fields.Char(
        help='Code defined to this position. If this record will be '
        'used as fiscal regime to CFDI, here must be assigned the code '
        'defined to this fiscal regime in the SAT catalog')
