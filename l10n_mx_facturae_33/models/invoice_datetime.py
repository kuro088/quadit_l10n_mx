# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.


import datetime
from pytz import timezone
import pytz
from odoo import api, fields, models


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def _get_date_invoice_tz_33(self):
        for invoice in self.filtered(lambda i: i.type in (
                'out_invoice', 'out_refund')):
            dt_tz = fields.Datetime.context_timestamp(
                self.with_context(tz=self.env.user.tz),
                fields.Datetime.from_string(fields.Datetime.now()))
            invoice.date_invoice_tz_33 = fields.Datetime.to_string(dt_tz)

    invoice_datetime_33 = fields.Datetime(
        'Date Electronic Invoiced ',
        states={'open': [('readonly', True)], 'close': [('readonly', True)]},
        help="Keep empty to use the current date")
    date_invoice_tz_33 = fields.Datetime(
        compute=_get_date_invoice_tz_33, method=True,
        string='Date Invoiced with TZ',
        help='Date of Invoice with Time Zone')

    @api.multi
    def copy(self, default=None):
        default = dict(default or {})
        default.update({'invoice_datetime': False, 'date_invoice': False})
        return super(AccountInvoice, self).copy(default)

    @api.model
    def _get_time_zone_33(self):
        res_users_obj = self.env['res.users']
        userstz = res_users_obj.browse(self._uid).partner_id.tz
        total = 0
        if userstz:
            hours = timezone(userstz)
            fmt = '%Y-%m-%d %H:%M:%S %Z%z'
            now = datetime.datetime.now()
            loc_dt = hours.localize(datetime.datetime(
                now.year, now.month, now.day, now.hour, now.minute,
                now.second))
            timezone_loc = (loc_dt.strftime(fmt))
            diff_timezone_original = timezone_loc[-5:-2]
            timezone_original = int(diff_timezone_original)
            times = str(datetime.datetime.now(pytz.timezone(userstz)))
            times = times[-6:-3]
            timezone_present = int(times)*-1
            total = timezone_original + ((
                timezone_present + timezone_original)*-1)
        return total

    @api.model
    def assigned_datetime_33(self, values):
        res = {}
        if values.get(
            'date_invoice', False) and not values.get(
                'invoice_datetime_33', False):
            res['invoice_datetime_33'] = fields.Datetime.from_string(
                fields.Datetime.now()).strftime('%Y-%m-%d %H:%M:%S')
            res['date_invoice'] = values['date_invoice']
        if values.get('invoice_datetime_33', False) and not values.get(
                'date_invoice', False):
            date_invoice = values['invoice_datetime_33'].date().strftime(
                '%Y-%m-%d')
            res['date_invoice'] = date_invoice
            res['invoice_datetime_33'] = values['invoice_datetime_33']

        if 'invoice_datetime_33' in values and 'date_invoice' in values and values[  # noqa
                'invoice_datetime_33'] and values['date_invoice']:
            date_invoice = values['invoice_datetime_33'].date().strftime(
                '%Y-%m-%d')
            if date_invoice != values['date_invoice'].strftime('%Y-%m-%d'):
                date_invoice = fields.Datetime.context_timestamp(
                    self, values['invoice_datetime_33'])
                res['date_invoice'] = date_invoice
                res['invoice_datetime_33'] = values['invoice_datetime_33']

        if not values.get('invoice_datetime_33', False) and not values.get(
                'date_invoice', False):
            res['date_invoice'] = fields.Date.context_today(self)
            res['invoice_datetime_33'] = fields.Datetime.now()

        return res

    @api.multi
    def action_move_create(self):
        for inv in self.filtered(lambda i: i.type in (
                'out_invoice', 'out_refund')):
            vals_date = self.assigned_datetime_33(
                {'invoice_datetime_33': inv.invoice_datetime_33,
                    'date_invoice': inv.date_invoice})
            inv.write(vals_date)
        return super(AccountInvoice, self).action_move_create()
