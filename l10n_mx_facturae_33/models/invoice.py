# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import base64
import os
import tempfile
import time

from suds.client import Client
from lxml import etree, objectify
from jinja2 import Environment, FileSystemLoader, StrictUndefined
from odoo import api, fields, models, _, tools
from odoo.exceptions import ValidationError, UserError


def conv_ascii(text):
    """Convierte vocales accentuadas, ñ y ç a sus caracteres equivalentes ASCII
    """
    old_chars = [
        'á', 'é', 'í', 'ó', 'ú', 'à', 'è', 'ì', 'ò', 'ù', 'ä', 'ë', 'ï', 'ö',
        'ü', 'â', 'ê', 'î', 'ô', 'û', 'Á', 'É', 'Í', 'Ó', 'Ú', 'À', 'È', 'Ì',
        'Ò', 'Ù', 'Ä', 'Ë', 'Ï', 'Ö', 'Ü', 'Â', 'Ê', 'Î', 'Ô', 'Û', 'ñ', 'Ñ',
        'ç', 'Ç', 'ª', 'º', '°', ' ', 'Ã', '"']
    new_chars = [
        'a', 'e', 'i', 'o', 'u', 'a', 'e', 'i', 'o', 'u', 'a', 'e', 'i', 'o',
        'u', 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U', 'A', 'E', 'I',
        'O', 'U', 'A', 'E', 'I', 'O', 'U', 'A', 'E', 'I', 'O', 'U', 'n', 'N',
        'c', 'C', 'a', 'o', 'o', ' ', 'A', "'"]
    for old, new in zip(old_chars, new_chars):
        try:
            text = text.replace(unicode(old, 'UTF-8'), new)
        except:
            try:
                text = text.replace(old, new)
            except:
                raise ValidationError(_(
                    'No se pudo re-codificar la cadena [%s] en la letra [%s]' % (text, old)))  # noqa
    return text


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    sign = fields.Boolean('Timbrada', copy=False)
    fiscal = fields.Boolean(related='journal_id.fiscal', string='CFDI')
    cfdi_folio_fiscal = fields.Char('Folio Fiscal(UUID)', size=256)
    forma_pago_id = fields.Many2one('res.forma.pago', 'Forma de pago')
    met_pago_id = fields.Many2one('res.met.pago', 'Metodo de pago')
    uso_cfdi_id = fields.Many2one('res.uso.cfdi', 'Uso CFDI')
    invoice_rel_id = fields.Many2one(
        'account.invoice', 'CFDI relacionado', domain=[('sign', '=', True)])
    cfdi_fecha_cancelacion = fields.Datetime('CFDI Fecha cancelacion')
    cfdi_cadena_original = fields.Text('CFD-I Original String')
    cfdi_sello = fields.Text('CFD-I Sello', help='Sign assigned by the SAT')
    cfdi_no_certificado = fields.Char(
        'CFD-I Certificado', size=32, help='Serial Number of the Certificate')
    cfdi_cadena_original = fields.Text(
        'CFD-I Cadena Original',
        help='Original String used in the electronic invoice')
    cfdi_fecha_timbrado = fields.Datetime(
        'CFD-I Fecha Timbrado',
        help='Date when is stamped the electronic invoice')
    no_certificado = fields.Text(
        'Certificate', size=64, help='Certificate used in the invoice')
    sello = fields.Text('Stamp', size=512, help='Digital Stamp')
    cfdi_cbb = fields.Binary('CFD-I CBB')
    cfdi_name = fields.Char(help='Used to save the CFDI name')
    sat_status = fields.Char(help='Return the document status in the SAT')

    def encrypt_key(self, rsa, password):
        rsa_encrypt = rsa.as_pem("des_ede3_cbc", lambda x: password)
        return rsa_encrypt

    def action_cancel(self):
        res = super(AccountInvoice, self).action_cancel()
        self.action_cancel_cfdi()
        self.env['ir.attachment'].search([
            ('res_id', 'in', self.filtered(lambda i: not i.sign).ids),
            ('res_model', '=', 'account.invoice')]).unlink()
        return res

    def action_cancel_cfdi(self):
        msg = ''
        folio_cancel = ''
        uuids = []
        pac_params_obj = self.env['params.pac']
        for inv in self.filtered('sign'):
            certificate_id = inv.company_id.certificate_id
            if not certificate_id:
                inv.message_post(body=_(
                    'No tienes definido certificado para esta compañia!'))
                continue
            pac_params = pac_params_obj.search([
                ('method_type', '=', 'cancelar'),
                ('company_id', '=', inv.company_emitter_id.id),
            ], limit=1)
            if not pac_params:
                inv.message_post(body=_(
                    'No tienes parametros del PAC configurados para '
                    'cancelar'))
                continue
            pac_usr = pac_params.user
            pac_pwd = pac_params.password
            wsdl_url = pac_params.url_webservice
            cer_pem = base64.encodestring(certificate_id.get_pem_cer(
                certificate_id.cer_file)).decode('UTF-8')
            key_pem = base64.encodestring(certificate_id.get_pem_key(
                certificate_id.key_file, certificate_id.password)).decode('UTF-8')
            try:
                client = Client(wsdl_url, cache=None)
            except:
                inv.message_post(body=_(
                    'Revisa tu conexion a internet y los datos del PAC'))
                continue
            taxpayer_id = inv.company_emitter_id.partner_id.vat
            folio_cancel = inv.cfdi_folio_fiscal
            uuids.append(folio_cancel)
            uuids_list = client.factory.create("UUIDS")
            uuids_list.uuids.string = uuids
            result = client.service.cancel(
                uuids_list, pac_usr, pac_pwd, taxpayer_id, cer_pem, key_pem)
            time.sleep(1)
            if 'Folios' not in result:
                msg += _('%s' % result)
                inv.message_post(body=_('Mensaje %s') % (msg))
                continue
            estatus_uuid = result.Folios[0][0].EstatusUUID
            if estatus_uuid in ('201', '202'):
                msg += _(
                    '\n- El proceso de cancelación se ha completado '
                    'correctamente.\n El uuid cancelado es: ') + folio_cancel
                self.cfdi_fecha_cancelacion = time.strftime(
                    '%Y-%m-%d %H:%M:%S')
            else:
                inv.message_post(body=_('Mensaje %s %s Code: %s') % (
                    msg, result.Folios[0][0].EstatusCancelacion,
                    estatus_uuid))
            inv.message_post(body=msg)
            if 'Acuse' in result:
                cname = 'ACUSE_CANCELACION_' + inv.move_name + '.xml'
                self.env['ir.attachment'].create({
                    'name': cname,
                    'datas_fname': cname,
                    'datas': base64.encodestring(str(
                        result.Acuse).encode()),
                    'res_model': 'account.invoice',
                    'res_id': inv.id,
                })

    @api.multi
    def cfdi_etree(self):
        fname = '%s_%s.xml' % (self.company_emitter_id.partner_id.vat,
                               self.number)
        att_xml_id = self.env['ir.attachment'].search([
            ('name', '=', fname),
            ('datas_fname', '=', fname),
            ('res_model', '=', 'account.invoice'),
            ('res_id', '=', self.id),
        ], limit=1)
        if not len(att_xml_id) >= 1:
            return False
        return objectify.fromstring(base64.decodestring(att_xml_id.datas))

    @api.multi
    def retrive_attachment(self):
        self.ensure_one()
        return self.env['ir.attachment'].search([
            ('name', '=', self.cfdi_name),
            ('datas_fname', '=', self.cfdi_name),
            ('res_model', '=', 'account.invoice'),
            ('res_id', '=', self.id)], limit=1)

    @api.multi
    def sign_cfdi(self):
        for invoice in self:
            attachment_xml_id = invoice.retrive_attachment()
            if not attachment_xml_id:
                invoice.create_cfdi()
                attachment_xml_id = invoice.retrive_attachment()
                if not attachment_xml_id:
                    invoice.message_post(body=_(
                        'No se encuentra XML para timbrar !'))
                    continue
            pac_params_ids = self.env['params.pac'].search([
                ('method_type', 'ilike', 'firmar'), (
                    'company_id', '=', invoice.company_emitter_id.id), (
                        'active', '=', True)], limit=1)
            if not pac_params_ids:
                invoice.message_post(body=_(
                    'No tienes parametros del PAC configurados'))
                continue
            pac_usr = pac_params_ids.user
            pac_pwd = pac_params_ids.password
            pac_url = pac_params_ids.url_webservice
            client = Client(pac_url, cache=None)
            xml = attachment_xml_id.datas
            resultado = client.service.stamp(xml.decode(), pac_usr, pac_pwd)
            if resultado.Incidencias:
                code = getattr(resultado.Incidencias[0][0], 'CodigoError', None)
                msg = getattr(
                    resultado.Incidencias[0][0],
                    'MensajeIncidencia' if code != '301' else 'ExtraInfo', None)
                invoice.message_post(body=_(' %s - %s ' % (code, msg)))
                continue

            if not resultado.Incidencias or None:
                cfdi_xml = resultado.xml.encode(
                    'ascii', 'xmlcharrefreplace') or ''
                attachment_xml_id.write({
                    'datas': base64.encodestring(cfdi_xml)})
                data_cfdi = {
                    'sign': True,
                    'cfdi_folio_fiscal': resultado['UUID'],
                    'cfdi_cadena_original': self._create_original_str(resultado),
                    'cfdi_fecha_timbrado': resultado['Fecha'],
                    'cfdi_no_certificado': resultado['NoCertificadoSAT'],
                    # 'no_certificado': certificate.serial_number,
                    # 'sello': sello,
                    'cfdi_sello': resultado['SatSeal'],
                }
                self.write(data_cfdi)
        return self.env.ref('l10n_mx_facturae_33.custom_invoice_pdf').render_qweb_pdf([self.id])[0]
        # return self.env['report'].get_action(self,'l10n_mx_facturae_33.invoice_pdf')

    @api.multi
    def action_move_create(self):
        res = super(AccountInvoice, self).action_move_create()
        self.create_cfdi()
        return res

    @api.multi
    def check_required_values(self):
        self.ensure_one()
        msg = []
        if not self.company_id.certificate_id:
            msg.append('No tienes definido certificado para esta compañia !')
        if not self.uso_cfdi_id.name:
            msg.append('La factura no tiene UsoCFDI !')
        if not self.met_pago_id.name:
            msg.append('La factura no tiene Método de Pago !')
        if not self.forma_pago_id.name:
            msg.append('La factura no tiene Forma de Pago !')
        if not self.company_emitter_id.partner_id.property_account_position_id:
            msg.append('La Empresa no tiene Regimen Fiscal !')
        for line in self.invoice_line_ids.filtered(
                lambda l: not l.claveserv_id):
            msg.append('Producto %s no tiene ClaveServ SAT !' % line.product_id.name)  # noqa
        if self.tax_line_ids.mapped('tax_id').filtered(
                lambda l: not l.code_sat):
            msg.append(
                'El impuesto no tiene clave del SAT !\npor disposición '
                'del SAT los impuestos deben de catalogarse en ISR, IVA o '
                'IEPS, para ligar su tipo de impuesto configure estos en: '
                'Contabilidad -> Configuración -> Impuestos -> Impuestos, '
                'edite el impuesto y escoja el tipo en el campo '
                'llamado "Código SAT"')
        if msg:
            msgs = ''
            for item in msg:
                msgs += '<li>' + item + '</li>'
            self.message_post(body=_('<ul>' + msgs + '</ul>'))
            return False
        return True

    @api.multi
    def create_cfdi(self):
        all_paths = tools.config["addons_path"].split(",")
        for my_path in all_paths:
            if os.path.isdir(os.path.join(
                    my_path, 'l10n_mx_facturae_33', 'SAT')):
                path_cadena = os.path.join(
                    my_path, 'l10n_mx_facturae_33', 'SAT',
                    'cadenaoriginal_3_3.xslt')
        type_invoice = 'I'
        for invoice in self.filtered(
                lambda i: i.journal_id.fiscal and i.check_required_values()):
            certificate_id = invoice.company_id.certificate_id
            invoice_datetime = invoice.date_invoice_tz_33.strftime(
                '%Y-%m-%dT%H:%M:%S')
            if invoice.type == 'out_invoice':
                type_invoice = 'I'
                tiporelacion = '04'
            if invoice.type == 'out_refund':
                type_invoice = 'E'
                tiporelacion = '01'

            lines = []
            tax_trasladados = False
            tax_retencion = False
            total_tax_tras = total_tax_ret = 0
            tax_traslados_list = []
            tax_ret_list = []
            drop_tax = False
            total_discount = subtotal = discount = total_tax = 0
            tax_traslados_list = []
            tax_ret_list = []
            moneda = 'MXN'
            tipo_cambio = '1'
            if invoice.currency_id and invoice.currency_id.name:
                moneda = invoice.currency_id.name
            if str(moneda) != 'MXN':
                tipo_cambio = str("%.2f" % (
                    1 / invoice.currency_id.rate))

            for line in invoice.invoice_line_ids:
                discount = 0
                drop_tax = True
                nom_uom = line.uom_id.name or 'pieza'

                line_subtotal = round(line.price_subtotal * 100 / (
                    100 - line.discount), 2)
                if line.discount:
                    discount = round(line.discount / 100 * line_subtotal, 2)
                line_dic = {
                    'cantidad': "%.2f" % (line.quantity),
                    'unidad': nom_uom,
                    'noIdentificacion': str(line.product_id.default_code),
                    'descripcion': str(conv_ascii(line.name)),
                    'valorUnitario': round(line_subtotal/line.quantity, 2),
                    'importe': "%.2f" % (line_subtotal),
                    'descuento': "%.2f" % (discount),
                    'ClaveUnidad': line.uom_id.claveuni_id.clave or 'EA',
                    'ClaveProdServ': line.claveserv_id.name,
                    'Impuestos': {
                        'Traslados': [],
                        'Retenciones': [],
                    },
                }
                total_discount += discount
                subtotal += line_subtotal
                for tax in line.invoice_line_tax_ids:
                    total_tax += 1
                    drop_tax = False
                    add = True
                    if tax.amount >= 0:
                        tax_trasladados = True
                        line_dic['Impuestos']['Traslados'].append({
                            'impuesto': tax.code_sat,
                            'tasaocuota': "%.6f" % (tax.amount/100),
                            'tipofactor': 'Tasa',
                            'importe': "%.2f" % (
                                line.price_subtotal * (tax.amount / 100)),
                            'base': "%.2f" % (line.price_subtotal),
                        })
                        if not tax_traslados_list:
                            tax_traslados_list.append({
                                'impuesto': tax.code_sat,
                                'tasaocuota': "%.6f" % (tax.amount/100),
                                'importe': "%.2f" % (line.price_subtotal * (
                                    tax.amount / 100)),
                                'tipofactor': 'Tasa',
                            })
                        else:
                            for t in tax_traslados_list:
                                if t['impuesto'] == tax.code_sat and "%.6f" % float(t['tasaocuota']) == "%.6f"%float(tax.amount/100):
                                    add = False
                            if add:
                                tax_traslados_list.append({
                                    'impuesto': tax.code_sat,
                                    'tasaocuota': "%.6f" % (tax.amount/100),
                                    'importe': "%.2f" % (line.price_subtotal*(tax.amount/100)),
                                    'tipofactor': 'Tasa',
                                })
                            else:
                                for t in tax_traslados_list:
                                    if t['impuesto'] == tax.code_sat and "%.6f" % float(
                                            t['tasaocuota']) == "%.6f" % float(
                                                tax.amount/100):
                                        before = t['importe']
                                        t['importe'] = "%.2f" % (
                                            float(before) + line.price_subtotal * (tax.amount/100))  # noqa

                        total_tax_tras += round(line.price_subtotal * (
                            tax.amount/100), 2)
                    if tax.amount < 0:
                        tax_retencion = True
                        line_dic['Impuestos']['Retenciones'].append({
                            'impuesto': tax.code_sat,
                            'tasaocuota': "%.6f" % (abs(tax.amount/100)),
                            'tipofactor': 'Tasa',
                            'importe': "%.2f" % (abs(line.price_subtotal * (
                                tax.amount/100))),
                            'base': "%.2f" % (line.price_subtotal),
                        })
                        if not tax_ret_list:
                            tax_ret_list.append({
                                'impuesto': tax.code_sat,
                                'tasaocuota': "%.6f" % (abs(tax.amount/100)),
                                'importe': "%.2f" % (abs(
                                    line.price_subtotal * (tax.amount/100))),
                                'tipofactor': 'Tasa',
                            })
                        else:
                            for t in tax_ret_list:
                                if t['impuesto']==tax.code_sat and "%.6f" %float(t['tasaocuota'])== "%.6f"%float(tax.amount/100):
                                    add=False
                            if add:
                                tax_ret_list.append({
                                    'impuesto': tax.code_sat,
                                    'tasaocuota': "%.6f" % (abs(
                                        tax.amount / 100)),
                                    'importe': "%.2f" % (abs(line.price_subtotal*(tax.amount/100))),
                                    'tipofactor': 'Tasa',
                                })
                            else:
                                for t in tax_ret_list:
                                    if t['impuesto']==tax.code_sat and "%.6f" %float(t['tasaocuota'])== "%.6f"%float(tax.amount/100):
                                        before=t['importe']
                                        t['importe']="%.2f" % (float(before)+ line.price_subtotal*(tax.amount/100))
                        total_tax_ret += (abs(line.price_subtotal*(tax.amount/100)))
                if not tax_trasladados:
                    line_dic['Impuestos'].pop('Traslados')
                if not tax_retencion:
                    line_dic['Impuestos'].pop('Retenciones')
                if drop_tax:
                    line_dic.pop('Impuestos')

                lines.append(line_dic)
            data = {
                'Atributos': {
                    'serie': str(invoice.journal_id.sequence_id.serie or ''),
                    'folio': invoice.number,
                    'fecha': invoice_datetime,
                    'sello': '',
                    'formaDePago': invoice.forma_pago_id.name,
                    'noCertificado': certificate_id.serial_number,
                    'certificado': certificate_id.sudo().get_data()[0].decode(),
                    'condicionesDePago': '',
                    'subTotal': "%.2f" % (subtotal),
                    'descuento': "%.2f" % (total_discount),
                    'motivoDescuento': '',
                    'TipoCambio': tipo_cambio,
                    'Moneda': moneda,
                    'total': "%.2f" % (
                        subtotal - total_discount + total_tax_tras -total_tax_ret),  # noqa
                    'tipoDeComprobante': type_invoice,
                    'metodoDePago': invoice.met_pago_id.name,
                    'LugarExpedicion': invoice.address_issued_id.zip,
                },
                'Emisor': {
                    'rfc': invoice.company_emitter_id.partner_id.vat or '',
                    'nombre': invoice.company_emitter_id.name,
                    'regimenFiscal': invoice.company_emitter_id.partner_id.property_account_position_id.code,  # noqa
                },
                'Receptor': {
                    'rfc': invoice.partner_id._get_vat_partner(),
                    'nombre': invoice.partner_id.name,
                    'UsoCFDI':  invoice.uso_cfdi_id.name,
                },
                'Conceptos': lines,
                'Impuestos': {},
            }
            if total_tax > 0:
                if tax_traslados_list:
                    data['Impuestos'] = {'totalImpuestosTrasladados': "%.2f" %(total_tax_tras)}
                    data['Impuestos']['Traslados']=tax_traslados_list
                if tax_ret_list:
                    data['Impuestos'].update({'totalImpuestosRetenidos': "%.2f" %(total_tax_ret)})
                    data['Impuestos'].update({'Retenciones':tax_ret_list})
            else:
                data.pop('Impuestos')
            if invoice.invoice_rel_id:
                data['CfdiRelacionados'] = {
                    'tiporelacion': tiporelacion,
                    'CfdiRelacionado': {
                        'uuid': invoice.invoice_rel_id.cfdi_folio_fiscal}}

            # self.validate_data(data,cer, key, pass_key)
            env = Environment(
                loader=FileSystemLoader(
                    os.path.join(
                        os.path.dirname(os.path.abspath(__file__)), '../template',),),
                undefined=StrictUndefined, autoescape=True,
            )
            template = env.get_template('cfdi.jinja')
            xml = template.render(data=data).encode('utf-8')
            cadena = self.generate_cadena_original(
                xml, {'path_cadena': path_cadena})
            sello = certificate_id.get_sello(cadena)
            tree = objectify.fromstring(xml)
            tree.attrib['Sello'] = sello
            xml = etree.tostring(
                tree, pretty_print=True,
                xml_declaration=True, encoding='UTF-8')
            fname = '%s_%s.xml' % (invoice.company_emitter_id.partner_id.vat,
                                   invoice.number)
            attachment = self.env['ir.attachment'].create({
                'name': fname,
                'datas': base64.b64encode(xml),
                'datas_fname': fname,
                'res_model': 'account.invoice',
                'res_id': invoice.id,
            })
            invoice.message_post(
                body=_('CFDI generated'),
                attachment_ids=[attachment.id],
                subtype='account.mt_invoice_validated')
            invoice.cfdi_name = fname
        return True

    @classmethod
    def _create_original_str(self, result):
        cfdi_folio_fiscal = result.UUID or ''
        cfdi_fecha_timbrado = result.Fecha or ''
        sello = result.SatSeal or ''
        cfdi_no_certificado = result.NoCertificadoSAT or ''
        original_string = '||1.1|' + cfdi_folio_fiscal + '|' + str(
            cfdi_fecha_timbrado) + '|' + sello + '|' \
            + cfdi_no_certificado + '||'
        return original_string

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        super()._onchange_partner_id()
        self.uso_cfdi_id = self.partner_id.uso_cfdi_id.id or False

    @classmethod
    def validate_data(self, data, cer, key, pwd):
        '''
        This method will be used to validate correct input.
        @param data: python dictionary with invoide data.
        '''
        if cer and key and pwd != '':
            data['Atributos']['certificado'] = cer
            data['Atributos']['noCertificado'] = self.get_noCertificado(cer)

    @classmethod
    def generate_cadena_original(self, xml, context=None):
        xlst_file = tools.file_open(context.get('path_cadena', '')).name
        dom = etree.fromstring(xml)
        xslt = etree.parse(xlst_file)
        transform = etree.XSLT(xslt)
        return str(transform(dom))

    @classmethod
    def base64_to_tempfile(self, b64_str=None, suffix=None, prefix=None):
        """ Convert strings in base64 to a temp file
        @param b64_str : Text in Base_64 format for add in the file
        @param suffix : Sufix of the file
        @param prefix : Name of file in TempFile
        """
        (fileno, file_name) = tempfile.mkstemp(suffix, prefix)
        f_read = open(file_name, 'wb')
        f_read.write(base64.decodestring(b64_str))
        f_read.close()
        os.close(fileno)
        return file_name

    @classmethod
    def stamp_finkok(self, xml, context=None):
        pwd = context.get('pac_pwd', "")
        usr = context.get('pac_usr', "")
        url = context.get('pac_url', "")
        client = Client(url, cache=None)
        xml = base64.encodestring(xml)
        res = client.service.stamp(xml, usr, pwd)
        return res

    @api.model
    def _get_stamp_data(self, cfdi):
        self.ensure_one()
        if not hasattr(cfdi, 'Complemento'):
            return None
        attribute = 'tfd:TimbreFiscalDigital[1]'
        namespace = {'tfd': 'http://www.sat.gob.mx/TimbreFiscalDigital'}
        node = cfdi.Complemento.xpath(attribute, namespaces=namespace)
        return node[0] if node else None

    @api.multi
    def action_invoice_draft(self):
        records = self
        for inv in self.filtered('journal_id.fiscal'):
            att = inv.retrive_attachment()
            if not att:
                continue
            status = inv.valida_xml(att.datas).get('acuse')
            if status and status.Estado == 'Cancelado':
                continue
            if status and status.Estado != 'Cancelado':
                inv.action_cancel_cfdi()
            status = inv.valida_xml(att.datas).get('acuse')
            inv.sat_status = status and status.Estado or ''
            if inv.sat_status != 'Cancelado':
                records -= inv
                inv.message_post(body=_(
                    'This invoice is not cancelled in the SAT system'))
        return super(AccountInvoice, records).action_invoice_draft()

    def valida_xml(self, xml_file):
        url = 'https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc?wsdl'  # noqa
        try:
            client = Client(url)
            (fileno, fname) = tempfile.mkstemp(".xml", "invoice_tmp")
            f = open(fname, 'wb' )
            f.write(base64.decodestring(xml_file or '' ))
            f.close()
            os.close(fileno)
            fo = open(fname, "r")
            tree = objectify.fromstring(fo.read().encode())
            tfd = self._get_stamp_data(tree)
            rfc_emisor = tree.Emisor.get('Rfc', '').replace('&', '&amp;amp;')
            rfc_receptor = tree.Receptor.get('Rfc', '').replace(
                '&', '&amp;amp;')
            ffiscal = tfd.get('UUID')
            total = tree.get('Total')
            acuse = """"?re=%s&rr=%s&tt=%f&id=%s""" % (
                rfc_emisor, rfc_receptor, float(total), ffiscal)
            res = client.service.Consulta(acuse)
            dict_data = {}
            # dict_data = dict(xmltodict.parse(base64.decodestring(
            #     xml_file)).get('cfdi:Comprobante', {}))
            return {'acuse': res, 'comprobante': dict_data}
        except:
            try:
                # dict_data = dict(xmltodict.parse(
                #     base64.decodestring(xml_file)).get('cfdi:Comprobante', {}))
                dict_data = {}
            except:
                dict_data = {}
            return {'acuse': False, 'comprobante': dict_data}


class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    claveserv_id = fields.Many2one('clave.prod.serv', 'Clave servicio')

    @api.onchange('product_id')
    def _onchange_product_id(self):
        super(AccountInvoiceLine, self)._onchange_product_id()
        self.claveserv_id = self.product_id.claveserv_id.id or \
            self.product_id.product_tmpl_id.claveserv_id.id or \
            self.product_id.categ_id.claveserv_id.id or False


class ir_attachment(models.Model):

    _inherit = 'ir.attachment'

    @api.multi
    def unlink(self):
        invoice = self.env['account.invoice']
        for att in self.filtered(lambda a: a.res_model == 'account.invoice' and
                                 '.xml' in a.name):
            if invoice.browse(att.res_id).sign:
                raise UserError(_(
                    'No puedes eliminar un adjunto si la factura fue timbrada')
                )
        return super(ir_attachment, self).unlink()
