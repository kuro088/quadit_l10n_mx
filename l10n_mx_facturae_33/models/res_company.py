# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.


import base64
import time
import ssl
import subprocess
import tempfile

from datetime import datetime
from OpenSSL import crypto
from pytz import timezone

from odoo import api, fields, models, tools, _

from odoo.exceptions import ValidationError
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT

KEY_TO_PEM_CMD = 'openssl pkcs8 -in %s -inform der -outform pem -out %s -passin file:%s'


def convert_key_cer_to_pem(key, password):
    # TODO compute it from a python way
    with tempfile.NamedTemporaryFile('wb', suffix='.key', prefix='edi.mx.tmp.') as key_file, \
            tempfile.NamedTemporaryFile('wb', suffix='.txt', prefix='edi.mx.tmp.') as pwd_file, \
            tempfile.NamedTemporaryFile('rb', suffix='.key', prefix='edi.mx.tmp.') as keypem_file:
        key_file.write(key)
        key_file.flush()
        pwd_file.write(password)
        pwd_file.flush()
        subprocess.call((KEY_TO_PEM_CMD % (key_file.name, keypem_file.name, pwd_file.name)).split())
        key_pem = keypem_file.read()
    return key_pem


class ResCompanyFacturaeCertificate(models.Model):
    """Object use to save the CSD files"""
    _name = 'res.company.facturae.certificate'
    _description = __doc__
    _rec_name = 'serial_number'

    company_id = fields.Many2one(
        'res.company', required=True,
        help='Company where you add this certificate',
        default=lambda self: self.env.user.company_id)
    cer_file = fields.Binary(
        required=True, help='This file .cer is proportionate by the SAT')
    key_file = fields.Binary(
        required=True, help='This file .key is proportionate by the SAT')
    password = fields.Char(
        required=True, help='This password is proportionate by the SAT')
    date_start = fields.Date(
        required=False,
        default=lambda *a: time.strftime('%Y-%m-%d'),
        help='Date start the certificate before the SAT')
    date_end = fields.Date(
        help='Date end of validity of the certificate')
    serial_number = fields.Char(
        help='Number of serie of the certificate')
    fname_xslt = fields.Char(
        'File XML Parser (.xslt)', size=256,
        required=False, help='Folder in server with XSLT file', )
    active = fields.Boolean(
        help='Indicate if this certificate is active', default=True)

    @api.one
    def get_certificate_info(self):
        cer_der_b64str = self.certificate_file
        key_der_b64str = self.certificate_key_file
        password = self.certificate_password
        data = self.onchange_certificate_info(
            cer_der_b64str, key_der_b64str, password)
        if data['warning']:
            raise ValidationError(data['warning']['message'])
        self.write(data['value'])

    @api.multi
    @tools.ormcache('content')
    def get_pem_cer(self, content):
        '''Get the current content in PEM format
        '''
        self.ensure_one()
        return ssl.DER_cert_to_PEM_cert(base64.decodestring(content)).encode(
            'UTF-8')

    @api.multi
    @tools.ormcache('key', 'password')
    def get_pem_key(self, key, password):
        """Get the current key in PEM format"""
        self.ensure_one()
        return convert_key_cer_to_pem(base64.decodestring(key), password.encode('UTF-8'))

    @api.multi
    def get_data(self):
        '''Return the content (b64 encoded) and the certificate decrypted
        '''
        self.ensure_one()
        cer_pem = self.get_pem_cer(self.cer_file)
        certificate = crypto.load_certificate(crypto.FILETYPE_PEM, cer_pem)
        for to_del in ['\n', ssl.PEM_HEADER, ssl.PEM_FOOTER]:
            cer_pem = cer_pem.replace(to_del.encode('UTF-8'), b'')
        return cer_pem, certificate

    @api.constrains('cer_file', 'key_file', 'password')
    def _check_credentials(self):
        '''Check the validity of content/key/password and fill the fields
        with the certificate values.
        '''
        mexican_tz = timezone('America/Mexico_City')
        # mexican_dt = self.get_mx_current_datetime()
        date_format = '%Y%m%d%H%M%SZ'
        for record in self:
            # Try to decrypt the certificate
            try:
                cer_pem, certificate = record.get_data()
                before = mexican_tz.localize(
                    datetime.strptime(certificate.get_notBefore().decode("utf-8"), date_format))
                after = mexican_tz.localize(
                    datetime.strptime(certificate.get_notAfter().decode("utf-8"), date_format))
                serial_number = certificate.get_serial_number()
            except Exception:
                raise ValidationError(_('The certificate content is invalid.'))
            # Assign extracted values from the certificate
            record.serial_number = ('%x' % serial_number)[1::2]
            record.date_start = before.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            record.date_end = after.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            # if mexican_dt > after:
            #     raise ValidationError(_('The certificate is expired since %s') % record.date_end)
            # Check the pair key/password
            try:
                key_pem = self.get_pem_key(self.key_file, self.password)
                crypto.load_privatekey(crypto.FILETYPE_PEM, key_pem)
            except Exception:
                raise ValidationError(_(
                    'The certificate key and/or password is/are invalid.'))

    @api.model
    def get_sello(self, cadena):
        key_pem = self.get_pem_key(self.key_file, self.password)
        private_key = crypto.load_privatekey(crypto.FILETYPE_PEM, key_pem)
        encrypt = 'sha256WithRSAEncryption'
        cadena_crypted = crypto.sign(private_key, cadena, encrypt)
        return base64.b64encode(cadena_crypted)


class ResCompany(models.Model):
    _inherit = 'res.company'

    @api.multi
    @api.depends('certificate_ids')
    def _get_current_certificate(self):
        self.ensure_one()
        res = {}
        certificate_obj = self.env['res.company.facturae.certificate']
        date = time.strftime('%Y-%m-%d')
        context = dict(self.env.context or {})
        if 'date_work' in context:
            # Si existe este key, significa, que no viene de un function, si no
            # de una invocacion de factura
            date = context['date_work']
            if not date:
                # Si existe el campo, pero no esta asignado, significa que no fue por un function, y que no se requiere la current_date
                # print "NOTA: Se omitio el valor de date"
                return res
        certificate_ids = certificate_obj.search(
            [('company_id', '=', self.id),
             ('date_start', '<=', date),
             ('date_end', '>=', date),
             ('active', '=', True)], limit=1)
        self.certificate_id = certificate_ids.id

    certificate_ids = fields.One2many(
        'res.company.facturae.certificate', 'company_id', 'Certificates',
        help='Certificates configurated in this company')
    certificate_id = fields.Many2one(
        'res.company.facturae.certificate',
        string='Certificate Current', help='Serial Number of the certificate '
        'active and inside of dates in this company',
        compute='_get_current_certificate')
    invoice_out_sequence_id = fields.Many2one(
        'ir.sequence', 'Invoice Out Sequence',
        help="The sequence used for invoice out numbers.")
    invoice_out_refund_sequence_id = fields.Many2one(
        'ir.sequence',
        help="The sequence used for invoice out refund numbers.")
