# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    forma_pago_id = fields.Many2one('res.forma.pago', 'Forma de pago')
    met_pago_id = fields.Many2one('res.met.pago', 'Metodo de pago')

    @api.multi
    def _prepare_invoice(self):
        res= super(SaleOrder, self)._prepare_invoice()
        if self.partner_id.uso_cfdi_id:
            res.update({'uso_cfdi_id':self.partner_id.uso_cfdi_id.id})
        res.update({'forma_pago_id':self.forma_pago_id and self.forma_pago_id.id or False})
        res.update({'met_pago_id':self.forma_pago_id and self.forma_pago_id.id or False})
        return res

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'


    @api.multi
    def _prepare_invoice_line(self,qty):
        res= super(SaleOrderLine, self)._prepare_invoice_line(qty=qty)
        res.update({'claveserv_id':self.product_id.claveserv_id and
        self.product_id.claveserv_id.id or
        self.product_id.product_tmpl_id.claveserv_id and
        self.product_id.product_tmpl_id.claveserv_id.id or
        self.product_id.categ_id.claveserv_id and self.product_id.categ_id.claveserv_id.id or False})

        return res
