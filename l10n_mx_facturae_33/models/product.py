# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import api, fields, models


class ClaveProdUom(models.Model):
    """Object to hold the SAT catalog to UoMs"""
    _name = 'clave.prod.uom'
    _description = __doc__
    _rec_name = 'clave'

    @api.depends('clave', 'name')
    def name_get(self):
        result = []
        for pay in self:
            name = pay.clave + '-' + pay.name
            result.append((pay.id, name))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        recs = self.browse()
        if name:
            recs = self.search([('name', operator, name)] + args, limit=limit)
        if not recs:
            recs = self.search([('name', operator, name)] + args, limit=limit)
        return recs.name_get()

    clave = fields.Char(size=3, required=True)
    name = fields.Char(size=128, required=True)
    simbol = fields.Char(size=32)
    description = fields.Text()

    _sql_constraints = [
        ('clave_unique', 'UNIQUE (clave)', 'La clave debe ser unica')]


class ClaveProdServ(models.Model):
    """Object to hold the SAT catalog to products"""
    _name='clave.prod.serv'
    _description = __doc__

    @api.depends('name', 'description')
    def name_get(self):
        result = []
        for pay in self:
            name = pay.name + '-' + pay.description
            result.append((pay.id, name))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        recs = self.browse()
        if name:
            recs = self.search([('description', operator, name)] + args, limit=limit)
        if not recs:
            recs = self.search([('name', operator, name)] + args, limit=limit)
        return recs.name_get()

    name = fields.Char(size=32, required=True)
    description = fields.Text()
    iva_tras = fields.Selection([
        ('si', 'Si'),
        ('no', 'No'),
        ('opcional', 'Opcional') ],
        'Incluir IVA trasladado',default='opcional')
    ieps_tras = fields.Selection([
        ('si', 'Si'),
        ('no', 'No'),
        ('opcional', 'Opcional') ],
        'Incluir IEPS trasladado',default='no')

    _sql_constraints= [('name_unique','UNIQUE (name)','La clave debe ser unica')]


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    claveserv_id = fields.Many2one('clave.prod.serv','ClaveProdServ SAT')

class ProductCategory(models.Model):
    _inherit = 'product.category'

    claveserv_id = fields.Many2one('clave.prod.serv','ClaveProdServ SAT')


class UomUom(models.Model):
    _inherit = 'uom.uom'

    claveuni_id = fields.Many2one('clave.prod.uom','ClaveUnidad SAT')
