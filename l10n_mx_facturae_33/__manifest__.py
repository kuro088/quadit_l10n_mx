# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    "name": "l10n_mx_facturae_33",
    "version": "12.0.1.0.0",
    "author": "Quadit, S.A. de C.V.",
    "category": "account",
    "description": """
    Module create new invoice xml version 3.3 SAT.
    Add catalog of new keys and values of SAT""",
    'website': 'https://www.quadit.mx',
    "license": "OEEL-1",
    "depends": [
        'account',
        'sale',
    ],
    "demo": [],
    "data": [
        'views/invoice_view.xml',
        'views/account_view.xml',
        'views/sale_view.xml',
        'views/res_company_view.xml',
        'views/res_partner_view.xml',
        'views/product_view.xml',
        'views/params_pac_view.xml',
        'report/invoice_pdf_view.xml',
        'report/invoice_view.xml',
        'data/res.uso.cfdi.csv',
        'data/res.forma.pago.csv',
        'data/clave.prod.uom.csv',
        'data/clave.prod.serv.csv',
        'data/res.met.pago.csv',
        'security/groups.xml',
        'security/ir.model.access.csv',
    ],
    "installable": True,
}
