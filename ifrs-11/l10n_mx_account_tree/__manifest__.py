# -*- coding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    "name": "Account Parent / Children Tree Structure",
    "version": "11.0.1.0.0",
    "depends": [
        "account", "analytic"
    ],
    "author": "Quadit, S.A. de C.V.",
    'website': 'https://www.quadit.mx',
    "category": "Accounting",
    "demo": [],
    "test": [],
    "data": [
        "view/account_view.xml",
        "view/analytic_view.xml",
    ],
    "application": False,
    "active": False,
    "installable": True,
}
