# -*- coding: utf-8 -*-
#from openerp.osv import expression, osv
#from openerp.tools.float_utils import float_round as round
#from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.exceptions import UserError, ValidationError
from openerp import api, fields, models, _





class AccountAnalyticAccount(models.Model):
    _inherit = 'account.analytic.account'
        
    @api.multi
    def _get_children(self, ids=[]):
        #this function search for all the children and all consolidated children (recursively) of the given account ids
        ids = list(ids)
        res = []
        record = self.search([('parent_id', 'child_of', ids)])
        ids3 = []
        for rec in record:
            res.append(rec.id)
        return res    
    
    @api.one
    @api.depends('child_parent_ids')
    def _get_child_ids(self):
        self.ensure_one()
        result = []
        if self.child_parent_ids:
            for record in self.child_parent_ids:
                result.append(record.id)
        self.child_id = result
        
        
    @api.one
    def __compute_argil(self):        
        analytic_line_obj = self.env['account.analytic.line']
        children_ids = self._get_children(self._ids)
        domain = [('account_id', 'in', (tuple(children_ids,)))]
        if self._context.get('date_from', False):
            domain.append(('date', '>=', self._context['date_from']))
        if self._context.get('date_to', False):
            domain.append(('date', '<=', self._context['date_to']))

        account_amounts = analytic_line_obj.search_read(domain, ['account_id', 'amount'])
        debit = 0.0
        credit = 0.0
        for account_amount in account_amounts:            
            if account_amount['amount'] < 0.0:
                credit += abs(account_amount['amount'])
            else:
                debit += account_amount['amount']

        self.argil_debit = debit
        self.argil_credit = credit
        self.argil_balance = debit - credit        
        
    parent_id = fields.Many2one('account.analytic.account', string='Parent', required=False, index=True)
    child_parent_ids  = fields.One2many('account.analytic.account','parent_id', string='Children')
    child_id          = fields.Many2many('account.analytic.account', compute=_get_child_ids,  string="Child Accounts", store=False)
    argil_debit   = fields.Monetary(compute=__compute_argil, string='Debit')
    argil_credit  = fields.Monetary(compute=__compute_argil, string='Credit')
    argil_balance = fields.Monetary(compute=__compute_argil, string='Balance')



    
class AccountChartOfAnalyticsArgilWizard(models.TransientModel):    
    """
    For Chart of Analytics
    """
    _name = "account.analytic.chart_argil"
    _description = "Account Chart Of Analytics Argil"
        
        
    date_from   = fields.Date(string='From', required=False)
    date_to     = fields.Date(string='To', required=False)
    
    
        
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
