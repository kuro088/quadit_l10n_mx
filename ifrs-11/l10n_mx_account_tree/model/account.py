# -*- coding: utf-8 -*-
#

#from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.exceptions import UserError, ValidationError
from openerp import api, fields, models, _



class AccountAccountType(models.Model):
    _inherit = "account.account.type"

    type = fields.Selection([('view', 'View'),
                             ('consolidation', 'Consolidation'),
                             ('other', 'Regular'),
                             ('receivable', 'Receivable'),
                             ('payable', 'Payable'),
                             ('liquidity', 'Liquidity'),
                            ], required=True, default='other',
                            help="The 'Internal Type' is used for features available on "\
                            "different types of accounts: liquidity type is for cash or bank accounts"\
                            ", payable/receivable is for vendor/customer accounts.")



class AccountAccount(models.Model):
    _inherit = "account.account"

    @api.one
    @api.depends('parent_id')
    def _get_level(self):
        self.ensure_one()
        level = 0
        parent = self.parent_id or False
        while parent:
            level += 1
            parent = parent.parent_id or False
        self.level = level

        
         
    @api.one
    @api.depends('child_parent_ids')
    def _get_child_ids(self):
        self.ensure_one()
        result = []
        if self.child_parent_ids:
            for record in self.child_parent_ids:
                result.append(record.id)            

        if self.child_consol_ids:
            for acc in self.child_consol_ids:
                if acc.id not in result:
                    result.append(acc.id)
        self.child_id = result
        

    @api.multi
    def _get_children_and_consol(self, ids=[]):
        #this function search for all the children and all consolidated children (recursively) of the given account ids
        ids = list(ids)
        res = []
        record = self.search([('parent_id', 'child_of', ids)])
        ids3 = []
        for rec in record:
            res.append(rec.id)
            for child in rec.child_consol_ids:
                res.append(child.id)
        #if record:
        #    res2 = self._get_children_and_consol()
        return res    
    
    
    @api.one
    def __compute_argil(self):
        query=''
        context = self._context.copy()
        #res = {}.fromkeys(ids, {}.fromkeys(field_names, 0))
        context2 = context.copy()
        periods = context.get('periods', False)
        
        if periods:
            subquery = """select p2.id from account_period p2
                            where p2.name2 < (select min(name2) from account_period p1
		                                      where id in (%s));
                """ % (str(periods).replace('[','').replace(']',''))
            self._cr.execute( subquery )
            period_ids = [period_id[0] for period_id in self._cr.fetchall() ]
            res2 = {}
            if period_ids:
                context2.update({'periods': period_ids, 'period_id': False})
                res2 = self.with_context(context2).__compute()
        res1 = self.__compute()
        self.argil_initial_balance = (periods and 'balance' in res2) and res2['balance'] or 0.0
        self.argil_balance_all = ('balance' in res1 and res1['balance'] or 0.0) + ((periods and 'balance' in res2) and res2['balance'] or 0.0) 
        self.debit  = 'debit' in res1 and res1['debit'] or 0.0
        self.credit = 'credit' in res1 and res1['credit'] or 0.0
        self.balance= 'balance' in res1 and res1['balance'] or 0.0
        
    
    argil_initial_balance = fields.Monetary(compute=__compute_argil, string='Initial Balance')
    argil_balance_all     = fields.Monetary(compute=__compute_argil, string='Balance All')
    balance               = fields.Monetary(compute=__compute_argil, string='Balance')
    credit                = fields.Monetary(compute=__compute_argil, string='Credit')
    debit                 = fields.Monetary(compute=__compute_argil, string='Debit')

    parent_id = fields.Many2one('account.account', string='Parent', required=False, index=True)
    sign      = fields.Selection([
                                  (1, 'Debitable'), 
                                  (-1, 'Creditable')], 
                                  string='MX Report Sign', required=True, default=1,
                                  help="Determines sign to use in reports")

    partner_breakdown = fields.Boolean(index=True, default=False, string='Desglosar Empresas en Balanza', 
                                       help= 'Si activa esta casilla se desglosará en la Balanza de Comprobación las empresas que conforman los cargos / abonos y saldos de esta cuenta')
    level             = fields.Integer(string='Level', store=True, readonly=True, compute=_get_level, method=True)

    
    child_parent_ids  = fields.One2many('account.account','parent_id', string='Children')
    child_consol_ids  = fields.Many2many('account.account', 'account_account_consol_rel', 'child_id', 'parent_id', string='Consolidated Children')
    child_id          = fields.Many2many('account.account', compute=_get_child_ids,  string="Child Accounts", store=False)

    
    @api.constrains('parent_id')
    def _check_parent_id_recursion(self):
        if not self._check_recursion():
            raise ValidationError(_('Error ! You cannot create recursive accounts.'))
        return True
    


class AccountChartOfAccountsArgilWizard(models.TransientModel):    
    """
    For Chart of Accounts
    """
    _name = "account.chart_argil"
    _description = "Account chart special"
    
    
    fiscalyear_id = fields.Many2one('account.fiscalyear', string='Periodo Anual', required=True,
                                   default=lambda self: self.env['account.fiscalyear'].search([('date_start', '<=', fields.Date.today()), ('date_stop', '>=', fields.Date.today()), ('company_id', '=', self.env.user.company_id.id)], limit=1))
    period_from   = fields.Many2one('account.period', string='Periodo Inicial', required=True,
                                   default=lambda self: self.env['account.period'].search([('date_start', '<=', fields.Date.today()), ('date_stop', '>=', fields.Date.today()), ('company_id', '=', self.env.user.company_id.id)], limit=1))
    
    period_to     = fields.Many2one('account.period', string='Periodo Final',required=True,
                                   default=lambda self: self.env['account.period'].search([('date_start', '<=', fields.Date.today()), ('date_stop', '>=', fields.Date.today()), ('company_id', '=', self.env.user.company_id.id)], limit=1))
    target_move   = fields.Selection([
                                        ('posted', 'All Posted Entries'),
                                        ('all', 'All Entries'),
                                ], string='Movimientos a incluir',required=True, default='posted')


    
    
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
