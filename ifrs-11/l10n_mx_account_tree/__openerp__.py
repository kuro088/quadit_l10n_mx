# -*- encoding: utf-8 -*-
#

{
    "name": "Account Parent / Children Tree Structure",
    "version": "1.0",
    "depends": [
        'account', 'analytic'
    ],
    "author": "German Ponce Dominguez",
    "description" : """
Account Parent / Children Tree Structure
========================================

This module adds some customization to Chart of Accounts Catalog and Chart of Accounts Analytics.

    """,
    "website": "http://poncesoft.blogspot.com",
    "category": "Accounting",
    "demo": [],
    "test": [],
    "data": [
        'view/account_view.xml',
        'view/analytic_view.xml',
    ],
    'application': False,
    "active": False,
    "installable": True,
}
