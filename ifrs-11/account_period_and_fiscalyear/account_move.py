# -*- coding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time
from openerp import api, fields, models, _
from openerp.exceptions import UserError


#----------------------------------------------------------
# Account Move
#----------------------------------------------------------

class AccountMove(models.Model):
    _inherit = "account.move"

    @api.multi
    def _check_lock_date(self):
        res = super(AccountMove, self)._check_lock_date()
        for move in self:
            if move.period_id.state == 'done':
                raise UserError(_("You cannot add/modify entries in Closed Period %s. Check the Period state" % (move.period_id.name)))
        return res

    @api.multi    
    @api.depends('date')
    def _compute_period(self):
        """Compute Period using Date Move.
        """
        for move in self:
            self._cr.execute("select id from account_period where '%s' between date_start and date_stop and company_id=%s;" % (move.date, move.company_id.id))
            res = self._cr.fetchall()
            if res:
                period_id = res[0][0]
                move.period_id = period_id
            else:
                raise UserError(_('There is no Period matching Account Move date.'))
    
    
    
    period_id = fields.Many2one('account.period', string='Period', readonly=True, required=False,
                compute='_compute_period', store=True)    
    

#----------------------------------------------------------
# Account Move Line
#----------------------------------------------------------

class AccountMoveLine(models.Model):
    _inherit = "account.move.line"


    period_id = fields.Many2one('account.period', string='Period', related='move_id.period_id',
                                store=True, readonly=True, copy=False)
