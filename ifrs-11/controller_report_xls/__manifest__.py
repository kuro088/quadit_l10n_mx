# -*- coding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    "name": "XLS Report Controller",
    "version": "11.0.1.0.0",
    "author": "Quadit, S.A. de C.V.",
    "category": "account",
    'website': 'https://www.quadit.mx',
    "license": "OEEL-1",
    "depends": ['web'],
    "demo": [],
    "data": [
    ],
    "test": [],
    "js": [],
    "css": [],
    "qweb": [],
    "installable": True,
    "auto_install": False,
}
