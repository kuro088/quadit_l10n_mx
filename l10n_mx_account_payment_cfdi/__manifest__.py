# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    "name": "l10n_mx_account_payment_cfdi",
    "version": "11.0.1.0.0",
    "author": "Quadit, S.A. de C.V.",
    "category": "account",
    "description": """create CFDI account_payment for SAT""",
    'website': 'https://www.quadit.mx',
    "license": "OEEL-1",
    "depends": ['account_payment', 'l10n_mx_facturae_33'],
    "demo": [],
    "data": [
        'views/account_view.xml',
        'views/account_payment_view.xml',
        'report/voucher_cfdi_pdf_view.xml',
        'report/voucher_cfdi_view.xml',
    ],
    "installable": True,
}
