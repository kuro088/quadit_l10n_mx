# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import api, fields, models, tools, _
from datetime import datetime,date
import base64
import xmltodict
import tempfile
import os
from collections import OrderedDict
try:
    from qrcode import QRCode, ERROR_CORRECT_L
except ImportError:
    _logger.debug('Can not import qrcode.')
try:
    import xmltodict
except ImportError:
    _logger.debug('Can not import xmltodict.')

class report_pdf(models.AbstractModel):
    # _name must be report.module_name.template_id
    _name = 'report.l10n_mx_account_payment_cfdi.voucher_pdf'

    def create_qrcode(self):
        rfc_emisor = ""
        rfc_receptor = ""
        folio_fiscal = ""
        amount_total = 0
        payment_obj = self.env['account.payment']
        payment_ids = self.env.context.get('active_ids')
        payment_rows =  payment_obj.browse(payment_ids[0])
        if payment_rows.company_id.vat:
            rfc_emisor = payment_rows.company_id.vat[2:]
        if payment_rows.partner_id.vat:
            rfc_receptor = payment_rows.partner_id.vat[2:]
        amount_total = payment_rows.amount
        if payment_rows.name:
            folio_fiscal = payment_rows.name
        #
        amount_total = str.zfill("%0.6f" % amount_total, 17)
        qrstr = "?re=" + rfc_emisor + "&rr=" + rfc_receptor + "&tt=" + amount_total + "&id=" + folio_fiscal
        qr = QRCode(version=1, error_correction=ERROR_CORRECT_L)
        qr.add_data(qrstr)
        qr.make()  # Generate the QRCode itself
        im = qr.make_image()
        fname = tempfile.NamedTemporaryFile(suffix='.png', delete=False)
        im.save(fname.name)
        return base64.encodestring(open(os.path.join(fname.name), 'rb').read())

    def get_rfc(self):
        rfc = ""
        payment_obj = self.env['account.payment']
        payment_ids = self.env.context.get('active_ids')
        payment_rows =  payment_obj.browse(payment_ids[0])
        if payment_rows.company_id.vat:
            rfc = payment_rows.company_id.vat[2:]
        return rfc

    def get_rfc_partner(self):
        rfc = ""
        payment_obj = self.env['account.payment']
        payment_ids = self.env.context.get('active_ids')
        payment_rows =  payment_obj.browse(payment_ids[0])
        if payment_rows.partner_id.vat:
            rfc = payment_rows.partner_id.vat[2:]
        return rfc


    # def get_voucher_line(self):
    #     invoice_obj = self.env['account.invoice']
    #     payment_obj = self.env['account.payment']
    #     payment_ids = self.env.context.get('active_ids')
    #     payment_rows =  payment_obj.browse(payment_ids[0])
    #     #~ voucher_line_obj = self.env['account.voucher.line']
    #     #~ voucher_line_rows = voucher_line_obj.search([('voucher_id','=',voucher_rows.id)])
    #     line = []
    #     lines = []
    #     # Para cada linea de factura
    #     for inv_row in payment_rows.invoice_ids:
    #         uuid=inv_row.cfdi_folio_fiscal
    #         line.append(inv_row.number)
    #         line.append(uuid)
    #         line.append(inv_row.date_invoice)
    #         line.append(inv_row.date_due)
    #         line.append(inv_row.amount_total)
    #         line.append(inv_row.residual)
    #         # line.append(line_voucher.amount_unreconciled - line_voucher.amount)
    #         line.append(inv_row.residual)
    #         # line.append(inv_row.amount)
    #         line.append(inv_row.amount_total)
    #         lines.append(line)
    #         line = []
    #     return lines

    def get_payment_lines(self):
        invoice_obj = self.env['account.invoice']
        payment_obj = self.env['account.payment']
        payment_ids = self.env.context.get('active_ids')
        payment_rows =  payment_obj.browse(payment_ids[0])
        lines=[]
        for invoice in payment_rows.invoice_ids:
            line = []
            amount = 0.0
            amount_payment = 0.0
            count_payments = 1
            move_payment_row = False
            # Buscamos el monto que se pago pro factura
            # Recorremos cada pago de la factura
            for payment_move_row in invoice.payment_move_line_ids:
                # Buscamos el movimiento que coincida con el pago a timbrar
                if payment_move_row.payment_id != payment_rows:
                    if amount > 0.0:
                        # Obtenemos los montos aplicados a la factura aparte del que se quiere timbrar
                        # para ontener el saldo anterior al pago y cuantos pagos se habia aplicado antes que este
                        for p in payment_move_row.matched_debit_ids:
                            if p.debit_move_id in invoice.move_id.line_ids:
                                amount_payment += p.amount
                                count_payments += 1
                else:
                    if amount == 0.0:
                        # Recorremos los move reconcile para saber el monto ocupado en esta factura
                        for p in payment_move_row.matched_debit_ids:
                            # Si el pago esta en aplicado a la factura
                            if p.debit_move_id in invoice.move_id.line_ids:
                                # Obtenemos el monto aplicado del pago a la factura
                                amount = p.amount
                                break

            # Obtenemos el monto anterior alpago restando los pagos aplicados antes que este pago y el monto totald e la factura
            amount_unreconciled = round(invoice.amount_total,2) - round(amount_payment,2)
            outstanding_balance = round(amount_unreconciled,2) - round(amount,2)
            uuid=invoice.cfdi_folio_fiscal
            line.append(invoice.number)
            line.append(uuid)
            line.append(invoice.date_invoice)
            line.append(invoice.date_due)
            line.append(invoice.amount_total)
            line.append(amount_unreconciled)
            line.append(outstanding_balance)
            line.append(amount)
            lines.append(line)
        return lines

    def get_cfdi_sello(self):
        # Obtenemos la fila del attachment
        dic = self.get_dic()
        cfdi_sello=dic.get('cfdi:Complemento', {}).get('tfd:TimbreFiscalDigital', {}).get('@SelloSAT', '')
        return cfdi_sello or " "

    def get_cfdi_cer(self):
        dic = self.get_dic()
        certificate_sat=dic.get('cfdi:Complemento', {}).get('tfd:TimbreFiscalDigital', {}).get('@NoCertificadoSAT', '')
        return certificate_sat or " "

    def get_cer(self):
        dic = self.get_dic()
        certificate=dic.get('@NoCertificado', '')
        return certificate or " "

    def get_cfdi_folio(self):
        # Obtenemos la fila del attachment
        dic = self.get_dic()
        cfdi_sello=dic.get('cfdi:Complemento', {}).get('tfd:TimbreFiscalDigital', {}).get('@SelloCFD', '')
        return cfdi_sello or " "

    def get_uuid(self):
        dic = self.get_dic()
        uuid=dic.get('cfdi:Complemento', {}).get('tfd:TimbreFiscalDigital', {}).get('@UUID', '')
        return uuid or " "

    def get_date_sign(self):
        dic = self.get_dic()
        date_sign=dic.get('cfdi:Complemento', {}).get('tfd:TimbreFiscalDigital', {}).get('@FechaTimbrado', '')
        return date_sign or " "

    def get_cfdi_cadena(self):
        payment_obj = self.env['account.payment']
        payment_ids = self.env.context.get('active_ids')
        payment_rows =  payment_obj.browse(payment_ids[0])
        return payment_rows.cadena_original

    def get_dic(self):
        payment_obj = self.env['account.payment']
        payment_ids = self.env.context.get('active_ids')
        payment_rows =  payment_obj.browse(payment_ids[0])
        attach_obj = self.env['ir.attachment']
        attach_row = attach_obj.search([('res_id','=',payment_rows.id),('res_model','=','account.payment')])
        dic = {}
        for atta in attach_row:
            ext = atta.datas_fname.split('.')[-1].lower()
            if 'xml' == ext:
                dict_data = dict(xmltodict.parse(base64.decodestring( atta.datas)).get('cfdi:Comprobante', {}))
                dic=dict_data
                break
        return dic

    def get_recs(self):
        payment_obj = self.env['account.payment']
        payment_ids = self.env.context.get('active_ids')
        payment_rows =  payment_obj.browse(payment_ids[0])
        return payment_rows

    def get_name_company(self):
        rfc = ""
        payment_obj = self.env['account.payment']
        payment_ids = self.env.context.get('active_ids')
        payment_rows =  payment_obj.browse(payment_ids[0])
        if payment_rows.company_id._fields.get('fiscal_name',False):
            if payment_rows.company_id.fiscal_name:
                name_company= payment_rows.company_id.fiscal_name
            else:
                name_company=payment_rows.company_id.partner_id.name
        else:
            name_company=payment_rows.company_id.partner_id.name
        return name_company

    # The main function that pass on xml file
    @api.multi
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('l10n_mx_account_payment_cfdi.voucher_pdf')
        self.dic={}
        if self.env.context.get('active_ids'):
            voucher_ids = self.env.context.get('active_ids')
            docargs = {
                'create_qrcode':self.create_qrcode,
                'get_payment_lines':self.get_payment_lines,
                'get_cfdi_sello':self.get_cfdi_sello,
                'get_cer':self.get_cer,
                'get_cfdi_cer':self.get_cfdi_cer,
                'get_uuid':self.get_uuid,
                'get_date_sign':self.get_date_sign,
                'get_cfdi_folio':self.get_cfdi_folio,
                'get_cfdi_cadena':self.get_cfdi_cadena,
                'get_rfc':self.get_rfc,
                'get_rfc_partner':self.get_rfc_partner,
                'get_name_company':self.get_name_company,
                'get_recs':self.get_recs,
                'doc_ids': voucher_ids,
                'doc_model': report.model,
                'docs': self,
            }
        else:
            voucher_ids = docids
            docargs = {
                'create_qrcode':self.with_context(active_ids=docids).create_qrcode,
                'get_payment_lines':self.with_context(active_ids=docids).get_payment_lines,
                'get_cfdi_sello':self.with_context(active_ids=docids).get_cfdi_sello,
                'get_cer':self.with_context(active_ids=docids).get_cer,
                'get_cfdi_cer':self.with_context(active_ids=docids).get_cfdi_cer,
                'get_uuid':self.with_context(active_ids=docids).get_uuid,
                'get_date_sign':self.with_context(active_ids=docids).get_date_sign,
                'get_cfdi_folio':self.with_context(active_ids=docids).get_cfdi_folio,
                'get_cfdi_cadena':self.with_context(active_ids=docids).get_cfdi_cadena,
                'get_rfc':self.with_context(active_ids=docids).get_rfc,
                'get_rfc_partner':self.with_context(active_ids=docids).get_rfc_partner,
                'get_name_company':self.with_context(active_ids=docids).get_name_company,
                'get_recs':self.with_context(active_ids=docids).get_recs,
                'doc_ids': voucher_ids,
                'doc_model': report.model,
                'docs': self,
            }
        return report_obj.render('l10n_mx_account_payment_cfdi.voucher_pdf', docargs)


