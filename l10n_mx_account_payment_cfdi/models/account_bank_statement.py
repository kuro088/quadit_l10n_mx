from odoo import models, fields


class AccountBankStatementLine(models.Model):
    _inherit = "account.bank.statement.line"

    forma_pago_id = fields.Many2one('res.forma.pago', 'Forma de pago')

    def process_reconciliation(self, counterpart_aml_dicts=None,
                               payment_aml_rec=None, new_aml_dicts=None):
        invoice_ids = []
        for aml_dict in counterpart_aml_dicts or []:
            if aml_dict['move_line'].invoice_id:
                invoice_ids.append(aml_dict['move_line'].invoice_id.id)
        res = super().process_reconciliation(
            counterpart_aml_dicts=counterpart_aml_dicts,
            payment_aml_rec=payment_aml_rec, new_aml_dicts=new_aml_dicts)
        payments = res.mapped('line_ids.payment_id')
        if not self.journal_id.fiscal:
            return res
        payments.write({
            'forma_pago_id': self.forma_pago_id.id,
            'invoice_ids': [(6, 0, invoice_ids)]
        })
        self.create_xml()
        return res
