# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.


import base64
import hashlib
import os

import tempfile
import time
import datetime
from suds.client import Client
from M2Crypto import X509, RSA
import xmltodict
from lxml import etree, objectify
from jinja2 import Environment, FileSystemLoader, StrictUndefined
from odoo import api, _, fields, models, tools
from odoo.exceptions import ValidationError


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    forma_pago_id = fields.Many2one('res.forma.pago', 'Forma de pago')


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    cadena_original = fields.Text('Original String')
    sign = fields.Boolean('Timbrado', copy=False)
    cfdi_folio_fiscal = fields.Char('Folio Fiscal(UUID)')
    forma_pago_id = fields.Many2one('res.forma.pago', 'Forma de pago')

    @api.multi
    def post(self):
        res = super(AccountPayment, self).post()
        self.create_xml()
        return res

    def _get_datetime_with_user_tz(self, datetime_invoice=False):
        if datetime_invoice:
            dt_tz = fields.Datetime.context_timestamp(
                self.with_context(tz=self.env.user.tz),
                fields.Datetime.from_string(fields.Datetime.now()))

        return dt_tz

    def create_xml(self):
        all_paths = tools.config["addons_path"].split(",")
        for my_path in all_paths:
            if os.path.isdir(os.path.join(my_path, 'l10n_mx_account_payment_cfdi', 'SAT')):
                path_cadena=os.path.join(
                    my_path, 'l10n_mx_account_payment_cfdi', 'SAT',
                    'cadenaoriginal_3_3.xslt')
        type_voucher = 'P'
        self.get_payment_amount()
        for voucher in self:
            if not voucher.journal_id.fiscal:
                return True
            pagos = []
            lines = []
            moneda = self.currency_id.name
            tipo_cambio = '1'
            if not voucher.forma_pago_id:
                raise ValidationError(_('Falta la forma de pago fiscal!'))
            if str(moneda) != 'MXN':
                tipo_cambio = str("%.2f" % (1 / voucher.currency_id.rate))
            for invoice in voucher.invoice_ids:
                amount = 0.0
                amount_payment = 0.0
                count_payments = 1
                # Si alguna factura que pago el pago no esta timbrada se muestra error
                if not invoice.sign:
                    raise ValidationError(_('Error !\n\nLa factura que estas pagando no esta timbrada!'))
                # Buscamos el XML de la factura timbrada
                attachment = invoice.retrive_attachment()
                if not attachment:
                    raise ValidationError(_(
                        'Error !\n\nLa factura no tiene XML adjunto!'))
                # Buscamos el monto que se pago pro factura
                # Recorremos cada pago de la factura
                payments = [p for p in invoice._get_payments_vals() if p.get(
                    'account_payment_id', False) == voucher.id]
                amount = payments[0].get('amount', 0.0) if payments else 0.0
                for payment_move_row in invoice.payment_move_line_ids:
                    # Buscamos el movimiento que coincida con el pago a timbrar
                    if payment_move_row.payment_id != voucher:
                        if amount > 0.0:
                            # Obtenemos los montos aplicados a la factura aparte del que se quiere timbrar
                            # para obtener el saldo anterior al pago y cuantos pagos se habia aplicado antes que este
                            for p in payment_move_row.matched_debit_ids:
                                if p.debit_move_id in invoice.move_id.line_ids:
                                    amount_payment += p.amount
                                    count_payments += 1
                # Obtenemos el monto anterior al pago restando los pagos aplicados antes que este pago y el monto total de la factura
                amount_unreconciled = invoice.currency_id.compute(
                    invoice.amount_total, voucher.currency_id) - amount_payment
                # Obtenemos el UUID del XML
                dict_data = dict(xmltodict.parse(base64.decodestring(
                    attachment.datas)).get('cfdi:Comprobante', {}))
                IdDocumento = dict_data.get('cfdi:Complemento', {}).get(
                    'tfd:TimbreFiscalDigital', {}).get('@UUID', '')
                # Validamos si ya se esta liquidando la factura con este pago
                if amount and amount >= amount_unreconciled:
                    if amount > amount_unreconciled:
                        amount = amount_unreconciled
                    # Si el metodo de pago es PUE no lleva ImpSaldoAnt y ImpSaldoInsoluto
                    if dict_data.get('@MetodoPago', '') == 'PUE':
                        pago = {
                            'IdDocumento': IdDocumento,
                            'MonedaDR': dict_data.get('@Moneda', '') or 'MXN',
                            'MetodoDePagoDR': dict_data.get('@MetodoPago', ''),
                            'ImpPagado': "%.2f" % (amount),
                            # 'TipoCambioDR': 1.0,
                        }
                        if str(moneda) != dict_data.get('@Moneda', ''):
                            imp = amount / float(dict_data.get('@TipoCambio', 1))
                            new_imp = str(imp).split('.')[0]+'.'+str(imp).split('.')[1][:2]
                            pago.update({
                                # 'TipoCambioDR': dict_data.get('@TipoCambio', ''),
                                'ImpPagado': "%.2f" % float(new_imp)})
                    else:
                        pago = {
                            'IdDocumento': IdDocumento,
                            'MonedaDR': dict_data.get('@Moneda', '') or 'MXN',
                            'MetodoDePagoDR': dict_data.get('@MetodoPago', ''),
                            'ImpSaldoAnt': "%.2f" % (amount_unreconciled),
                            'ImpPagado': "%.2f" % (amount),
                            'ImpSaldoInsoluto': "%.2f" % (0.0),
                            'NumParcialidad': str(count_payments),
                            # 'TipoCambioDR': 1.0,
                        }
                        if str(moneda) != dict_data.get('@Moneda', ''):
                            before = amount_unreconciled / float(dict_data.get('@TipoCambio', 1))
                            imp = amount / float(dict_data.get('@TipoCambio', 1))
                            new_imp = str(imp).split('.')[0]+'.'+str(imp).split('.')[1][:2]
                            pago.update({
                                # 'TipoCambioDR': invoice.currency_id.with_context(date=voucher.payment_date).compute(1, voucher.currency_id, round=False),  # noqa
                                'ImpPagado': "%.2f" % float(new_imp),
                                'ImpSaldoAnt': "%.2f" % (before),
                                'ImpSaldoInsoluto': "%.2f" % abs(before- float(new_imp))})
                    pagos.append(pago)
                elif amount and amount < amount_unreconciled:
                    # Si se esta pagando menos tenemos que decirle el numero de pago que es y los saldos de la factura
                    pago = {
                        'IdDocumento': IdDocumento,
                        'MonedaDR': dict_data.get('@Moneda', '') or 'MXN',
                        'MetodoDePagoDR': dict_data.get('@MetodoPago', ''),
                        'ImpSaldoAnt': "%.2f" % (amount_unreconciled),
                        'ImpPagado': "%.2f" % (amount),
                        'ImpSaldoInsoluto': "%.2f" % (amount_unreconciled - amount),
                        'NumParcialidad': str(count_payments),
                        # 'TipoCambioDR': dict_data.get('@TipoCambio', ''),
                    }
                    if str(moneda) != dict_data.get('@Moneda', ''):
                        before = amount_unreconciled / float(dict_data.get('@TipoCambio', 1))
                        imp = amount / float(dict_data.get('@TipoCambio', 1))
                        new_imp = str(imp).split('.')[0]+'.'+str(imp).split('.')[1][:2]
                        pago.update({
                            # 'TipoCambioDR': dict_data.get('@TipoCambio', ''),
                            'ImpPagado': "%.2f" % float(new_imp),
                            'ImpSaldoAnt': "%.2f" % (before),
                            'ImpSaldoInsoluto': "%.2f" % abs(
                                before - float(new_imp))})
                    pagos.append(pago)

            line_dic = {
                'cantidad': 1,
                'descripcion': 'Pago',  # Optional
                'valorUnitario':0,
                'importe': 0,
                'ClaveUnidad':'ACT',
                'ClaveProdServ':'84111506',
            }
            lines.append(line_dic)
            if not voucher.partner_id.bank_ids and voucher.forma_pago_id.bancarizado:
                raise ValidationError('Faltan datos bancarios del cliente!')
            rfc_bank = ''
            acc_number = ''

            certificate_id = self.company_id.certificate_id
            if not certificate_id:
                raise ValidationError(_(
                    'No tienes definido certificado para esta compañia !'))
            date = datetime.datetime.combine(
                fields.Datetime.from_string(self.payment_date),
                datetime.datetime.strptime(
                    '12:00:00', '%H:%M:%S').time()).strftime(
                        '%Y-%m-%dT%H:%M:%S')
            mxn = self.env.ref('base.MXN')
            fecha_cfdi = voucher._get_datetime_with_user_tz(
                voucher.payment_date).strftime('%Y-%m-%dT%H:%M:%S')
            data = {
                'Atributos': {
                    'serie': str(voucher.journal_id.sequence_id.serie or ''),
                    'folio': voucher.name,
                    'fecha': date,
                    'fecha_cfdi': fecha_cfdi,
                    'formaDePago': voucher.forma_pago_id.name,
                    'MonedaP': voucher.currency_id.name,
                    'TipoCambioP': voucher.currency_id.with_context(
                        date=voucher.payment_date).compute(1, mxn),
                    'sello': '',
                    'noCertificado': certificate_id.serial_number,
                    'certificado': certificate_id.sudo().get_data()[0].decode(),
                    'condicionesDePago': '',
                    'subTotal': 0,
                    'Moneda': 'XXX',
                    'total': 0,
                    'tipoDeComprobante': type_voucher,
                    'LugarExpedicion': voucher.company_id.zip,
                    'Monto': "%.2f" % (voucher.amount),
                    'acc_number':acc_number,
                    'rfc_bank':rfc_bank,
                },
                'Emisor': {
                    'rfc': voucher.company_id.partner_id.vat,
                    'nombre': voucher.company_id.name,
                    'regimenFiscal': voucher.company_id.partner_id.property_account_position_id.code,
                },
                'Receptor': {
                    'rfc': voucher.partner_id.vat,
                    'nombre': voucher.partner_id.name,
                    'UsoCFDI':  'P01',
                },
                'Conceptos': lines,
                'Pagos': pagos,
                'Impuestos': {},
            }
            if voucher.forma_pago_id.bancarizado:
                data.update({'bank': True})
            else:
                data.update({'bank': False})

            if str(moneda) != 'MXN':
                data['Atributos'].update({'TipoCambioP': tipo_cambio})
            certificate_obj = self.pool.get('res.company.facturae.certificate')
            company = voucher.company_id
            certificate_obj = self.env['res.company.facturae.certificate']
            certificate_id = certificate_obj.search([
                ('company_id', '=', company.id)], limit=1)
            if not certificate_id:
                raise ValidationError(_(
                    'No tienes definido certificado para esta compañia !'))

            pac_usr = ''
            pac_pwd = ''
            pac_url = ''
            pac_params_ids = self.env['params.pac'].search([
                ('method_type', 'ilike', 'firmar'), (
                    'company_id', '=', voucher.company_id.id), (
                        'active', '=', True)], limit=1)
            if not pac_params_ids:
                raise ValidationError(_(
                    'No tienes parametros del PAC configurados'))
            pac_usr = pac_params_ids.user
            pac_pwd = pac_params_ids.password
            pac_url = pac_params_ids.url_webservice
            client = Client(pac_url, cache=None)

            env = Environment(
                loader=FileSystemLoader(
                    os.path.join(
                        os.path.dirname(os.path.abspath(__file__)), '../template',),),
                undefined=StrictUndefined, autoescape=True,
            )
            template = env.get_template('pago.jinja')
            xml = template.render(data=data).encode('utf-8')
            cadena = self.generate_cadena_original(
                xml, {'path_cadena': path_cadena})
            key = certificate_id.key_file
            pass_key = certificate_id.password

            sello = self.get_sello(key, pass_key, cadena)
            tree = objectify.fromstring(xml)
            tree.attrib['Sello'] = sello
            xml = etree.tostring(
                tree, pretty_print=True,
                xml_declaration=True, encoding='UTF-8')

            xml = base64.encodestring(xml)
            resultado = client.service.stamp(xml.decode(), pac_usr, pac_pwd)

            if resultado.Incidencias and resultado.Incidencias.Incidencia and resultado.Incidencias.Incidencia[0]:
                msg = ' %s - %s ' % (
                    resultado.Incidencias.Incidencia[0].CodigoError,
                    resultado.Incidencias.Incidencia[0].
                    MensajeIncidencia)
                raise ValidationError(_(msg))

            if not resultado.Incidencias or None:
                # folio_fiscal = resultado.UUID or False
                # original_string = self._create_original_str(resultado)
                cfdi_xml = resultado.xml.encode(
                    'ascii', 'xmlcharrefreplace') or ''
            fname = voucher.company_id.partner_id.vat + '_' + str(voucher.invoice_ids[0].number) +  '-pay.xml' or ''
            self.env['ir.attachment'].create({
                'name': fname,
                'datas': base64.encodestring(cfdi_xml),
                'datas_fname': fname,
                'res_model': 'account.invoice',
                'res_id': voucher.invoice_ids[0].id,
            })
            self.env['ir.attachment'].create({
                'name': fname,
                'datas': base64.encodestring(cfdi_xml),
                'datas_fname': fname,
                'res_model': 'account.payment',
                'res_id': voucher.id,
            })

        datas = {
            'ids': [self.id]
        }
        try:
            self.payment_pdf_attach()
        except:
            pass
        self.env.ref('l10n_mx_account_payment_cfdi.custom_voucher_cfdi_pdf').report_action(self, data=datas)

        return True

    def cancel_voucher(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        res = False
        folio_cancel = ''
        uuids = []
        pac_params_obj = self.pool.get('params.pac')

        ids = isinstance(ids, (int, long)) and [ids] or ids
        res = super().cancel_voucher()
        for voucher in self.pool.get('account.voucher').browse(cr, uid, ids, context=context):
            if voucher.sign:
                pac_params_ids = pac_params_obj.search(cr, uid, [
                     ('method_type', '=', 'cancelar'), (
                         'company_id', '=', voucher.company_id.id), (
                                 'active', '=', True)], limit=1, context=context)
                if pac_params_ids:
                    pac_params = pac_params_obj.browse(cr, uid, pac_params_ids, context)[0]
                    pac_usr = pac_params.user
                    pac_pwd = pac_params.password
                    wsdl_url = pac_params.url_webservice
                else:
                    raise ValidationError(_(
                        'No tienes parametros del PAC configurados para cancelar'))
                certificate_obj = self.pool.get('res.company.facturae.certificate')
                company = voucher.company_id
                certificate_ids = certificate_obj.search(cr, uid, [
                    ('company_id', '=', company.id)], limit=1)
                certificate_id = certificate_ids and certificate_ids[0] or False
                certificate_id = certificate_id and self.pool.get(
                    'res.company.facturae.certificate').browse(
                    cr, uid, [certificate_id], context=context)[0] or False
                if not certificate_id:
                    raise ValidationError(_(
                        'No tienes definido certificado para esta compañia !'))
                cer_no_pem=certificate_id.certificate_file_pem
                cer_csd = cer_no_pem.encode('base64')
                cer_csd = base64.decodestring(base64.decodestring(cer_csd))
                key_no_pem=base64.decodestring(certificate_id.certificate_key_file_pem)
                key_csd = key_no_pem and base64.encodestring(key_no_pem)
                rsa = RSA.load_key_string(base64.decodestring(key_csd))
                encr_key = self.encrypt_key(rsa, str(pac_pwd))
                key_csd = base64.encodestring(encr_key)
                try:
                    client = Client(wsdl_url, cache=None)
                except:
                    raise ValidationError(_(
                        'Connection lost, verify your internet connection '
                        'or verify your PAC'))
                taxpayer_id=voucher.company_id.partner_id.vat
                folio_cancel = voucher.cfdi_folio_fiscal
                uuids.append(folio_cancel)
                uuids_list = client.factory.create("UUIDS")
                uuids_list.uuids.string = uuids
                result=client.service.cancel(uuids_list,pac_usr,pac_pwd, taxpayer_id,base64.encodestring(cer_csd),key_csd)
                if not result.Folios:
                    raise ValidationError(result)
                voucher.cfdi_fecha_cancelacion = time.strftime(
                    '%Y-%m-%d %H:%M:%S')
                if result.Acuse:
                    self.pool.get('ir.attachment').create(cr, uid, {
                    'name': 'Acuse_Cancelacion_' + voucher.number +'.xml',
                    'datas_fname':  'Acuse_Cancelacion_' + voucher.number +'.xml',
                    'datas': base64.encodestring(result.Acuse),
                    'res_model': 'account.voucher',
                    'res_id': voucher.id})

            else:
                return res
        return res

    @classmethod
    def encrypt_key(self, rsa, password):
        rsa_encrypt = rsa.as_pem("des_ede3_cbc", lambda x: password)
        return rsa_encrypt

    def _create_original_str(self,result):
        cfdi_folio_fiscal = result.UUID or ''
        cfdi_fecha_timbrado = result.Fecha or ''
        sello = result.SatSeal or ''
        cfdi_no_certificado = result.NoCertificadoSAT or ''
        original_string = '||1.0|' + cfdi_folio_fiscal + '|' + str(
            cfdi_fecha_timbrado) + '|' + sello + '|' \
            + cfdi_no_certificado + '||'
        return original_string

    @classmethod
    def validate_data(self,data, cer, key, pwd):
        '''
        This method will be used to validate correct input.
        @param data: python dictionary with invoide data.
        '''
        if cer and key and pwd != '':
            data['Atributos']['certificado'] = cer
            data['Atributos']['noCertificado'] = self.get_noCertificado(cer)
        else:
            raise ValueError(
                'You have not defined cer, key or pwd')

    @classmethod
    def base64_to_tempfile(self, b64_str=None, suffix=None, prefix=None):
        """ Convert strings in base64 to a temp file
        @param b64_str : Text in Base_64 format for add in the file
        @param suffix : Sufix of the file
        @param prefix : Name of file in TempFile
        """
        (fileno, file_name) = tempfile.mkstemp(suffix, prefix)
        f_read = open(file_name, 'wb')
        f_read.write(base64.decodestring(b64_str))
        f_read.close()
        os.close(fileno)
        return file_name

    @classmethod
    def get_noCertificado(self, cer):
        cert = X509.load_cert_string(
            base64.decodestring(cer), X509.FORMAT_DER)
        serial = str(u'{0:0>40x}'.format(cert.get_serial_number()))
        return serial.replace('33', 'B').replace('3', '').replace(
            'B', '3').replace(' ', '').replace('\r', '').replace(
            '\n', '').replace('\r\n', '')

    @classmethod
    def generate_cadena_original(self, xml, context=None):
        xlst_file = tools.file_open(context.get('path_cadena', '')).name
        dom = etree.fromstring(xml)
        xslt = etree.parse(xlst_file)
        transform = etree.XSLT(xslt)
        return str(transform(dom))

    @classmethod
    def get_sello(self, key, pwd, cadena):
        key_file = self.base64_to_tempfile(key, '', '')
        (no, pem) = tempfile.mkstemp()
        os.close(no)
        cmd = ('openssl pkcs8 -inform DER -outform PEM'
               ' -in "%s" -passin pass:%s -out %s' % (key_file, pwd, pem))
        os.system(cmd)
        keys = RSA.load_key(pem)
        digest = hashlib.new('sha256', cadena.encode()).digest()
        return base64.b64encode(keys.sign(digest, "sha256"))

    @classmethod
    def stamp_finkok(self, xml, context=None):
        pwd=context.get('pac_pwd',"")
        usr=context.get('pac_usr',"")
        url=context.get('pac_url',"")
        client = Client(url, cache=None)
        xml=base64.encodestring(xml)
        res=client.service.stamp(xml, usr, pwd)
        return res



    @api.multi
    def get_payment_amount(self):
        for payment in self:
            domain = [('payment_id','=',payment.id),('partner_id', '=', self.env['res.partner']._find_accounting_partner(payment.partner_id).id), ('amount_residual', '!=', 0.0),('credit', '>', 0), ('debit', '=', 0)]
            line_row = self.env['account.move.line'].search(domain, limit=1)
            if abs(line_row.amount_residual) > 0 :
                raise ValidationError(_('Error !\n\nPrimero debe consumir todo el pago realizado aun falta $ %s de este pago.'%(abs(line_row.amount_residual),)))
        return True

    @api.multi
    def payment_pdf_attach(self):
        """ Print the invoice and mark it as sent, so that we can see more
            easily the next step of the workflow
        """
        self.ensure_one()
        Template_obj = self.env['account.payment']
        dummy, report_id = self.env['ir.model.data'].get_object_reference('l10n_mx_account_payment_cfdi', 'custom_voucher_pdf')
        report = self.env['ir.actions.report.xml'].browse(report_id)
        report_service = report.report_name

        if report.report_type in ['qweb-html', 'qweb-pdf']:
            result, format = Template_obj.env['report'].get_pdf([self.id], report_service), 'pdf'
        # else:
        #     result, format = odoo_report.render_report(self._cr, self._uid, [self.id], report_service, {'model': self.model}, Template_obj._context)

        # TODO in trunk, change return format to binary to match message_post expected format
        result = base64.b64encode(result)
        return True


class account_register_payments(models.TransientModel):
    _inherit = "account.register.payments"

    forma_pago_id = fields.Many2one('res.forma.pago', 'Forma de pago')

    def _prepare_payment_vals(self, invoices):
        res = super()._prepare_payment_vals(invoices)
        res.update({'forma_pago_id': self.forma_pago_id.id})
        return res
