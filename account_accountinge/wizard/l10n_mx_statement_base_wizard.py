# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _

from odoo import tools, registry
#~ from odoo.report import report_sxw
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError

import os
import io
#~ import StringIO
#~ from io import BytesIO
from io import StringIO
import jinja2
from jinja2 import Template, Environment, FileSystemLoader, StrictUndefined
import base64
import hashlib
import os
import tempfile
import time
from M2Crypto import RSA
import cgi
import re
import xml
import calendar
from lxml import etree, objectify
 
today=datetime.now()
calendar.monthrange(today.year, today.month)[1]
class L10nMxStatementBaseWizard(models.TransientModel):

    _name = 'l10n_mx.statement.base.wizard'

    

    @api.multi
    def _get_period(self, fy_id=None):
        now = time.strftime('%Y-%m-%d')
        period_obj = self.env['account.period']
        last_month_date = datetime.strptime(
            now, '%Y-%m-%d') - relativedelta(months=1)
        periods = period_obj.search([
            ('date_stop', '>=', last_month_date),
            ('date_start', '<=', last_month_date),
            ('fiscalyear_id', '=', fy_id)])
        return periods and periods[0] or False

    @api.multi
    def on_change_company_id(self, company_id=False):
        if company_id:
            now = time.strftime('%Y-%m-%d')
            domain = [('company_id', '=', company_id),
                      ('date_stop', '>=', now),
                      ('date_start', '<=', now)
                      ]
            fiscalyear_current = self.env[
                'account.fiscalyear'].search(domain, limit=1)
            fiscalyear_current = fiscalyear_current and fiscalyear_current[
                0] or False
            return {'value': {
                'fy_id': fiscalyear_current,
                'period_id': self._get_period(fy_id=fiscalyear_current.id)
            }
            }
        else:
            return {'value': {
                'fy_id': False,
                'period_id': False
            }
            }

    @api.multi
    def onchange_fy_id(self, fy_id=False):
        result = {}
        if fy_id:
            fiscalyear = self.env['account.fiscalyear'].browse(fy_id)
            company_id = fiscalyear.company_id.id
            result = {'value': {
                'period_id': self._get_period(fy_id=fiscalyear.id),
                'company_id': company_id,
                }
            }
        return result

    @api.multi
    def _get_account(self):
        user = self.env['res.users'].browse(self._uid)
        accounts = self.env['account.account'].search([
            ('company_id', '=', user.company_id.id)], limit=1)
        return accounts and accounts[0] or False


            
            
    def string_to_xml_format(self,text):
        """
        Transforms text to xml format.
        """
        if text:
            return cgi.escape(text, True).encode(
                'ascii', 'xmlcharrefreplace').decode().replace('\n\n', ' ')
        else:
            return ''

    def get_rate(self, to_currency_id,
                 from_currency_id, from_amount):
        return False

        return self.env['res.currency'].compute(from_amount,to_currency_id) or False

    def validate_scheme_accountinge_xml(self, datas_xmls=None,
                                        xsd_type=None, scheme_type='xsd',
                                        context=None):
        if datas_xmls is None:
            datas_xmls = []
        return True
        certificate_lib = self.env['facturae.certificate.library']
        for data_xml in datas_xmls:
            all_paths = tools.config["addons_path"].split(",")
            for my_path in all_paths:
                if os.path.isdir(os.path.join(my_path, 'account_accountinge',
                                              'complements')):
                    fname_scheme = my_path and os.path.join(
                        my_path, 'account_accountinge',
                        'complements',
                        xsd_type + '.' + scheme_type) or ''
                    result = certificate_lib.check_xml_scheme(data_xml, fname_scheme)
                    if result:
                        raise ValidationError(
                            'Validating the xml %s:\n%s' % (xsd_type, result))
            return True

    @classmethod
    def generate_cadena_original(self, xml, context=None):
        xlst_file = tools.file_open(context.get('path_cadena', '')).name
        dom = etree.fromstring(xml)
        xslt = etree.parse(xlst_file)
        transform = etree.XSLT(xslt)
        return str(transform(dom))

    @classmethod
    def get_sello(self, key, pwd, cadena):
        key_file = self.base64_to_tempfile(key, '', '')
        (no, pem) = tempfile.mkstemp()
        os.close(no)
        cmd = ('openssl pkcs8 -inform DER -outform PEM'
               ' -in "%s" -passin pass:%s -out %s' % (key_file, pwd, pem))
        os.system(cmd)
        keys = RSA.load_key(pem)
        digest = hashlib.new('sha256', cadena.encode()).digest()
        return base64.b64encode(keys.sign(digest, "sha256"))

    @classmethod
    def base64_to_tempfile(self, b64_str=None, suffix=None, prefix=None):
        """ Convert strings in base64 to a temp file
        @param b64_str : Text in Base_64 format for add in the file
        @param suffix : Sufix of the file
        @param prefix : Name of file in TempFile
        """
        (fileno, file_name) = tempfile.mkstemp(suffix, prefix)
        f_read = open(file_name, 'wb')
        f_read.write(base64.decodestring(b64_str))
        f_read.close()
        os.close(fileno)
        return file_name

    def create_xml_statement(self, dictargs, type_xml, context=None):
        xml_type_dict = {
            'CT': ['templates', 'template_accounts',
                   'CatalogoCuentas_1_3'],
            'BN': ['templates', 'template_balanza',
                   'BalanzaComprobacion_1_3'],
            'BC': ['templates', 'template_balanza',
                   'BalanzaComprobacion_1_3'],
            'PL': ['templates',
                   'template_journal_entries', 'PolizasPeriodo_1_3']
        }

        filename = ""
        type_xml = type_xml[:2]
        #~ period_id = self.pool.get("account.period").browse(
            #~ cr, uid, context.get("period_id"))
        #~ date_p = datetime.strptime(self.date, "%Y-%m-%d")
        date_p = self.date
        all_paths = tools.config["addons_path"].split(",")
        for my_path in all_paths:
            if os.path.isdir(os.path.join(my_path,
                                          'account_accountinge',
                                          xml_type_dict.get(type_xml)[0])):
                fname_jinja_tmpl = my_path and os.path.join(
                    my_path, 'account_accountinge',
                    xml_type_dict.get(type_xml)[0],
                    xml_type_dict.get(type_xml)[1] + '.jinja') or ''
        #~ company_obj = self.pool.get("res.company").browse(
            #~ cr, uid, context.get("company_id"))
        rfc = str(self.company_id.partner_id.vat) or False
        registry_t = registry(self._cr.dbname)
        rpt_obj = registry_t['l10n_mx.statement.base.wizard']
        #~ rml_obj = report_sxw.rml_parse(self._cr, self._uid,rpt_obj._name,context)
        dictargs.update({
                        'date_p': date_p,
                        'rfc': rfc,
                        '_': _,
                        'str': str,
                        'int': int,
                        'float': float,
                        'st_base_wz': self,
                        })
        #~ xml_tmp = io.StringIO.StringIO()
        xml_tmp = StringIO()
        #~ xml_tmp = BytesIO()
        with open(fname_jinja_tmpl, 'r') as f_jinja_tmpl:
            jinja_tmpl_str = f_jinja_tmpl.read().encode('utf-8')
            tmpl = jinja2.Template(jinja_tmpl_str.decode())
            xml_tmp.write(tmpl.render(dictargs))
        xml_data = xml_tmp.getvalue().encode('ascii', 'xmlcharrefreplace')
        try:
            self.validate_scheme_accountinge_xml([xml_data], xml_type_dict.get(type_xml)[2])
        except:
            raise ValidationError(_('Error ! %s'%e))

        filename += rfc + date_p.strftime("%Y%m") + type_xml + ".xml"
        
        
        certificate_obj = self.env['res.company.facturae.certificate']
        certificate_id = certificate_obj.search([
            ('company_id', '=', self.company_id.id)], limit=1)
        if not certificate_id:
            raise ValidationError(_(
                'No tienes definido certificado para esta compañia !'))
        #~ cer = certificate_ids[-1].certificate_file
        key = certificate_id.key_file
        pass_key = certificate_id.password
        certificate = certificate_id.sudo().get_data()[0].decode()
        no_certificate = certificate_id.serial_number

        for my_path in all_paths:
            if os.path.isdir(os.path.join(my_path, 'account_accountinge',
                                          'complements')):
                fname_xslt = my_path and os.path.join(
                    my_path, 'account_accountinge',
                    'complements',
                    xml_type_dict.get(type_xml)[2] + '.' + 'xslt') or ''

        cadena = self.generate_cadena_original(xml_data,{'path_cadena':fname_xslt})
        sello = self.get_sello(key, pass_key, cadena)
        tree = objectify.fromstring(xml_data)
        tree.attrib['Sello'] = sello
        tree.attrib['noCertificado'] = no_certificate
        tree.attrib['Certificado'] = certificate

        xml_data = etree.tostring(
                tree, pretty_print=True,
                xml_declaration=True, encoding='UTF-8')
        return xml_data, filename

    def execute_task_wizard(self):
        context = {}

        context.update({
            'check_acc_xml': self.check_acc_xml,
            'check_balance_xml': self.check_balance_xml,
            'check_journal_entries_xml': self.check_journal_entries_xml, 
            'company_id': self.company_id and self.company_id.id,
            'type_balance': self.type_balance or False,
            'type_journal_entry': self.type_journal_entry or False})

        xml_generate = []
        dictargs = {}
        statement_ids = []
        account_list=[]
        parent_list=[]
        account_ids =  self.env['account.account.tag'].search([('l10n_mx_type_id', '<>', False)])

        for acc in account_ids:
            acco_acco=self.env['account.account'].search([('tag_ids', 'in', acc.id)])

            if acc.l10n_mx_type_id.code not in parent_list:
                account_list.append({'code':acc.l10n_mx_type_id.code,'code_cta':acc.l10n_mx_type_id.code,'name':acc.l10n_mx_type_id.name,'level':acc.l10n_mx_type_id.level,'accounting_nature':acc.l10n_mx_type_id.accounting_nature})
                parent_list.append(acc.l10n_mx_type_id.code)
            account_list.append({'code':acc.name.split(' ')[0],'code_cta':acc.name.split(' ')[0],'name':acc.name.replace(acc.name.split(' ')[0]+' ',""),'level':int(acc.l10n_mx_type_id.level) +1,'accounting_nature':acc.l10n_mx_type_id.accounting_nature})
        if not account_ids:
            raise ValidationError(
                _("Ninguna cuenta del catalogo tiene tipo SAT mx"))
        values = self.env["account.journal"].fields_get('l10n_mx_type').get('l10n_mx_type').get('selection')
        l10n_mx_type_values_dict = {lin[0]: lin[1] for lin in values}
        dictargs.update(

            {'accounts': account_list,
             'total_acc': len(account_ids),
             'data_accounts': self.env["l10n_mx.statement.base"].get_balance_account(list(account_ids),self.date,self.date_end),
             'l10n_mx_type': l10n_mx_type_values_dict,
             }
            )
        if self.check_acc_xml:
            xml_generate.append('CT')
        name_balance = ''
        if context.get('check_balance_xml'):
            if context.get('type_balance'):
                name_balance = 'B'+context.get('type_balance')
                xml_generate.append(name_balance)
        name_journal_entry = ''
        if self.check_journal_entries_xml:
            if context.get('type_journal_entry'):
                name_journal_entry = 'PL'+context.get('type_journal_entry')
                xml_generate.append(name_journal_entry)
            move_line_ids = self.env["account.move.line"].search([('date','>=',self.date),('date','<=',self.date_end)])
            entries=[]
            for e in list(move_line_ids):
                entries.append(e.move_id)
            dictargs.update({'entries': entries})
        for type_xml in xml_generate:
            xml_data, filename = self.create_xml_statement(dictargs, type_xml, context=context)
            attachment_id =self.env['ir.attachment'].create({'name': filename,
                          'datas': base64.encodestring(xml_data),
                          'datas_fname': filename,
                          'res_model': self._name,
                          })
            statement_ids.append(
                self.env["l10n_mx.statement.base"].create( {
                    'name': filename,
                    'type': type_xml,
                    'statement_attach_id': attachment_id.id,
                    'company_id': self.company_id.id,
                    'generation_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                    'state': 'draft',
                     }).id)

        if statement_ids:
            return {
                'name': 'New Statements Created',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'l10n_mx.statement.base',
                'domain': [('id', 'in', statement_ids), ],
                'type': 'ir.actions.act_window',
            }
        return {}

    def get_receipt(self,move_id):
        res = {'uuid': ''}
        uuid_string= self.env["account.invoice"].search([('move_id','=',move_id)]).cfdi_folio_fiscal
        if uuid_string:
            uuid = {'uuid': uuid_string}
            res.update(uuid)
        return res
        


    company_id=fields.Many2one('res.company', 'Compañia',default=lambda self: self.env.user.company_id)

    date = fields.Date(string='Fecha Inicial',
        default="%s-%s-01" % (today.year, str(today.month).zfill(2)),  copy=False)
    date_end = fields.Date(string='Fecha Fin',
        default="%s-%s-%s" % (today.year, str(today.month).zfill(2), calendar.monthrange(today.year, today.month)[1]),  copy=False)
    check_acc_xml=fields.Boolean("Catalogo de Cuentas 1.3")
    check_balance_xml=fields.Boolean("Balanza de comprobacion 1.3")
    type_balance=fields.Selection(
            [('N', 'Normal'), ('C', 'Complementaria')])
    check_journal_entries_xml=fields.Boolean("Polizas 1.3")
    type_journal_entry=fields.Selection(
            [('AF', 'Act Audit'), ('FC', 'Attested Audit'),
             ('DE', 'Refund'), ('CO', 'Compensation')])

