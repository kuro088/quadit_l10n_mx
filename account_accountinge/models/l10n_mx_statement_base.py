# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from openerp.osv import osv
from openerp import api, fields, models
from openerp.tools.translate import _
import re

_RE_CHECK = re.compile(r'(check_accountinge)(\w)+')


class L10nMxStatementBase(models.Model):
    _name = 'l10n_mx.statement.base'

    @api.model
    def _get_type_selection(self):
        types = [
            # XML Account
            ('CT', 'Catalogo Contable'),
            # XML Balance
            ('BN', 'Balanza Normal'),
            ('BC', 'Balanza Complementaria'),
            # XML Journal Entries
            ('PLAF', 'Acto de Fiscalización  (Journal Entries)'),
            ('PLFC', 'Fiscalización  Compulsa(Journal Entries)'),
            ('PLDE', 'Devolucion (Journal Entries)'),
            ('PLCO', 'Compensacion (Journal Entries)')
        ]
        return types

    @api.model
    def _get_states(self):
        states = [
            ('draft', 'Borrador'),
            ('done', 'Listo'),
            ('cancel', 'Cancelado'),
        ]
        return states

    name = fields.Char('Nombre')
    statement_attach_id = fields.Many2one(
        'ir.attachment', 'Adjunto Oficial')
    download_statement_attach = fields.Binary(
        related='statement_attach_id.datas', string='Datas')
    statement_attach_name = fields.Char(
        string='Fname', related='statement_attach_id.datas_fname')
    csv_attach_id = fields.Many2one('ir.attachment', 'Adjunto CSV')
    download_csv_attach = fields.Binary(
        related='csv_attach_id.datas', string='CSV')
    csv_attach_name = fields.Char(
        related='csv_attach_id.datas_fname', string='Name CSV')

    description = fields.Text('Descripcion')
    type = fields.Selection(
        _get_type_selection, "Tipo", type='char', size=64)
    generation_date = fields.Datetime('Fecha de Generacion', readonly=True)
    company_id = fields.Many2one(
        'res.company', 'Compañia',
        readonly=True)

    state = fields.Selection(
        _get_states, 'Estado', type='char')

    def _accountinge_statement_valid_exists_state(self):
        for statement in self:
            statement_exist_ids = self.search(
                [('id', '<>', statement.id),
                ('name', '=', statement.name),
                 ('company_id', '=', statement.company_id.id),
                 ('type', '=', statement.type),
                 ('state', '<>', 'cancel')])
                 
            if statement_exist_ids:
                return False
        return True

    _constraints = [
        (_accountinge_statement_valid_exists_state,
         'Ya existe una declaracion para esta fecha y esta en estado'
         ' borrador o listo',
         ['company_id','type', 'state'])]

    def list_check(self):
        return sorted(job for job in dir(self) if _RE_CHECK.match(job))

    def invoke_checks(self, cr, uid, context=None):
        list_method_checks = self.list_check()
        result = False
        for method_check_name in list_method_checks:
            method_check = getattr(self, method_check_name)
            result = method_check(cr, uid, ids=None, context=context)
            if result:
                return result
        return False

    def check_accountinge_account_move_state(self, cr, uid, ids,
                                             context=None):
        return False
        if context is None:
            context = {}
        result = self.pool.get('account.move').search(
            cr, uid, [('period_id', '=', context.get('period_id')),
                      ('company_id', '=', context.get('company_id')),
                      ('state', '<>', 'posted')], limit=1)
        if result:
            raise osv.except_osv(
                _('Error!'),
                _("You have journal entries open in this period"))
        return False

    def check_accountinge_open_period(self, cr, uid, ids,
                                      context=None):
        return False
        period_browse = self.pool.get('account.period').browse(
            cr, uid, context.get("period_id"))
        if period_browse.state == "draft":
            raise osv.except_osv(
                _('Error!'),
                _("The period which you try generate xml file, still open."))
        return False

    def check_accountinge_statement_valid_exists(self, cr, uid,
                                                 ids, context=None):
        if context is None:
            context = {}
        statement_type_check = []
        if context.get('check_acc_xml'):
            statement_type_check.append('CT')
        name_balance = ''
        if context.get('check_balance_xml'):
            if context.get('type_balance'):
                name_balance = 'B'+context.get('type_balance')
                statement_type_check.append(name_balance)
        name_journal_entry = ''
        if context.get('check_journal_entries_xml'):
            if context.get('type_journal_entry'):
                name_journal_entry = 'PL'+context.get('type_journal_entry')
                statement_type_check.append(name_journal_entry)
        statement_exist_ids = self.search(cr, uid, [
            ('company_id', '=', context.get('company_id')),
            ('chart_account_id', '=', context.get('chart_account_id')),
            ('period_id', '=', context.get('period_id')),
            ('fy_id', '=', context.get('fy_id')),
            ('type', 'in', statement_type_check),
            ('state', '<>', 'cancel')])
        if statement_exist_ids:
            raise osv.except_osv(_('Error!'),
                                 _('Statement already exists for this period \
                                    and company'))
        return False

    def get_balance_account(self,acc_ids,start,end):
        acc_data = []
        acc_obj = self.env["account.account"]
        move_line_obj = self.env["account.move.line"]
        parents=[]
        parent_list=[]
        for acc in acc_ids:
            acc_debit=0
            acc_credit=0
            debit_ini=0
            credit_ini=0
            balance_total_ini=0
            ini_parent=0
            debit_parent=0
            credit_parent=0
            balance_parent=0

            acco_ids=acc_obj.search([('tag_ids','in',acc.id)])
            
            if acc.l10n_mx_type_id.code not in parent_list:
                parents.append({'code': acc.l10n_mx_type_id.code,
                                     'SalIni': 0,
                                     'SalFin': 0,
                                     'debit':0,
                                     'credit': 0})
            for parent in parents:
                if parent['code'] not in parent_list:
                    parent_list.append(parent['code'])
            for c in acco_ids:
                
                balance_ini=move_line_obj.search([('account_id','=',c.id),('date','<',start)])
                for ini in balance_ini:
                    debit_ini+=ini.debit
                    credit_ini+=ini.credit
                balance_total_ini=debit_ini-credit_ini
                line_balance=move_line_obj.search([('account_id','=',c.id),('date','>=',start),('date','<=',end)])
                for line in line_balance:
                    acc_debit+=line.debit
                    acc_credit+=line.credit
                acc_data.append({'code': c.code,
                             'SalIni': balance_total_ini,
                             'SalFin': balance_total_ini+acc_debit-acc_credit,
                             'debit':acc_debit,
                             'credit': acc_credit})
            for p in parents:
                if p['code']==acc.l10n_mx_type_id.code:
                    p['SalIni']+=balance_total_ini
                    p['SalFin']+= balance_total_ini+acc_debit-acc_credit
                    p['debit']+=acc_debit
                    p['credit']+=acc_credit
            
        acc_data=acc_data+parents
        acc_data.sort(key=lambda x: x['code'])
        return acc_data

    @api.multi
    def action_cancel_draft(self):
        self.state = 'draft'
        #~ self.delete_workflow()
        #~ self.create_workflow()
        return True
        
    @api.multi
    def action_cancel(self):
        self.state = 'cancel'
        #~ self.delete_workflow()
        #~ self.create_workflow()
        return True
        
    @api.multi
    def action_done(self):
        self.state = 'done'
        #~ self.delete_workflow()
        #~ self.create_workflow()
        return True



    def get_aml_acc_chart(self,acc_chart_id,):
        account_ids_all = self.pool.get('account.account'). \
            search(cr, uid, [('id', 'child_of', acc_chart_id)])
        move_line_ids = self.pool.get('account.move.line'). \
            search(cr, uid, [("company_id", "=", context.get('company_id')),
                             ("period_id", "=", context.get('period_id')),
                             ('account_id', 'in', account_ids_all)])
        return move_line_ids

    def search_in_move_lines(self, cr, uid, search_by, context=None):
        if context is None:
            context = {}
        aml_ids = self.get_aml_acc_chart(cr, uid,
                                         context.get('chart_account_id'),
                                         context=context)
        obj_search_without_type = []
        move_line_pool = self.pool.get('account.move.line')
        move_line_ids = move_line_pool.search(cr, uid, [
            ('id', 'in', aml_ids),
            (search_by, '=', False)])
        for line in move_line_pool.browse(cr, uid, move_line_ids):
            if search_by == 'account_id.l10n_mx_type_id':
                obj_search_without_type.append(line.account_id.id)
            if search_by == 'journal_id.l10n_mx_type':
                obj_search_without_type.append(line.journal_id.id)
        return obj_search_without_type

    def check_accountinge_journal(self, cr, uid, ids=None, context=None):
        if context is None:
            context = {}
        if context.get('check_journal_entries_xml', False):
            journal_without_type = self.search_in_move_lines(
                cr, uid, 'journal_id.l10n_mx_type', context=context)
            if journal_without_type:
                return {
                    'name': 'Journal without Type MX',
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'account.journal',
                    'domain': [('id', 'in', journal_without_type), ],
                    'type': 'ir.actions.act_window',
                }
        return False

    def check_accountinge_balance_ok(self, cr, uid, ids=None, context=None):
        if context is None:
            context = {}
        if context.get('check_balance_xml', False):
            tot_ini = 0
            tot_fin = 0
            account_ids = self.pool.get('account.account'). \
                search(cr, uid, [('id', 'child_of',
                                  context.get('chart_account_id')),
                                 ('l10n_mx_type_id', '<>', False),
                                 ('level', '=', 1)])
            balance_list = self.get_balance_account(
                cr, uid, account_ids, context.get('period_id'),
                context.get('period_id'))
            for balance_acc in balance_list:
                tot_ini += balance_acc.get('SalIni')
                tot_fin += balance_acc.get('SalFin')
            if not tot_ini == 0 or not tot_fin == 0:
                raise osv.except_osv(_('Error !'),
                                     _('The sum of the opening balance and \
                                        the ending balance should be 0'))
        return False


