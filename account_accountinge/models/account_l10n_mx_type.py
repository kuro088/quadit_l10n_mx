# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp.osv import osv
from openerp import api, fields, models
from openerp.tools.translate import _
import re
from odoo.osv import expression


class L10nMxAccountType(models.Model):
    _name = 'l10n_mx.account.type'


    name=fields.Char('Name')
    code=fields.Char('Code')
    parent_id=fields.Many2one('l10n_mx.account.type', 'Parent')
    child_parent_ids=fields.One2many('l10n_mx.account.type','parent_id', 'Children')
    level=fields.Char()
    accounting_nature=fields.Selection([
            ('D', 'Debitable Account'),
            ('A', 'Creditable Account')], "Accounting Nature")


    @api.multi
    @api.depends('name', 'code')
    def name_get(self):
        result = []
        for account in self:
            name = account.code + ' ' + account.name
            result.append((account.id, name))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('code', '=ilike', name + '%'), ('name', operator, name)]
            if operator in expression.NEGATIVE_TERM_OPERATORS:
                domain = ['&', '!'] + domain[1:]
        accounts = self.search(domain + args, limit=limit)
        return accounts.name_get()
