# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from openerp.osv import osv
from openerp import api, fields, models
from openerp.tools.translate import _
import re


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    l10n_mx_type=fields.Selection([
            ('receipts', 'Receipts'),
            ('expenses', 'Expenses'),
            ('journal', 'Journal'),
        ], 'AccountingE Type', required=False,
            help="This field corresponds to the attribute required by \
the SAT to express the nature of the account entry.")
