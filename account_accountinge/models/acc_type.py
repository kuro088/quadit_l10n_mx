# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp.osv import osv
from openerp import api, fields, models

class AccountAccountTag(models.Model):

    _inherit = 'account.account.tag'


    l10n_mx_type_id=fields.Many2one("l10n_mx.account.type", "Type MX")

