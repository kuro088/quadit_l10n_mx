# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    "name": "Account Accountinge",
    "version": "12.0.1.0.0",
    "author": "Quadit, S.A. de C.V.",
    "category": "account",
    "description" : """
        Modulo para la contabilidad electronica 

    """,
    'website': 'https://www.quadit.mx',
    "license": "OEEL-1",
    "depends": [
        "account",
        "l10n_mx_facturae_33",
    ],
    "demo": [],
    "data": [
        "wizard/l10n_mx_accountinge_wizard_view.xml",
        "view/l10n_mx_statement_base_view.xml",
        "security/ir.model.access.csv",
        "view/account_account_type.xml",
        "view/account_inherit_view.xml",
        "data/l10n_mx_acc_type_data.xml",
        "view/account_journal_view.xml"
    ],
    "test": [],
    "js": [],
    "css": [],
    "qweb": [],
    "installable": True,
    "auto_install": False
}
