# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from openerp.exceptions import ValidationError
from openerp.tools import float_compare, float_is_zero


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    def _default_journal_id(self):
        # TODO: Add a way to set a journal type for payslips
        res = self.env['account.journal'].search(
            [('type', '=', 'purchase')], limit=1,
        )
        return res.id and res or False

    journal_id = fields.Many2one(
        'account.journal', 'Salary Journal',
        states={'draft': [('readonly', False)]}, readonly=True,
        required=True, default=lambda self: self._default_journal_id(),
    )
    move_id = fields.Many2one(
        'account.move', 'Accounting Entry', readonly=True, copy=False,
    )

    @api.model
    def create(self, vals):
        if 'journal_id' in self._context:
            vals.update({'journal_id': self._context.get('journal_id')})
        return super(HrPayslip, self).create(vals)

    @api.onchange('contract_id')
    def onchange_contract(self):
        self.journal_id = self.contract_id.journal_id.id or False
        return super(HrPayslip, self).onchange_contract()

#     def cancel_sheet(self, cr, uid, ids, context=None):
#         context = dict(context or {})
#         move_pool = self.pool.get('account.move')
#         move_ids = []
#         move_to_cancel = []
#         for slip in self.browse(cr, uid, ids, context=context):
#             if slip.move_id:
#                 move_ids.append(slip.move_id.id)
#                 if slip.move_id.state == 'posted':
#                     move_to_cancel.append(slip.move_id.id)
#         move_pool.button_cancel(cr, uid, move_to_cancel, context=context)
#         move_pool.unlink(cr, uid, move_ids, context=context)
#         return super(HrPayslip, self).cancel_sheet(cr, uid, ids, context=context)

#     def process_sheet(self, cr, uid, ids, context=None):
#         move_pool = self.pool.get('account.move')
#         period_pool = self.pool.get('account.period')
#         precision = self.pool.get('decimal.precision').precision_get(cr, uid, 'Payroll')

#         for slip in self.browse(cr, uid, ids, context=context):
#             line_ids = []
#             debit_sum = 0.0
#             credit_sum = 0.0
#             if not slip.period_id:
#                 ctx = dict(context or {}, account_period_prefer_normal=True)
#                 search_periods = period_pool.find(cr, uid, slip.date_to, context=ctx)
#                 period_id = search_periods[0]
#             else:
#                 period_id = slip.period_id.id

#             default_partner_id = slip.employee_id.address_home_id.id
#             name = _('Payslip of %s') % (slip.employee_id.name)
#             move = {
#                 'name': slip.number,
#                 'narration': name,
#                 'date': slip.date_to,
#                 'ref': slip.number.replace('/', ''),
#                 'journal_id': slip.journal_id.id,
#                 'period_id': period_id,
#             }
#             for line in slip.details_by_salary_rule_category:
#                 amt = slip.credit_note and -line.total or line.total
#                 if float_is_zero(amt, precision_digits=precision):
#                     continue
#                 partner_id = line.salary_rule_id.register_id.partner_id and line.salary_rule_id.register_id.partner_id.id or default_partner_id
#                 debit_account_id = line.salary_rule_id.account_debit.id
#                 credit_account_id = line.salary_rule_id.account_credit.id

#                 if debit_account_id:
#                     vals = {
#                         'name': line.name,
#                         'date': slip.date_to,
#                         'partner_id': partner_id or False,
#                         'account_id': debit_account_id,
#                         'journal_id': slip.journal_id.id,
#                         'period_id': period_id,
#                         'debit': amt > 0.0 and amt or 0.0,
#                         'credit': amt < 0.0 and -amt or 0.0,
#                         'analytic_account_id': line.salary_rule_id.analytic_account_id and line.salary_rule_id.analytic_account_id.id or False,
#                         'tax_code_id': line.salary_rule_id.account_tax_id and line.salary_rule_id.account_tax_id.id or False,
#                         'tax_amount': line.salary_rule_id.account_tax_id and amt or 0.0,
#                     }
#                     debit_line = (0, 0, vals)
#                     line_ids.append(debit_line)
#                     debit_sum += debit_line[2]['debit'] - debit_line[2]['credit']

#                 if credit_account_id:
#                     vals = {
#                         'name': line.name,
#                         'date': slip.date_to,
#                         'partner_id': partner_id or False,
#                         'account_id': credit_account_id,
#                         'journal_id': slip.journal_id.id,
#                         'period_id': period_id,
#                         'debit': amt < 0.0 and -amt or 0.0,
#                         'credit': amt > 0.0 and amt or 0.0,
#                         'analytic_account_id': line.salary_rule_id.analytic_account_id and line.salary_rule_id.analytic_account_id.id or False,
#                         'tax_code_id': line.salary_rule_id.account_tax_id and line.salary_rule_id.account_tax_id.id or False,
#                         'tax_amount': line.salary_rule_id.account_tax_id and amt or 0.0,
#                     }
#                     credit_line = (0, 0, vals)
#                     line_ids.append(credit_line)
#                     credit_sum += credit_line[2]['credit'] - credit_line[2]['debit']

#             if float_compare(credit_sum, debit_sum, precision_digits=precision) == -1:
#                 acc_id = slip.journal_id.default_credit_account_id.id
#                 if not acc_id:
#                     raise ValidationError(
#                         _('The Expense Journal "%s" has not properly '
#                           'configured the Credit Account!') %
#                         (slip.journal_id.name),
#                     )
#                 adjust_credit = (0, 0, {
#                     'name': _('Adjustment Entry'),
#                     'date': slip.date_to,
#                     'partner_id': False,
#                     'account_id': acc_id,
#                     'journal_id': slip.journal_id.id,
#                     'period_id': period_id,
#                     'debit': 0.0,
#                     'credit': debit_sum - credit_sum,
#                 })
#                 line_ids.append(adjust_credit)

#             elif float_compare(debit_sum, credit_sum, precision_digits=precision) == -1:
#                 acc_id = slip.journal_id.default_debit_account_id.id
#                 if not acc_id:
#                     raise ValidationError(
#                         _('The Expense Journal "%s" has not properly '
#                           'configured the Debit Account!') %
#                         (slip.journal_id.name),
#                     )
#                 adjust_debit = (0, 0, {
#                     'name': _('Adjustment Entry'),
#                     'date': slip.date_to,
#                     'partner_id': False,
#                     'account_id': acc_id,
#                     'journal_id': slip.journal_id.id,
#                     'period_id': period_id,
#                     'debit': credit_sum - debit_sum,
#                     'credit': 0.0,
#                 })
#                 line_ids.append(adjust_debit)

#             move.update({'line_id': line_ids})
#             move_id = move_pool.create(cr, uid, move, context=context)
#             self.write(
#                 cr, uid, [slip.id],
#                 {'move_id': move_id, 'period_id': period_id},
#                 context=context,
#             )
#             if slip.journal_id.entry_posted:
#                 move_pool.post(cr, uid, [move_id], context=context)
#         return super(HrPayslip, self).process_sheet(cr, uid, [slip.id], context=context)
