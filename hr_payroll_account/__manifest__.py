# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Payroll Accounting',
    'version': '11.0.1.0.0',
    'category': 'Human Resources',
    'author': 'Quadit, S.A. de C.V.',
    'license': 'OEEL-1',
    'website': 'https://www.quadit.mx',
    'depends': [
        'hr_payroll',
        'account',
        'hr_expense'
    ],
    'data': [
        'views/hr_payroll_account_view.xml',
    ],
    'demo': [
        'hr_payroll_account_demo.xml',
    ],
    'installable': True,
    'auto_install': False,
}
