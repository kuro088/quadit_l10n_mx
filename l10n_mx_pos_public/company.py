# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, tools, _

class ResCompany(models.Model):
    _inherit = 'res.company'

    public_partner_pos_id=fields.Many2one('res.partner','Cliente público en general para POS')

    
class PosOrder(models.Model):
    _inherit = 'pos.order'
    

    pos_public =fields.Boolean('Factura global', default=False)

    

