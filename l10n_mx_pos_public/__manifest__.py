# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    "name" : "l10n_mx_pos_public",
    "version": "11.0.1.0.0",
    "author": "Quadit, S.A. de C.V.",
    "category" : "sale",
    "description" : """This module add wizard in pos list, to create invoice general public from pos""",
    'website': 'https://www.quadit.mx',
    "license": "OEEL-1",
    "depends" : ['point_of_sale','l10n_mx_facturae_33'],
    "demo" : [],
    "data" : ['company_view.xml','wizard/wizard_invoice_view.xml','pos_order_view.xml'],
    "installable" : True,
    "active" : False,
}
