# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from openerp.osv import osv
from odoo import tools
import time
from collections import Counter
from odoo.exceptions import ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

class wizard_invoice_pos(osv.TransientModel):
    _name = 'wizard.invoice.pos'
    

    partner_id= fields.Many2one('res.partner', 'Cliente', required=True)
    journal_id=fields.Many2one('account.journal', 'Diario')
    date=fields.Datetime('Fecha')
    sign=fields.Boolean('Validar y timbrar automáticamente')
    description=fields.Char('Descripcion')
    amount= fields.Float('Monto')
    sub= fields.Float('SubTotal')
    pos_ids= fields.Many2many('pos.order', 'wizard_pos_invoice_ids', 'wizard_id', 'pos_id', 'Pos')


    
    @api.model
    def default_get(self, fields):
        res = super(wizard_invoice_pos, self).default_get(fields)
        user=self.env['res.users'].browse(self._uid)
        res['partner_id']=user.company_id.public_partner_pos_id.id or False
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        lista=[]
        name=''
        monto=0
        sub=0
        pay_form=False
        for pos in active_ids:
            p=self.env['pos.order'].browse(pos)
            if p.state=='done' and not p.pos_public:
                lista.append(pos)
                name += str(p.name[0])+ str(p.name.split('/')[1] or '') + ','
                monto += p.amount_total
                sub += p.amount_total-p.amount_tax

        if not lista:
            raise ValidationError('Aviso ! Los pedidos seleccionados no se pueden facturar, posiblemente ya han sido facturados anteriormente o no han cerrado caja del día.')
        res['pos_ids'] = lista
        res['description'] = "Factura Global: " + name[:-1]
        res['amount'] = monto
        res['sub'] = sub

        res['date'] = time.strftime('%Y-%m-%d %H:%M:%S')
        return res
        
    def create_invoice(self):
        for wiz in self:
            forma_ids=[]
            for j in wiz.pos_ids:
                pos_line=self.env['pos.order'].browse(j.id)
                for pay in pos_line.statement_ids:
                    if pay.amount > 0:
                        forma_ids.append(pay.journal_id.forma_pago_id.id)
            c=Counter(forma_ids)
            forma_id=max(c, key=c.get)
            account_id=False
            journal_id = self.env['account.invoice'].default_get(['journal_id'])['journal_id']
            journal = self.env['account.journal'].browse(journal_id)
            account_id=journal.default_credit_account_id.id
            met_id=self.env['res.met.pago'].search( [('name','=','PUE')])
            uom_id=self.env['product.uom'].search( [('name','=','ACT')])
            if not uom_id:
                clave_uom_id=self.env['clave.prod.uom'].search( [('clave','=','ACT')])
                categ_id=self.env['product.uom.categ'].search( [('name','=','Unidad')])
                uom_id=self.env['product.uom'].create({'name':'ACT','claveuni_id':clave_uom_id.id,'category_id':categ_id.id})
            clave_id=self.env['clave.prod.serv'].search( [('name','=','01010101')])
            tax_id = self.env['product.product'].default_get( ['taxes_id'])['taxes_id']
            line_ids=[]
            for l in wiz.pos_ids:
                if l.amount_tax==0:
                    line={
                        'name':'Venta del ticket :' + str(l.name),
                        'account_id':account_id,
                        'quantity': 1,
                        'price_unit':l.amount_total- l.amount_tax,
                        'invoice_line_tax_ids':[],
                        'claveserv_id':clave_id.id,
                        'uom_id':uom_id.id,
                    }
                    line_ids.append(self.env['account.invoice.line'].create(line).id)
                else:
                    line={
                        'name':'Venta del ticket :' + str(l.name),
                        'account_id':account_id,
                        'quantity': 1,
                        'price_unit':l.amount_total- l.amount_tax,
                        'invoice_line_tax_ids':tax_id,
                        'claveserv_id':clave_id.id,
                        'uom_id':uom_id.id,
                    }
                    line_ids.append(self.env['account.invoice.line'].create(line).id)
            invoice={
                'partner_id':wiz.partner_id.id,
                #~ 'invoice_datetime':wiz.date,
                'name':wiz.description[:-1],
                'account_id': wiz.partner_id.property_account_receivable_id.id,
                'type': 'out_invoice',
                'journal_id': wiz.journal_id.id or journal_id,
                'origin':wiz.description,
                'date_invoice':time.strftime('%Y-%m-%d %H:%M:%S'),
                'invoice_line_ids': [(6, 0, line_ids)],
                'forma_pago_id':forma_id,
                'met_pago_id':met_id.id,
                'uso_cfdi_id':wiz.partner_id.uso_cfdi_id.id,
            }
            id_invoice=self.env['account.invoice'].create(invoice)


            for line in wiz.pos_ids:
                line.write({'pos_public':True,'invoice_id':id_invoice.id})

            id_invoice.action_invoice_open()

            try:
                id_invoice.sign_cfdi()
            except:
                raise ValidationError("Error al firmar XML")
                
            id_invoice.write({'sign': True})
            self.env.ref('l10n_mx_facturae_33.custom_invoice_pdf').render_qweb_pdf([id_invoice.id])[0]
        for invoice in id_invoice:
            amls_to_reconcile = self.env['account.move.line']
            for order in wiz.pos_ids:
                for statement in order.statement_ids:
                        for account_move in statement.journal_entry_ids:
                            for move_line in account_move.filtered(lambda r: not r.reconciled and r.account_id.internal_type in ('payable', 'receivable')):
                                amls_to_reconcile += move_line
            amls_to_reconcile += invoice.move_id.line_ids.filtered(lambda r: not r.reconciled and r.account_id.internal_type in ('payable', 'receivable'))
            amls_to_reconcile.reconcile(writeoff_acc_id=False, writeoff_journal_id=False)
        view_id = self.env.ref('account.invoice_form').id
        
        return {
            'name':'Factura general del día',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'res_model': 'account.invoice',
            'context': "{'type':'out_invoice'}",
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': id_invoice.id or False,
        }
