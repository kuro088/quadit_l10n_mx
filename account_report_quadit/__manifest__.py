# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    "name": "account_report_quadit",
    "version": "11.0.1.0.0",
    "author": "Quadit, S.A. de C.V.",
    "category": "account",
    "description": """
    Module create reports of taxes and reports accountad """,
    'website': 'https://www.quadit.mx',
    "license": "OEEL-1",
    "depends": ['account'],
    "demo": [],
    "data": [
        'wizard/account_tax_view.xml',
        'views/account_report.xml',
        'views/report_tax_q.xml',
        'views/report_tax_pen.xml',
        
    ],
    "installable": True,
}
