# -*- coding: utf-8 -*-

from odoo import api, models, _
from odoo.exceptions import UserError


class ReportTaxPen(models.AbstractModel):
    _name = 'report.account_report_quadit.report_tax_pen'

    @api.model
    def get_report_values(self, docids, data=None):
        if not data.get('form'):
            raise UserError(_("Form content is missing, this report cannot be printed."))
        return {
            'data': data['form'],
            'lines': self.get_lines(data.get('form')),
        }

    def _sql_from_amls_one(self):
        sql = """SELECT "account_move_line".tax_line_id, COALESCE(SUM("account_move_line".debit-"account_move_line".credit), 0)
                    FROM %s
                    WHERE %s AND "account_move_line".tax_exigible GROUP BY "account_move_line".tax_line_id"""
        return sql

    def _sql_from_amls_two(self):
        sql = """SELECT r.account_tax_id, COALESCE(SUM("account_move_line".debit-"account_move_line".credit), 0)
                 FROM %s
                 INNER JOIN account_move_line_account_tax_rel r ON ("account_move_line".id = r.account_move_line_id)
                 INNER JOIN account_tax t ON (r.account_tax_id = t.id)
                 WHERE %s AND "account_move_line".tax_exigible GROUP BY r.account_tax_id"""
        return sql

    def _compute_from_amls(self, options, taxes):
        #compute the tax amount
        sql = self._sql_from_amls_one()
        tables, where_clause, where_params = self.env['account.move.line']._query_get()
        query = sql % (tables, where_clause)
        self.env.cr.execute(query, where_params)
        results = self.env.cr.fetchall()
        for result in results:
            if result[0] in taxes:
                taxes[result[0]]['tax'] = abs(result[1])

        #compute the net amount
        sql2 = self._sql_from_amls_two()
        query = sql2 % (tables, where_clause)
        self.env.cr.execute(query, where_params)
        results = self.env.cr.fetchall()
        for result in results:
            if result[0] in taxes:
                taxes[result[0]]['net'] = abs(result[1])

    @api.model
    def get_lines(self, options):
        taxes = {}
        tax_ids=[]
        for tax in self.env['account.tax'].search([('type_tax_use', '!=', 'none')]):
            if tax.children_tax_ids:
                for child in tax.children_tax_ids:
                    if child.type_tax_use != 'none':
                        continue
                    taxes[child.id] = {'tax': 0, 'net': 0, 'name': child.name, 'type': tax.type_tax_use,}
                    tax_ids.append(child)
            else:
                taxes[tax.id] = {'tax': 0, 'net': 0, 'name': tax.name, 'type': tax.type_tax_use}
                tax_ids.append(tax)
        groups = dict((tp, []) for tp in ['sale', 'total'])
        move_lines=self.env['account.move.line'].search([('invoice_id','!=',None),('tax_line_id','!=',None),('date', '<=', options['date_to']),('date', '>=', options['date_from'])])
        sum_base=0
        sum_total=0
        sum_monto=0
        if not move_lines:
            return groups
        for line in move_lines:
            if line.journal_id.type=='sale':
                #~ child_lines=move_lines=self.env['account.move.line'].search([('name','=',line.move_id.name)])
                #~ line_ids=[]

                #~ for h in child_lines:
                    
                    #~ line_ids.append({'date':h.date,'acc':line.tax_line_id.cash_basis_account.code + '- '+line.tax_line_id.cash_basis_account.name,'pago':h.ref,'amount':h.debit})
                    #~ sum_pago+=h.debit
                #~ self.with_context(date_from=options['date_from'], date_to=options['date_to'], strict_range=True)._compute_from_amls(options, taxes)
            
                groups['sale'].append({'date':line.date,'acc':line.account_id.code + '-'+ line.account_id.name,'tax':line.name,'invoice':line.move_id.name,'amount_base':line.tax_base_amount,
                'amount':line.credit,'total':(line.credit-line.credit_cash_basis)})
                sum_monto+=line.credit
                sum_base+=line.tax_base_amount
                sum_total+=(line.credit-line.credit_cash_basis)
        groups['total'].append({'total_base':sum_base,'total_monto':sum_monto,'total': sum_total})
        return groups
