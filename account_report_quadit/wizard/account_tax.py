# -*- coding: utf-8 -*-

from odoo import models


class AccountTaxQ(models.TransientModel):
    _inherit = "account.common.report"
    _name = 'account.tax.q'
    _description = 'Tax Report'


    def _print_report(self, data):
        data['form']['company']=self.company_id.name
        return self.env.ref('account_report_quadit.action_report_account_tax_q').report_action(self, data=data)
        
class AccountTaxPen(models.TransientModel):
    _inherit = "account.common.report"
    _name = 'account.tax.pen'
    _description = 'IVA Pendiente'


    def _print_report(self, data):
        data['form']['company']=self.company_id.name
        return self.env.ref('account_report_quadit.action_report_account_tax_pen').report_action(self, data=data)
