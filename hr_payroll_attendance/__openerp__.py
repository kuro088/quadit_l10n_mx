# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Payroll - Attendance',
    'version': '11.0.1.0.0',
    'category': 'Human Resources',
    'license': 'OEEL-1',
    'author': 'Quadit, S.A. de C.V.',
    'website': 'https://www.quadit.mx',
    'depends': [
        'hr_payroll',
        'hr_attendance',
    ],
    'data': [
    ],
    'installable': True,
    'auto_install': True,
    'application': False,
}
