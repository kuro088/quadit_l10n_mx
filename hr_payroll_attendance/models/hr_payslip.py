# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, models


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    @api.model
    def _have_worked(self, working_calendar, employee, day):
        """
        Determine if employee have worked on a given day

        Search in hr.attendance records in order to see if employee have
        arrive to work on a given day.

        @param working_calendar: The calendar that define working days
        @type working_calendar: resource.calendar
        @param employee: The employee to test
        @type employee: hr.employee
        @param day: The day to test
        @type day: Datetime
        @return: True if employee have worked False otherwise
        @rtype: bool
        """
        have_worked = self.env['hr.attendance'].have_worked(
            working_calendar, employee, day,
        )
        return have_worked

    @api.model
    def _late_hours(self, working_calendar, employee, day):
        """
        Determine if employee arrive late to work on a given day

        @param working_calendar: The calendar that define working days
        @type working_calendar: resource.calendar
        @param employee: The employee to test
        @type employee: hr.employee
        @param day: The day to test
        @type day: Datetime
        @return: Hours employee arrives late
        @rtype: int
        """
        late_hours = self.env['hr.attendance'].late_hours(
            working_calendar, employee, day,
        )
        return late_hours

    @api.model
    def _overtime_hours(self, working_calendar, employee, day):
        """
        Determine if employee do overtime on a given day
        The function in this module is just a placeholder, other modules must
        inherit and extend.

        @param working_calendar: The calendar that define working days
        @type working_calendar: resource.calendar
        @param employee: The employee to test
        @type employee: hr.employee
        @param day: The day to test
        @type day: Datetime
        @return: Hours employee do on overtime
        @rtype: int
        """
        overtime_hours = self.env['hr.attendance'].overtime_hours(
            working_calendar, employee, day,
        )
        return overtime_hours
