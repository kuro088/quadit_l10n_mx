# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _, tools


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    material_cost = fields.Float(
        string='Material Cost', readonly=True)
    production_cost = fields.Float(
        string='Production Cost', readonly=True)
    subcontracting_cost = fields.Float(
        string='Subcontracting Cost', readonly=True)
    landed_cost = fields.Float(
        string='Landed Cost', readonly=True)
