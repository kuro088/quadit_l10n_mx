import base64
import logging
import time
from datetime import datetime, timedelta
from os import path
from jinja2 import Environment, FileSystemLoader, StrictUndefined
from pytz import timezone
from lxml import etree, objectify
from suds.client import Client
from dateutil.relativedelta import relativedelta

from odoo import _, api, fields, models
from odoo.exceptions import ValidationError

from .safe_eval import ast, Transformer
from .jinja_extension import RaiseExtension

CFDI_XSLT_CADENA = 'l10n_mx_facturae_33/SAT/cadenaoriginal_3_3.xslt'

_logger = logging.getLogger(__name__)

_errors = {
    'bank_account': _('Missing bank account for employee.'),
    'employer_number': _('Missing employer number.'),
    'social_security': _('Missing social security number for employee.'),
    'payslip_type': _('Missing payslip type on payroll structure.'),
    'contract_type': _('Missing code for contract type.'),
    'company_curp': _('CURP is required when company is a Person.'),
    'tipo_regimen_tipo_contrato': _(
        'When Contract Type code is between 01 and 08, '
        'then Employee Regime must be 02, 03 or 04.',
    ),
}


def _domain2statement(domain):
    statement = ''
    operator = False
    for d in domain:
        if not operator:
            if isinstance(d, str):
                if d == '|':
                    operator = ' or'
                continue
            operator = False
        statement += ' o.' + str(d[0]) + ' '
        statement += (d[1] == '=' and '==' or d[1]) + ' '
        statement += (
            (isinstance(d[2], str) and '\'' + d[2] + '\'' or str(d[2]))
        )
        if d != domain[-1]:
            statement += operator or ' and'
        operator = False


def required_field(value, error):
    """Filter used to enforce a required value on template

    @param value: Value to evaluate and enforce presence
    @type value: any
    @param error: Error message to display if value not present
    @type error: str
    @raise ValidationError: if value is not set
    """
    if not value:
        raise ValidationError(error)
    return value


class HrPayslip(models.Model):
    _name = 'hr.payslip'
    _inherit = ['hr.payslip', 'mail.thread']

    @api.model
    def _default_currency_id(self):
        return self.env.user.company_id.currency_id

    no_certificado = fields.Char(
        'No. Certificate', size=64, copy=False,
        help='Number of serie of certificate used for the invoice',
    )
    certificado = fields.Text(
        'Certificate', size=64, copy=False,
        help='Certificate used in the invoice',
    )
    sello = fields.Text('Stamp', size=512, copy=False, help='Digital Stamp')
    cadena_original = fields.Text(
        'String Original', size=512, copy=False,
        help='Data stream with the information contained in the electronic'
        ' invoice',
    )
    cfdi_folio_fiscal = fields.Char(
        'CFD-I Folio Fiscal', size=64, copy=False,
        help='Folio used in the electronic invoice',
    )
    date_payroll = fields.Datetime(
        'Payroll Date', readonly=True,
        states={'draft': [('readonly', False)]}, index=True,
        help='Keep empty to use the current date',
    )
    payment_type = fields.Many2one(
        'res.forma.pago',
        help='Indicates the way it was paid or will be paid the invoice,'
        'where the options could be: check, bank transfer, reservoir in '
        'account bank, credit card, cash etc. If not know as will be '
        'paid the invoice, leave empty and the XML show “Unidentified”.',
    )
    currency_id = fields.Many2one(
        'res.currency', 'Currency', required=True, readonly=True,
        states={'draft': [('readonly', False)]}, track_visibility='always',
        default=lambda self: self._default_currency_id(),
    )
    cfdi_fecha_timbrado = fields.Datetime(
        'CFD-I Date Stamping', copy=False,
        help='Date when is stamped the electronic invoice',
    )
    cfdi_sello = fields.Text(
        'CFD-I Stamp', copy=False, help='Sign assigned by the SAT',
    )
    cfdi_cadena_original = fields.Text(
        'CFD-I Original String', copy=False,
        help='Original String used in the electronic invoice',
    )
    cfdi_no_certificado = fields.Char(
        'SAT CFDI Certificado', copy=False,
        help='SAT Certificate used for sign current file',
    )
    date_tz = fields.Datetime(
        compute='_compute_date_tz', string='Date Payroll with TZ',
        help='Date of Invoice with Time Zone',
    )
    antiquity = fields.Integer(
        compute='_compute_antiquity', help='Antiquity in weeks',
    )
    total_days = fields.Integer(
        compute='_compute_total_days',
        help='Helper field to compute the total days included in payslip',
    )
    cfdi_nomina = fields.Binary()
    source_resource = fields.Selection([
        ('IP', 'Own income'),
        ('IF', 'Federal income'),
        ('IM', 'Mixed income')],
        help='Used in XML to identify the source of the resource used '
        'for the payment of payroll of the personnel that provides or '
        'performs a subordinate or assimilated personal service to salaries '
        'in the dependencies. This value will be set in the XML attribute '
        '"OrigenRecurso" to node "EntidadSNCF".')
    amount_sncf = fields.Float(
        'Own resource', help='When the attribute in "Source Resource" is "IM" '
        'this attribute must be added to set in the XML attribute '
        '"MontoRecursoPropio" in node "EntidadSNCF", and must be less that '
        '"TotalPercepciones" + "TotalOtrosPagos"')

    @api.multi
    def _compute_date_tz(self):
        for record in self:
            record.date_tz = timezone('America/Mexico_City').localize(
                record.date_payroll).strftime('%Y-%m-%d %H:%M:%S')

    @api.multi
    def _compute_antiquity(self):
        for record in self.filtered('contract_id.date_start'):
            begin = record.contract_id.date_start
            end = record.date_to + timedelta(days=1)
            record.antiquity = (end - begin).days / 7

    @api.multi
    def _compute_total_days(self):
        for record in self:
            total_days = 0
            for days in record.worked_days_line_ids:
                total_days += days.number_of_days
            record.total_days = total_days

    @api.multi
    def _get_imss(self, fee_name='c_obrera'):
        # Calculate amount to pay for IMSS according to actual IMSS tables
        # pylint: disable=eval-used,unused-variable
        result = 0
        transformer = Transformer()
        smgvdf = self.env['hr.payroll'].search(  # noqa: F841
            [('date_start', '<=', self.date_from)], limit=1,
            order='date_start desc',
        ).smgvdf
        sbc = self.contract_id.integrated_wage  # noqa: F841
        total_days = self.contract_id.isr_table.number_of_days
        imss_table = self.env['imss.table'].search([])

        for row in imss_table:
            tree = ast.parse(row.base, mode='eval')
            # raises RuntimeError on invalid code
            transformer.visit(tree)

            # compile the ast into a code object
            clause = compile(tree, '<AST>', 'eval')

            # and eval the compiled object
            base = eval(clause)
            if base > 0:
                fee = getattr(row, fee_name)
                result = result + base * (fee / 100) * total_days
        return result

    @api.multi
    def cancel_done_sheet(self):
        """ Cancel payslip on SAT and also cancel payslip """
        cfdi_obj = self.env['ir.attachment.facturae.mx']
        for payslip in self:
            # First we are going to try cancel the payslip, and only when
            # the payslip is properly cancelled then we try to cancel the
            # cfdi on SAT
            payslip.cancel_sheet()

            # Search for cfdis to cancel on SAT
            cfdis = cfdi_obj.search([
                ('res_id', '=', payslip.id),
                ('type_attachment', '=', 'hr.payslip'),
            ])
            for cfdi in cfdis:
                if cfdi.state != 'cancel':
                    cfdi.signal_cancel([cfdi.id])

    @api.multi
    def action_printable(self):
        attachment_obj = self.env['ir.attachment']
        ir_attach_obj = self.env['ir.attachment.facturae.mx']
        for payslip in self:
            # the value of data is in this way because the report is aeroo
            data = {
                'model': 'hr.payslip',
                'id': payslip.id,
                'report_type': 'aeroo',
            }
            result, mimetype = self.render_report(
                self._cr, self._uid, [payslip.id],
                'payroll_report_aeroo', data, context=self._context,
            )
            vat = payslip.company_id.partner_id.vat_split
            name = vat + '_' + payslip.number + '.' + mimetype
            attachment = attachment_obj.create({
                'name': name,
                'datas_fname': name,
                'datas': base64.encodestring(result),
                'res_model': 'hr.payslip',
                'res_id': payslip.id,
            })
            ir_attach_obj.write({
                'file_xml_sign': attachment.id,
                'state': 'signed',
                'last_date': time.strftime('%Y-%m-%d %H:%M:%S'),
            })
        return

    @api.model
    def create_ir_attachment_facturae(self):
        for payslip in self:
            payslip._get_signed_xml()
            vat = payslip.company_id.partner_id.vat
            name = '%s_%s.xml' % (vat or '', payslip.number or '')
            attachment = self.env['ir.attachment'].create({
                'name': name,
                'res_model': 'hr.payslip',
                'res_id': payslip.id,
                'datas': self.cfdi_nomina,
                'datas_fname': name,
            })
            payslip.message_post(
                body=_('CFDI generated'),
                attachment_ids=[attachment.id])
            # call the function which creates pdf report
            # payslip.action_printable()
        return

    def action_payslip_done(self):
        for payslip in self:
            if not payslip.date_payroll:
                payslip.date_payroll = fields.Datetime.now()
            payslip.create_ir_attachment_facturae()
        return super().action_payslip_done()

    @api.model
    def get_worked_day_lines(self, contracts, date_from, date_to):
        """Overwrite WORK100 to get all days
        """
        result = super().get_worked_day_lines(contracts=contracts, date_from=date_from, date_to=date_to)
        res = []
        number_of_hours = 0
        for line in result:
            if line.get('code') == 'WORK100':
                number_of_hours += line.get('number_of_hours')
                continue
            res.append(line)
        for contract in contracts.filtered(
                lambda contract: contract.resource_calendar_id):
            day_from = fields.datetime.combine(fields.Date.from_string(date_from), fields.datetime.min.time())
            day_to = fields.datetime.combine(fields.Date.from_string(date_to), fields.datetime.max.time())

            # compute leave days
            calendar = contract.resource_calendar_id
            tz = timezone(calendar.tz)
            day_leave_intervals = contract.employee_id.list_leaves(
                day_from, day_to, calendar=contract.resource_calendar_id)
            current_leave_days = 0
            for day, hours, _leave in day_leave_intervals:
                work_hours = calendar.get_work_hours_count(
                    tz.localize(fields.datetime.combine(day, fields.datetime.min.time())),
                    tz.localize(fields.datetime.combine(day, fields.datetime.max.time())),
                    compute_leaves=False,
                )
                if work_hours:
                    current_leave_days += hours / work_hours

            # compute worked days
            work_data = relativedelta(date_to, date_from).days
            attendances = {
                'name': _("Normal Working Days paid at 100%"),
                'sequence': 1,
                'code': 'WORK100',
                'number_of_days': work_data + 1 - current_leave_days,
                'number_of_hours': number_of_hours,
                'contract_id': contract.id,
            }

            res.append(attendances)
        return res

    @api.multi
    def _get_cfdi_dict_data(self):
        self.ensure_one()
        # Crate an environment of jinja in the templates directory
        env = Environment(
            loader=FileSystemLoader(
                path.join(
                    path.dirname(path.abspath(__file__)), '../templates',),),
            extensions=[RaiseExtension],
            undefined=StrictUndefined, autoescape=True,
        )
        # Add custom filters
        env.filters['required_field'] = required_field
        template = env.get_template('nomina1_2.mako')

        total_percepciones_grav = 0.0
        total_percepciones_ex = 0.0
        # Iterate in each line_ids to get importeGravado and importeExento
        for line in self.line_ids.filtered('total'):
            if line.category_id.code == 'PERC':
                percepcion = {
                    'tipo': line.salary_rule_id.code_sat,
                    'concepto': line.name,
                    'importegravado': line.total,
                    'importeexento': 0,
                    'clave': line.code,
                }
                total_percepciones_grav += percepcion['importegravado']
                total_percepciones_ex += percepcion['importeexento']
            if line.category_id.code == 'PERC_E':
                percepcion = {
                    'tipo': line.salary_rule_id.code_sat,
                    'concepto': line.name,
                    'importegravado': 0,
                    'importeexento': line.total,
                    'clave': line.code,
                }
                total_percepciones_grav += percepcion['importegravado']
                total_percepciones_ex += percepcion['importeexento']
        emitter = self.company_id
        # Covert date to a date with time zone
        date_payroll_tz = datetime.now(timezone(
            'America/Mexico_City')).strftime('%Y-%m-%dT%H:%M:%S')
        # Covert date to an adequate date
        payment_day = self.date_tz.strftime('%Y-%m-%d')
        # Create a template and pass a context
        total_percepciones = sum(self.line_ids.filtered(
            lambda l: l.category_id.code in ('PERC', 'PERC_E')).mapped('total'))
        total_deducciones = sum(self.line_ids.filtered(
            lambda l: l.category_id.code == 'DEDC').mapped('total'))
        total_otrospagos = sum(self.line_ids.filtered(
            lambda l: l.category_id.code == 'OTROS').mapped('total'))
        total_impuestos_retenidos = sum(self.line_ids.filtered(
            lambda l: l.salary_rule_id.code_sat == '002' and l.category_id.code == 'DEDC').mapped('total'))  # noqa
        days_overtime = lambda l: sum([w.number_of_days for w in l.payslip.id.worked_days_line_ids if w.code == l.code])  # noqa
        hours_overtime = lambda l: sum([w.number_of_hours for w in l.payslip.id.worked_days_line_ids if w.code == l.code])  # noqa
        total_other_ded = sum(self.line_ids.filtered(
            lambda l: l.salary_rule_id.code_sat != '002' and l.category_id.code == 'DEDC').mapped('total'))  # noqa
        has_other = self.line_ids.filtered(
            lambda l: l.category_id.code == 'OTROS')
        certificate_id = self.company_id.certificate_id
        if not certificate_id:
            raise ValidationError(_(
                'No tienes definido certificado para esta compañia !'))
        xml_data = template.render(
            payslip=self, emitter=emitter,
            percepciones_totalgravado=total_percepciones_grav,
            percepciones_totalexento=total_percepciones_ex,
            date=date_payroll_tz,
            payment_day=payment_day,
            errors=_errors, total_percepciones=total_percepciones,
            total_deducciones=total_deducciones,
            total_otrospagos=total_otrospagos,
            TotalImpuestosRetenidos=total_impuestos_retenidos,
            days_overtime=days_overtime, hours_overtime=hours_overtime,
            total_other_ded=total_other_ded, has_other=has_other,
            certificate=certificate_id,
            cer_data=certificate_id.sudo().get_data()[0].decode(),
        )
        _logger.debug('Jinja XML result: %s', xml_data)
        self.cfdi_nomina = base64.b64encode(xml_data.encode())
        return True

    @api.multi
    def _get_signed_xml(self):
        for payroll in self:
            payroll._get_cfdi_dict_data()
            payroll.set_sign_data()
            company_id = payroll.company_id.id
            copy_context = dict(self._context)
            copy_context.update({'company_id': company_id})
            self.sign_cfdi()
            return True

    @api.multi
    def sign_cfdi(self):
        invoice = self.env['account.invoice']
        for record in self.filtered('cfdi_nomina'):
            pac_usr = ''
            pac_pwd = ''
            pac_url = ''
            pac_params_ids = self.env['params.pac'].search([
                ('method_type', '=', 'firmar'),
                ('company_id', '=', record.company_id.id),
                ('active', '=', True)], limit=1)
            if not pac_params_ids:
                raise ValidationError(_(
                    'No tienes parametros del PAC configurados'))
            pac_usr = pac_params_ids.user
            pac_pwd = pac_params_ids.password
            pac_url = pac_params_ids.url_webservice
            client = Client(pac_url, cache=None)
            xml = [self.cfdi_nomina.decode('utf-8')]
            resultado = client.service.stamp(xml, pac_usr, pac_pwd)
            if resultado.Incidencias:
                code = getattr(resultado.Incidencias[0][0], 'CodigoError',
                               None)
                msg = getattr(resultado.Incidencias[0][0],
                              'MensajeIncidencia' if code != '301' else 'ExtraInfo', None)  # noqa
                raise ValidationError(_(' %s - %s ' % (code, msg)))

            if not resultado.Incidencias or None:
                folio_fiscal = resultado.UUID or False

                original_string = invoice._create_original_str(resultado)
                self.cfdi_nomina = base64.b64encode(resultado.xml.encode())
                data_cfdi = {
                    'cfdi_folio_fiscal': folio_fiscal,
                    'cfdi_cadena_original': original_string,
                }
                self.write(data_cfdi)

    def set_sign_data(self):
        invoice_obj = self.env['account.invoice']
        for payroll in self:
            company = payroll.company_id
            certificate_id = company.certificate_id
            if not certificate_id:
                raise ValidationError(_(
                    'No tienes definido certificado para esta compañia !'))
            xml = base64.decodestring(payroll.cfdi_nomina)
            cadena = invoice_obj.generate_cadena_original(
                xml, {'path_cadena': CFDI_XSLT_CADENA})
            sello = certificate_id.get_sello(cadena)
            tree = objectify.fromstring(xml)
            tree.attrib['Sello'] = sello
            xml = etree.tostring(
                tree, pretty_print=True,
                xml_declaration=True, encoding='UTF-8')
            self.cfdi_nomina = base64.b64encode(xml)

    @api.model
    def _get_xml_etree(self):
        self.ensure_one()
        cfdi = base64.decodebytes(self.cfdi_nomina)
        return objectify.fromstring(cfdi)

    @api.model
    def _get_stamp_data(self, cfdi):
        self.ensure_one()
        if not hasattr(cfdi, 'Complemento'):
            return None
        attribute = 'tfd:TimbreFiscalDigital[1]'
        namespace = {'tfd': 'http://www.sat.gob.mx/TimbreFiscalDigital'}
        node = cfdi.Complemento.xpath(attribute, namespaces=namespace)
        return node[0] if node else None


class HrPayslipRun(models.Model):
    _inherit = 'hr.payslip.run'

    @api.multi
    def action_payslips_done(self):
        self.ensure_one()
        for payslip in self.slip_ids.filtered(lambda p: p.state == 'draft'):
            try:
                payslip.source_resource = self.source_resource
                payslip.action_payslip_done()
            except ValidationError as ex:
                payslip.message_post(body=ex.name)

    source_resource = fields.Selection([
        ('IP', 'Own income'),
        ('IF', 'Federal income'),
        ('IM', 'Mixed income')],
        help='If this value is set will be assigned in all the payslips '
        'related.')
