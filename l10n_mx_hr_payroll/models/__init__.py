# -*- coding: utf-8 -*-

from . import regimen_employee
from . import isr_table_type
from . import hr_payroll
from . import hr_contract
from . import hr_employee
from . import hr_payslip_type
from . import hr_payslip
from . import hr_payslip_line
from . import hr_payroll_structure
from . import hr_salary_rule
# from . import ir_attachment_facturae
from . import overtime_type
from . import resource_calendar
from . import resource_calendar_type
from . import res_company
from . import salary_payment_type
from . import since_risk
