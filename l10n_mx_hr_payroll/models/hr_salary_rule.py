from odoo import fields, models


class HrSalaryRule(models.Model):
    _inherit = 'hr.salary.rule'

    code_sat = fields.Char('Sat code')
    salary_payment_type_id = fields.Many2one(
        'salary.payment.type', string='Payment type',
    )
    overtime_type_id = fields.Many2one(
        'overtime.type', string='Overtime type',
    )
