# -*- coding: utf-8 -*-

from num2words import num2words
# from odoo.report import report_sxw

import base64
import qrcode
import string
# import StringIO


class Parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(self.__class__, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_total': self._get_total,
            'qrcode': self._get_qrcode,
            'num2words': num2words,
        })

    @staticmethod
    def _get_total(payslip, category='Neto'):
        result = 0.0
        for line in payslip.line_ids:
            if line.category_id.name == category:
                result += line.total
        return result

    def _get_qrcode(self, payslip):
        total = self._get_total(payslip)
        tt = string.zfill('%.6f' % total, 17)

        # Init qr code
        qr = qrcode.QRCode(version=4, box_size=4, border=1)
        # Add the data to qr code
        qr.add_data('?re=' + payslip.company_id.partner_id.vat_split)
        qr.add_data('&rr=' + payslip.employee_id.vat)
        qr.add_data('&tt=' + tt)
        qr.add_data(
            '&id=' +
            (payslip.cfdi_folio_fiscal and payslip.cfdi_folio_fiscal or '')
        )
        qr.make(fit=True)

        # Create image in memory
        img = qr.make_image()
        output = StringIO.StringIO()
        img.save(output, 'PNG')
        output_s = output.getvalue()
        result = base64.b64encode(output_s)
        return result
