===============
Mexican Payroll
===============

This module extends the functionality of hr_payroll module to support
mexican SAT rules for payroll and create electronic payslips using CFDI
1.2 specification.

Installation
============

To install this module, you need to install modules:

#. l10n_mx 2.2.0 or higher
#. hr_contract 1.1.0 or higher
#. hr_payroll 1.2.0 or higher
