<a name="1.2.2"></a>
## 1.2.2 (2017-05-03)


#### Bug Fixes

* **XML:**
  *  add restriction for contract type and employee regimen ([bd556f42](bd556f42))
  *  bank account only required if payment method is electronic transfer ([7a7b95e3](7a7b95e3))
  *  work termination payslip must use payment periodicity set to 99 ([5f1b95e7](5f1b95e7))
  *  exclude node TotalImpuestosRetenidos when value is zero ([ac53f422](ac53f422))
  *  metodoDePago must be "NA" ([78f29b27](78f29b27))
* **cfdi:**  only add curp attribute for emiter node when it is set ([a0183d43](a0183d43))
* **hr.payroll.structure:**  add payroll structure for work termination ([c38c28a3](c38c28a3))
* **hr.salary.rule:**
  *  oreder data in order to be able to install module ([16bd8fe2](16bd8fe2))
  *  set taxable property for several rules ([1ef69747](1ef69747))
* **hr_payslip:**  remove api.v7 decorator from process_sheet function ([b4a6ba7d](b4a6ba7d))
* **security:**  add missing permissions ([f948a0e0](f948a0e0))
* **xml:**  add support for asimilables a salarios ([2c0ad836](2c0ad836))



<a name="1.2.1"></a>
## 1.2.1 (2017-03-08)


#### Bug Fixes

* **Deductions:**
  *  Make late arrives deduction based on LATE code insted on WORK000 ([6533c493](6533c493))
  *  Add support for module hr_payslip_attendance ([4f67ce5e](4f67ce5e))
* **Salario minimo Views:**  display fields with theirs tags ([c7c58a16](c7c58a16))
* **contract:**
  *  Remove old onchange function ([1e9aff9d](1e9aff9d))
  *  Remove deprecated fields ([ac7ca99a](ac7ca99a))
  *  Float division by zero when creating a new contract ([5764cac9](5764cac9))
* **depends:**  Add dependency to l10n_mx_ir_attachment_facturae module ([74cdf5cc](74cdf5cc))
* **documentation:**  something goes wrong afeter merge documentation branch into master ([e5b097c3](e5b097c3))
* **hr.contract:**
  *  Catch error float divizion by zero error when calculate integrated wage ([81065680](81065680))
  *  use import module correctly ([2819bf0e](2819bf0e))
* **hr.payroll:**  fix field declaration ([bd28cbac](bd28cbac))
* **hr.payslip:**
  *  Remove fields defined twice ([c5de4e8d](c5de4e8d))
  *  calculate imss value ([56563b11](56563b11))
  *  use code sat in xml ([a0f12f0e](a0f12f0e))
  *  pass arguments to copy function ([04b5a317](04b5a317))
  *  keep compatibility with odoo v7 ([9eb007c9](9eb007c9))
  *  use name instaed of day in query ([dcb4b039](dcb4b039))
* **hr.payslip.employees:**  why is there a raise here? ([10ade4c9](10ade4c9))
* **hr.payslip.line:**  Set a decimal precision to allow limit the number of digits on taxable_amount ([b25ea29f](b25ea29f))
* **hr.salary.rule:**
  *  Remove inecesary factor on compute rule when selected fix amount ([1acd6879](1acd6879))
  *  Ensure proper rule computation when compute is set to fixed amount or percentag ([75237cc3](75237cc3))
  *  Move late arrivals rule to deductions ([f7d080d5](f7d080d5))
  *  Make salary rule for employeers that pay social security for employees use the ([1cb3950a](1cb3950a))
  *  Remove unknow field complemento from hr.salary.rule retentions data file ([32872ebf](32872ebf))
* **hr_payroll permisions:**  add neccessary permisions to modify this fields ([48bd9dbc](48bd9dbc))
* **hr_payroll.py:**
  *  fixing isr_Table null ([f86254d6](f86254d6))
  *  undefined variables ([77cf0855](77cf0855))
* **hr_payslip:**  send cfdi id when call signal_cancel function ([444836a2](444836a2))
* **imports:**  rewrite the way to import ([b41fb463](b41fb463))
* **manifest:**  Add missing view on manifest ([0bb306c2](0bb306c2))
* **payroll:**  Several fixes for stabilizate modules and can use them on the InternetComunicate proje ([8f017cc4](8f017cc4))
* **payroll view:**  missing form field in the view and ERP crash ([8abc1904](8abc1904))
* **payslip:**
  *  Base ISR calculation where not considering that only overtime payed twice is the one e ([1a58fb9e](1a58fb9e))
  *  Call to function action_printable were missing a param causing problems for get paysli ([d46ce2fa](d46ce2fa))
* **report pdf payslip:**  write correctly format out ([141f9885](141f9885))
* **res.company view:**  show tag field ([5d77b089](5d77b089))
* **security:**  Remove reference to deprecated models ([cc6598fc](cc6598fc))

#### Performance

* **hr.payslip:**  update onchange to work with api verison 8 ([4dab7508](4dab7508))

#### Features

* **api v8.0:**  change some code to work with apiv8.0 ([75990594](75990594))
* **catalogues:**  Update catalogues for CFDI payroll 1.2 ([a4c3edd0](a4c3edd0), closes [#17](17))
* **depends:**  Add dependency to l10n_mx_toponyms module ([4049a3ef](4049a3ef))
* **hr.employee:**  Add fields required by 1.2 payroll CFDI addendum ([1a4300cc](1a4300cc), closes [#14](14))
* **hr.overtime.policy:**  add support for hr.overtime.policy added on hr_payroll module version 1.2. ([dfc95f02](dfc95f02))
* **hr.payroll.structure:**
  *  Add payslip type object and catallogue ([a3ef5f02](a3ef5f02), closes [#16](16))
  *  Add payroll structure to compute Christmas box ([9e7668f9](9e7668f9), closes [#18](18))
* **hr.payslip:**
  *  add nomina1_2 template for payslip CFDI 1.2 addendum ([853c2857](853c2857), closes [#11](11))
  *  Allow cancel signed payslip ([b61a4e65](b61a4e65))
  *  Add payslip rule that allow employeer pay for Social Security for employee ([6995a015](6995a015))
  *  Refactor the function get_payslip_lines to be able to calculate the taxable amount ([e6d304fd](e6d304fd))
  *  Add rules to calculate and display employee obligations like vacations fee, aguina ([ae5f97c0](ae5f97c0))
  *  Add support for infonavit loan deduction on payslip ([f2fdebd4](f2fdebd4))
* **hr.salary.rule:**
  *  add category for OtrosPagos ([cc730434](cc730434))
  *  Add rule for deduct late arrivals for employee ([29af74b8](29af74b8))
* **hr_payslip:**  Update hr_payslip report to a better template ([e4a3f07d](e4a3f07d))
* **l10n_mx_hr_fdi:**  Deprecated module ([8c94a5bd](8c94a5bd))
* **menu:**  Add menus for tables ([3c574bb5](3c574bb5))
* **overtime.type:**  add option to set overtime type for CFDI ([68de697a](68de697a), closes [#15](15))
* **payslip:**  Update CFDI payslip to work with last changes done in payroll module ([328a38cf](328a38cf))
* **sign payroll in SAT:**  add ability to sign the payroll in SAT ([62ed83f7](62ed83f7))



