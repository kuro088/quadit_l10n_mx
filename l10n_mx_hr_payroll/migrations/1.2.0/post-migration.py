"""

Revision ID: l10n_mx_hr_payroll_1.2.0
Revises:
Create Date: 2016-12-28 00:12:32.925776

Set overtime_policy_id field for existing contracts
"""
# revision identifiers, used by Alembic.

from odoo import SUPERUSER_ID
from openupgradelib import openupgrade as tools


revision = '1.2.0'
down_revision = '1.1.0'
branch_labels = None
depends_on = None


@tools.migrate(use_env=True, uid=SUPERUSER_ID)
def migrate(env, installed_version):
    tools.message(
        env.cr, 'l10n_mx_hr_payroll', 'hr.contract', 'overtime_policy_id',
        message='Setting default overtime policy for contracts...',
    )
    # Find record for overtime policy uploaded by this module
    _, record_id = env['ir.model.data'].get_object_reference(
        'l10n_mx_hr_payroll', 'hr_overtime_policy',
    )
    # Search for all contracts where no field overtime_policy_id is not set
    contracts = env['hr.contract'].search([
        ('overtime_policy_id', '=', False),
    ])
    contracts.write({'overtime_policy_id': record_id})
    tools.message(
        env.cr, 'l10n_mx_hr_payroll', 'hr.contract', 'overtime_policy_id',
        message='%s contracts updated.' % len(contracts),
    )
